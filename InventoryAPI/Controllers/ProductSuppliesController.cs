﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    public class ProductSuppliesController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        // GET: api/ProductSupplies
        [ResponseType(typeof(ProductSupply[]))]
        public async Task<IHttpActionResult> GetProductSupplies()
        {
            var productSupplies = db.ProductSupplies.ToList();
            foreach(var ps in productSupplies)
            {
                ps.Product = await db.Products.FindAsync(ps.SkuNumber);
            }

            return Ok(productSupplies);
        }

        // GET: api/ProductSupplies/5
        [ResponseType(typeof(ProductSupply))]
        public IHttpActionResult GetProductSupply(Guid id)
        {
            ProductSupply productSupply = db.ProductSupplies.Find(id);
            if (productSupply == null)
            {
                return NotFound();
            }

            return Ok(productSupply);
        }

        // PUT: api/ProductSupplies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProductSupply(Guid id, ProductSupply productSupply)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != productSupply.ID)
            {
                return BadRequest();
            }

            var currentProductSupplyRecord = db.ProductSupplies.Find(id);
            if ((productSupply.Product.HasExpiredDate && productSupply.ExpiredDate != currentProductSupplyRecord.ExpiredDate || productSupply.PurchasedPrice != currentProductSupplyRecord.PurchasedPrice)
                    || (!productSupply.Product.HasExpiredDate && productSupply.PurchasedPrice != currentProductSupplyRecord.PurchasedPrice ))
            {
                ChangeProductInventory(currentProductSupplyRecord, -currentProductSupplyRecord.PurchasedQuantity, InventoryChangeReason.PurchaseAdjust, "Decrease quantity first " + -currentProductSupplyRecord.PurchasedQuantity + " because key factors are modified");
                ChangeProductInventory(productSupply, productSupply.PurchasedQuantity, InventoryChangeReason.PurchaseAdjust, "Increase quantity after"+ productSupply.PurchasedQuantity+" because key factors are modified");
            }
            else if(productSupply.PurchasedQuantity != currentProductSupplyRecord.PurchasedQuantity)
            {
                var changeQuantity = productSupply.PurchasedQuantity - currentProductSupplyRecord.PurchasedQuantity;
                ChangeProductInventory(productSupply, changeQuantity, InventoryChangeReason.PurchaseAdjust, "Purchased quantity is updated "+changeQuantity+" because purchased quantity is modified");
            }

            currentProductSupplyRecord.Product = null;
            db.ProductSupplies.Remove(currentProductSupplyRecord);
            productSupply.Product = null;
            db.ProductSupplies.Add(productSupply);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductSupplyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        private void ChangeProductInventory(ProductSupply productSupply, int changeQuantity, InventoryChangeReason changeReason, string changeNote="")
        {
            var calgaryLocationID = db.InventoryLocations.Where(il => il.LocationName.Contains("卡尔加里")).First().ID;
            var productInventoryList = db.ProductInventories.Where(pi => pi.SkuNumber == productSupply.SkuNumber).ToList();
            if (productSupply.Product == null)
            {
                productSupply.Product = db.Products.Find(productSupply.SkuNumber);
            }

            bool found = false;
            foreach (var pi in productInventoryList)
            {
                //同序列号产品，相同的保质期，相同的价格，可归为同一库存
                //若无保质期，只要价格相同，可归为同一库存
                if ( (calgaryLocationID == pi.LocationID) 
                    && ((productSupply.Product.HasExpiredDate && productSupply.ExpiredDate == pi.ExpiredDate && productSupply.PurchasedPrice == pi.PurchasedPrice)
                        || (!productSupply.Product.HasExpiredDate && productSupply.PurchasedPrice == pi.PurchasedPrice)
                        ))
                {
                    
                    pi.QuantityOnHand += changeQuantity;
                    db.Entry(pi).State = EntityState.Modified;
                    db.InventoryChangeRecords.Add(new InventoryChangeRecord()
                    {
                        ID = Guid.NewGuid(),
                        SkuNumber = productSupply.SkuNumber,
                        ProductInventoryID = pi.ID,
                        ChangeQuantity = changeQuantity,
                        ChangeDate = DateTime.Now,
                        ChangeReason = changeReason,
                        
                        ChangeNote=changeNote,
                        FromLocationID = Guid.Empty,
                        ToLocationID = calgaryLocationID
                    });
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                var newProdInventory = new ProductInventory()
                {
                    ID = Guid.NewGuid(),
                    SkuNumber = productSupply.SkuNumber,
                    LocationID = calgaryLocationID,
                    ExpiredDate = productSupply.Product.HasExpiredDate ? productSupply.ExpiredDate : DateTime.MinValue,
                    QuantityOnHand = productSupply.PurchasedQuantity,
                    PurchasedPrice = productSupply.PurchasedPrice
                };
                db.ProductInventories.Add(newProdInventory);
                db.InventoryChangeRecords.Add(new InventoryChangeRecord()
                {
                    ID = Guid.NewGuid(),
                    SkuNumber = productSupply.SkuNumber,
                    ProductInventoryID = newProdInventory.ID,
                    ChangeQuantity = productSupply.PurchasedQuantity,
                    ChangeDate = DateTime.Now,
                    ChangeReason = changeReason,
                    ChangeNote = changeNote,
                    FromLocationID = Guid.Empty,
                    ToLocationID = calgaryLocationID

                });
            }
        }

        // POST: api/ProductSupplies
        [ResponseType(typeof(ProductSupply))]
        public IHttpActionResult PostProductSupply(ProductSupply productSupply)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ChangeProductInventory(productSupply,productSupply.PurchasedQuantity,InventoryChangeReason.Purchase, "add new product purchased record");

            productSupply.Product = null;
            db.ProductSupplies.Add(productSupply);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ProductSupplyExists(productSupply.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = productSupply.ID }, productSupply);
        }

        // DELETE: api/ProductSupplies/5
        [ResponseType(typeof(ProductSupply))]
        public IHttpActionResult DeleteProductSupply(Guid id)
        {
            ProductSupply productSupply = db.ProductSupplies.Find(id);
            if (productSupply == null)
            {
                return NotFound();
            }

            ChangeProductInventory(productSupply, -productSupply.PurchasedQuantity, InventoryChangeReason.PurchaseAdjust, "delete product purchased record");

            db.ProductSupplies.Remove(productSupply);
            db.SaveChanges();

            return Ok(productSupply);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductSupplyExists(Guid id)
        {
            return db.ProductSupplies.Count(e => e.ID == id) > 0;
        }
    }
}