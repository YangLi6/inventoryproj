﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;

namespace InventoryAPI.Controllers
{
    public class PaymentMethodsController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        // GET: api/PaymentMethods
        public IQueryable<PaymentMethod> GetPaymentMethods()
        {
            return db.PaymentMethods;
        }

        // GET: api/PaymentMethods/5
        [ResponseType(typeof(PaymentMethod))]
        public IHttpActionResult GetPaymentMethod(Guid id)
        {
            PaymentMethod paymentMethod = db.PaymentMethods.Find(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            return Ok(paymentMethod);
        }

        // PUT: api/PaymentMethods/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPaymentMethod(Guid id, PaymentMethod paymentMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != paymentMethod.ID)
            {
                return BadRequest();
            }

            db.Entry(paymentMethod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PaymentMethodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PaymentMethods
        [ResponseType(typeof(PaymentMethod))]
        public IHttpActionResult PostPaymentMethod(PaymentMethod paymentMethod)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PaymentMethods.Add(paymentMethod);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PaymentMethodExists(paymentMethod.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = paymentMethod.ID }, paymentMethod);
        }

        // DELETE: api/PaymentMethods/5
        [ResponseType(typeof(PaymentMethod))]
        public IHttpActionResult DeletePaymentMethod(Guid id)
        {
            PaymentMethod paymentMethod = db.PaymentMethods.Find(id);
            if (paymentMethod == null)
            {
                return NotFound();
            }

            db.PaymentMethods.Remove(paymentMethod);
            db.SaveChanges();

            return Ok(paymentMethod);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PaymentMethodExists(Guid id)
        {
            return db.PaymentMethods.Count(e => e.ID == id) > 0;
        }
    }
}