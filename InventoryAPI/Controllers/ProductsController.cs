﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    public class ProductsController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        //// GET: api/Products
        [ResponseType(typeof(Product[]))]
        public async Task<IHttpActionResult> GetProduct()
        {
            var allProducts = db.Products.ToList();
            foreach (var prod in allProducts)
            {
                var productImages = db.ProductImages.Where(prodImage => prodImage.SkuNumber == prod.SkuNumber);
                foreach (var prodImage in productImages)
                {
                    prod.ProductImages.Add(prodImage);
                }
                var productInventory = db.ProductInventories.Where(pi => pi.SkuNumber == prod.SkuNumber && pi.QuantityOnHand != 0);
                foreach (var pi in productInventory)
                {
                    pi.InventoryLocation = await db.InventoryLocations.Where(il => il.ID == pi.LocationID).FirstAsync();
                    prod.ProductInventories.Add(pi);
                }

            }
            return Ok(allProducts);
        }

        //// GET: api/Products/5
        //[ResponseType(typeof(Product))]
        //public IHttpActionResult GetProduct(string id)
        //{
        //    Product product = db.Products.Find(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }

            //    return Ok(product);
            //}

            // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(string id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.SkuNumber)
            {
                return BadRequest();
            }

            //add image if not in db
            foreach (var productImage in product.ProductImages)
            {
                var existedProductImage = db.ProductImages.Find(productImage.ID);
                if (existedProductImage == null)
                {
                    db.ProductImages.Add(productImage);
                }
                else {
                    if (productImage.IsDefault != existedProductImage.IsDefault) {
                        existedProductImage.IsDefault = productImage.IsDefault;
                        db.Entry(existedProductImage).State = EntityState.Modified;
                    }
                }
            }
            db.SaveChanges();

            //remove image if user deleted the image
            var savedProductImages = db.ProductImages.Where(pi => pi.SkuNumber == product.SkuNumber).ToList();
            if (savedProductImages.Count > product.ProductImages.Count)
            {
                //remove extras
                foreach(var savedImage in savedProductImages)
                {
                    var deletedImage = product.ProductImages.FirstOrDefault(image => image.ID == savedImage.ID);
                    if (deletedImage == null)
                    {
                        db.ProductImages.Remove(savedImage);
                    }
                }
                db.SaveChanges();
            }


            //db.Entry(product).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //Todo: update id photo information
            db.Products.Add(product);

            foreach (var productImage in product.ProductImages)
            {
                var existedProductImage = db.ProductImages.Find(productImage.ID);
                if (existedProductImage == null)
                {
                    db.ProductImages.Add(productImage);
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ProductExists(product.SkuNumber))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = product.SkuNumber }, product);
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(string id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            //Todo: delete product image information
            //Todo: delete product inventory info

            db.Products.Remove(product);
            db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(string id)
        {
            return db.Products.Count(e => e.SkuNumber == id) > 0;
        }
    }
}