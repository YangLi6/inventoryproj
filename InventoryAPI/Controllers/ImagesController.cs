﻿using DatabaseAccessLayer.Repository;
using InventoryAPI.Helpers;
using Microsoft.WindowsAzure.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace InventoryAPI.Controllers
{
    public class ImagesController : ApiController
    {
        private InventoryManagementDBContext db;
        private AzureBlobProvider provider;
        static long MAXREQUESTLENGTH = 20480 * 1024; // Unit: bytes, so this is 20MB
        public ImagesController()
        {
            db = new InventoryManagementDBContext();
            //provider = new AzureBlobProvider(CloudStorageAccount.DevelopmentStorageAccount);
            provider = new AzureBlobProvider();
        }

        [ResponseType(typeof(ImageSummary))]
        //public async Task<IHttpActionResult> PostImage(HttpRequestMessage Request)
        public async Task<IHttpActionResult> PostImage(HttpRequestMessage Request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (Request.Content.Headers.ContentLength > MAXREQUESTLENGTH)
            {
                return BadRequest("Uploaded image exceeded 20MB");
            }

            if (Request.Content.IsMimeMultipartContent())
            {
                return BadRequest("Uploaded image is not readable");

            }
            HttpContent content = Request.Content;
            byte[] imagedata = await content.ReadAsByteArrayAsync();
            MemoryStream stream = new MemoryStream(imagedata);

            string mimeType = Request.Content.Headers.ContentType.MediaType;

            ImageUploadResult result = await provider.UploadImageAsync(stream, mimeType);
            if (result.Error != "")
            {
                return BadRequest(result.Error);
            }
            return Ok(result.Data as ImageSummary);

            //else
            //{
            //    MultipartMemoryStreamProvider content = await HttpContentMultipartExtensions.ReadAsMultipartAsync(Request.Content);
            //    var mimeType = content.Contents.First(a => a.Headers.ContentDisposition.Name.Contains("mimetype"));
            //    var file = content.Contents.First(a => a.Headers.ContentDisposition.Name.Contains("file"));
            //    if (mimeType == null || file == null)
            //    {
            //        return BadRequest("Missing mimetype or file in MimeMultipartContent.");
            //    }
            //    string mimeString = await mimeType.ReadAsStringAsync();
            //    Stream filestream = await file.ReadAsStreamAsync();

            //    //var serverResponse = await Testable.PostImage(User, canvasID, mimeString, filestream);
            //    ImageUploadResult result = await provider.UploadImageAsync(".png", filestream, canvasID);
            //    if (result.Error != "")
            //    {
            //        return BadRequest(result.Error);
            //    }
            //    return Ok(result.Data as ImageSummary);
            //}
        }
    }
}
