﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    public class OrdersController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        // GET: api/Orders
        public async Task<IHttpActionResult> GetOrders()
        {
            var orders = db.Orders.ToList();
            foreach(var order in orders)
            {
                //order.PaymentMethod= await db.PaymentMethods.FindAsync(order.PaymentMethodID);
                order.Customer = await db.Customers.FindAsync(order.CustomerID);
                order.OrderImages = await db.OrderImages.Where(oi => oi.OrderID == order.ID).ToListAsync();
                order.OrderedExistedProducts = await db.OrderProductInventories.Where(opi => opi.OrderID == order.ID).ToListAsync();
                order.PurchaseRequiredProducts = await db.PurchaseRequiredProducts.Where(prp => prp.OrderID == order.ID).ToListAsync();
                order.Shippings =await (from os in db.OrderShippings
                                   from sh in db.Shippings
                                   where os.OrderID == order.ID && os.ShippingID == sh.ID
                                   select sh).ToListAsync();
                                  
                foreach(var oep in order.OrderedExistedProducts){
                    oep.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == oep.ProductInventoryID).FirstAsync();
                    oep.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == oep.ProductInventory.SkuNumber).FirstAsync();
                    oep.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == oep.ProductInventory.LocationID).FirstAsync();
                }   
                    //(from opi in db.OrderProductInventories
                    //                     from pi in db.ProductInventories
                    //                     where opi.OrderID==order.ID && pi.ID==opi.ProductInventoryID
                    //                     select new ProductInventory 
                    ////db.OrderProductInventories.Where(opi => opi.OrderID == order.ID).ToList();

            }
            return Ok(orders);
        }

        // GET: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult GetOrder(Guid id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutOrder(Guid id, Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.ID)
            {
                return BadRequest();
            }

            db.Entry(order).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            order.OrderedExistedProducts = await db.OrderProductInventories.Where(opi => opi.OrderID == order.ID).ToListAsync();
            order.PurchaseRequiredProducts = await db.PurchaseRequiredProducts.Where(prp => prp.OrderID == order.ID).ToListAsync();
            order.Shippings = await(from os in db.OrderShippings
                                    from sh in db.Shippings
                                    where os.OrderID == order.ID && os.ShippingID == sh.ID
                                    select sh).ToListAsync();

            foreach (var oep in order.OrderedExistedProducts)
            {
                oep.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == oep.ProductInventoryID).FirstAsync();
                oep.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == oep.ProductInventory.SkuNumber).FirstAsync();
                oep.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == oep.ProductInventory.LocationID).FirstAsync();
            }
            return Ok(order);
        }

        // PUT: api/Orders/Shipped
        [Route("api/Orders/Shipped")]
        [HttpPut]
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> ShipOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Order existedOrder = await db.Orders.FindAsync(order.ID);
            double shippingcost = 0;
            foreach(var sh in order.Shippings)
            {
                sh.ReceivedDate = null;
                sh.ShippingStatus = ShippingStatus.Shipped;
                db.Shippings.Add(sh);
                db.OrderShippings.Add(new OrderShipping() { ID = Guid.NewGuid(), OrderID = order.ID, ShippingID = sh.ID });
                shippingcost += sh.ShippingCost;
            }
            existedOrder.ShippingCost = shippingcost;
            existedOrder.Status = OrderStatus.Shipped;
            db.Entry(existedOrder).State = EntityState.Modified;

            order.OrderedExistedProducts = await db.OrderProductInventories.Where(opi => opi.OrderID == order.ID).ToListAsync();
            order.PurchaseRequiredProducts = await db.PurchaseRequiredProducts.Where(prp => prp.OrderID == order.ID).ToListAsync();
            foreach (var oep in order.OrderedExistedProducts)
            {
                var productInventory = await db.ProductInventories.Where(pi => pi.ID == oep.ProductInventoryID).FirstAsync();
                productInventory.TotalReservedQuantity -= oep.ReservedQuantity;
                productInventory.QuantityOnHand -= oep.ReservedQuantity;
                db.Entry(productInventory).State = EntityState.Modified;
            }

            foreach(var prp in order.PurchaseRequiredProducts)
            {
                db.PurchaseRequiredProducts.Remove(prp);
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(order.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            existedOrder.Customer = await db.Customers.FindAsync(existedOrder.CustomerID);
            existedOrder.Shippings = await (from os in db.OrderShippings
                                     from sh in db.Shippings
                                     where os.OrderID == existedOrder.ID && os.ShippingID == sh.ID
                                     select sh).ToListAsync();
            existedOrder.OrderedExistedProducts = await db.OrderProductInventories.Where(opi => opi.OrderID == existedOrder.ID).ToListAsync();
            foreach (var oep in existedOrder.OrderedExistedProducts)
            {
                oep.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == oep.ProductInventoryID).FirstAsync();
                oep.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == oep.ProductInventory.SkuNumber).FirstAsync();
                oep.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == oep.ProductInventory.LocationID).FirstAsync();
            }
            existedOrder.PurchaseRequiredProducts = await db.PurchaseRequiredProducts.Where(prp => prp.OrderID == existedOrder.ID).ToListAsync();
            return Ok(existedOrder);

        }

        // PUT: api/Orders/Shipped
        [Route("api/Orders/UpdateShipping")]
        [HttpPut]
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> UpdateShipping(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Order existedOrder = await db.Orders.FindAsync(order.ID);

            //remove deleted shipping infos
            existedOrder.Shippings = await (from os in db.OrderShippings
                                            from sh in db.Shippings
                                            where os.OrderID == existedOrder.ID && os.ShippingID == sh.ID
                                            select sh).ToListAsync();
            foreach (var oldSh in existedOrder.Shippings)
            {
                Shipping foundShipping = order.Shippings.Where(sh => sh.ID == oldSh.ID).FirstOrDefault();
                if (foundShipping == null)
                {
                    db.Shippings.Remove(oldSh);
                    var toDeleteOS =db.OrderShippings.Where(os => os.ShippingID == oldSh.ID && os.OrderID == order.ID).First();
                    db.OrderShippings.Remove(toDeleteOS);
                }
            }

            double shippingcost = 0;
            foreach (var sh in order.Shippings)
            {
                Shipping existedShipping = db.Shippings.Find(sh.ID);
                if (existedShipping != null)
                {
                    db.Entry(existedShipping).State = EntityState.Detached;
                    db.Entry(sh).State = EntityState.Modified;
                }
                else
                {
                    db.Shippings.Add(sh);
                    db.OrderShippings.Add(new OrderShipping() { ID = Guid.NewGuid(), OrderID = order.ID, ShippingID = sh.ID });
                }

                shippingcost += sh.ShippingCost;
            }
            existedOrder.ShippingCost = shippingcost;
            db.Entry(existedOrder).State = EntityState.Modified;

            

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(order.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            existedOrder.Customer = await db.Customers.FindAsync(existedOrder.CustomerID);
            existedOrder.Shippings = await (from os in db.OrderShippings
                                            from sh in db.Shippings
                                            where os.OrderID == existedOrder.ID && os.ShippingID == sh.ID
                                            select sh).ToListAsync();
            existedOrder.OrderedExistedProducts = await db.OrderProductInventories.Where(opi => opi.OrderID == existedOrder.ID).ToListAsync();
            foreach (var oep in existedOrder.OrderedExistedProducts)
            {
                oep.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == oep.ProductInventoryID).FirstAsync();
                oep.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == oep.ProductInventory.SkuNumber).FirstAsync();
                oep.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == oep.ProductInventory.LocationID).FirstAsync();
            }
            existedOrder.PurchaseRequiredProducts = await db.PurchaseRequiredProducts.Where(prp => prp.OrderID == existedOrder.ID).ToListAsync();

            return Ok(existedOrder);
        }

        // POST: api/Orders
        [ResponseType(typeof(Order))]
        public async Task<IHttpActionResult> PostOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            order.Customer = null;
            db.Orders.Add(order);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (OrderExists(order.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            order = await db.Orders.FindAsync(order.ID);
            order.Customer = await db.Customers.FindAsync(order.CustomerID);
            order.PaymentMethodID = Guid.Empty;
            order.OrderImages = new List<OrderImage>();
            order.OrderedExistedProducts = new List<OrderProductInventory>();
            order.PurchaseRequiredProducts = new List<PurchaseRequiredProduct>();
            order.Shippings = new List<Shipping>();
            return CreatedAtRoute("DefaultApi", new { id = order.ID }, order);
        }

        // DELETE: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult DeleteOrder(Guid id)
        {
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }

            order.OrderedExistedProducts = db.OrderProductInventories.Where(opi => opi.OrderID == order.ID).ToList();

            //remove reserved product inventory quantity first
            foreach (var opi in order.OrderedExistedProducts)
            {
                var productInventory = db.ProductInventories.Find(opi.ProductInventoryID);
                productInventory.TotalReservedQuantity -= opi.ReservedQuantity;
                db.Entry(productInventory).State = EntityState.Modified;
                db.OrderProductInventories.Remove(opi);
            }

            //remove preparing order
            db.Orders.Remove(order);
            db.SaveChanges();

            return Ok(order);

           
        }


        [Route("api/Orders/OrderProductInventory")]
        public async Task<IHttpActionResult> PostOrderProductInventory(OrderProductInventory opi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var productInventory = db.ProductInventories.Find(opi.ProductInventoryID);
            if (productInventory.QuantityOnHand < opi.OrderedQuantity)
            {
                return BadRequest("Quantity is over ordered at current inventory location");
            }

            //update order product cost
            var order = db.Orders.Find(opi.OrderID);
            order.ProductCost += opi.ProductInventory.PurchasedPrice * opi.OrderedQuantity;
            db.Entry(order).State = EntityState.Modified;

            //update reserved quantity
            opi.ReservedQuantity += opi.OrderedQuantity;
            productInventory.TotalReservedQuantity += opi.OrderedQuantity;
            db.Entry(productInventory).State = EntityState.Modified;
            opi.ProductInventory = null;
            db.OrderProductInventories.Add(opi);


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (OrderProductInventoryExists(opi.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            opi.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == opi.ProductInventoryID).FirstAsync();
            opi.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == opi.ProductInventory.SkuNumber).FirstAsync();
            opi.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == opi.ProductInventory.LocationID).FirstAsync();
            return Ok(opi);
        }

        
        [Route("api/Orders/OrderProductInventoryQuantity")]
        public async Task<IHttpActionResult> PutOrderProductInventoryQuantity(Guid id, int orderQuantity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
           
            var opi = db.OrderProductInventories.Find(id);
            var changeQuantity = orderQuantity- opi.OrderedQuantity;
            var productInventory = db.ProductInventories.Find(opi.ProductInventoryID);
            //logic is wrong here
            //if (changeQuantity > 0 && productInventory.QuantityOnHand - changeQuantity<0)
            //{
            //    return BadRequest();
            //}
            opi.OrderedQuantity = orderQuantity;
            opi.ReservedQuantity += changeQuantity;
            productInventory.TotalReservedQuantity += changeQuantity;
            opi.ProductInventory = productInventory;

            db.Entry(opi).State = EntityState.Modified;
            db.Entry(productInventory).State = EntityState.Modified;

            //update order product cost
            var order = db.Orders.Find(opi.OrderID);
            order.ProductCost += opi.ProductInventory.PurchasedPrice * changeQuantity;
            db.Entry(order).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderProductInventoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(opi);
        }

        [ResponseType(typeof(OrderProductInventory))]
        [Route("api/Orders/OrderProductInventory")]
        public IHttpActionResult DeleteOrderProductInventory(Guid id)
        {
            var opi = db.OrderProductInventories.Find(id);
            if (opi == null)
            {
                return NotFound();
            }
            var productInventory = db.ProductInventories.Find(opi.ProductInventoryID);

            //update order product cost
            var order = db.Orders.Find(opi.OrderID);
            order.ProductCost -= productInventory.PurchasedPrice * opi.ReservedQuantity;
            db.Entry(order).State = EntityState.Modified;

            productInventory.TotalReservedQuantity -= opi.ReservedQuantity;
            db.Entry(productInventory).State = EntityState.Modified;

            db.OrderProductInventories.Remove(opi);

            db.SaveChanges();

            return Ok(opi);
        }

        [Route("api/Orders/PurchaseRequiredProduct")]
        public async Task<IHttpActionResult> PostPurchaseRequiredProduct(PurchaseRequiredProduct prp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(prp.OrderID==null || prp.OrderID == Guid.Empty)
            {
                return BadRequest("Missing order id");
            }
            var targetOrder = db.Orders.Find(prp.OrderID);
            if (targetOrder == null)
            {
                return BadRequest("Cannot find the request order");
            }

            PurchaseRequiredProduct result = null;
            if (prp.SkuNumber != null && prp.SkuNumber != "")
            {
                var existedPrp = db.PurchaseRequiredProducts.Where(p => p.OrderID == prp.OrderID && p.SkuNumber == prp.SkuNumber).FirstOrDefault();
                if (existedPrp != null)
                {
                    existedPrp.Quantity += prp.Quantity;
                    db.Entry(existedPrp).State = EntityState.Modified;
                    result = existedPrp;
                }
                else
                {
                    db.PurchaseRequiredProducts.Add(prp);
                    result = prp;
                }
            }
            else
            {
                db.PurchaseRequiredProducts.Add(prp);
                result = prp;
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PurchaseRequiredProductExists(prp.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            return Ok(result);
        }

        [Route("api/Orders/PurchaseRequiredProduct")]
        public async Task<IHttpActionResult> DeletePurchaseRequiredProduct(Guid id)
        {
            PurchaseRequiredProduct prp = db.PurchaseRequiredProducts.Find(id);
            if (prp == null)
            {
                return NotFound();
            }

            db.PurchaseRequiredProducts.Remove(prp);
            db.SaveChanges();

            return Ok(prp);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool OrderExists(Guid id)
        {
            return db.Orders.Count(e => e.ID == id) > 0;
        }

        private bool OrderProductInventoryExists(Guid id)
        {
            return db.OrderProductInventories.Count(e => e.ID == id) > 0;
        }

        private bool PurchaseRequiredProductExists(Guid id)
        {
            return db.PurchaseRequiredProducts.Count(e => e.ID == id) > 0;
        }
    }
}