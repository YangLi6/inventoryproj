﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;

namespace InventoryAPI.Controllers
{
    public class ExpressCompaniesController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        // GET: api/ExpressCompanies
        public IQueryable<ExpressCompany> GetExpressCompanies()
        {
            return db.ExpressCompanies;
        }

        // GET: api/ExpressCompanies/5
        [ResponseType(typeof(ExpressCompany))]
        public IHttpActionResult GetExpressCompany(Guid id)
        {
            ExpressCompany expressCompany = db.ExpressCompanies.Find(id);
            if (expressCompany == null)
            {
                return NotFound();
            }

            return Ok(expressCompany);
        }

        // PUT: api/ExpressCompanies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExpressCompany(Guid id, ExpressCompany expressCompany)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != expressCompany.ID)
            {
                return BadRequest();
            }

            db.Entry(expressCompany).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExpressCompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ExpressCompanies
        [ResponseType(typeof(ExpressCompany))]
        public IHttpActionResult PostExpressCompany(ExpressCompany expressCompany)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ExpressCompanies.Add(expressCompany);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ExpressCompanyExists(expressCompany.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = expressCompany.ID }, expressCompany);
        }

        // DELETE: api/ExpressCompanies/5
        [ResponseType(typeof(ExpressCompany))]
        public IHttpActionResult DeleteExpressCompany(Guid id)
        {
            ExpressCompany expressCompany = db.ExpressCompanies.Find(id);
            if (expressCompany == null)
            {
                return NotFound();
            }

            db.ExpressCompanies.Remove(expressCompany);
            db.SaveChanges();

            return Ok(expressCompany);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExpressCompanyExists(Guid id)
        {
            return db.ExpressCompanies.Count(e => e.ID == id) > 0;
        }
    }
}