﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;

namespace InventoryAPI.Controllers
{
    public class InventoryLocationsController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        // GET: api/InventoryLocations
        public IQueryable<InventoryLocation> GetInventoryLocations()
        {
            return db.InventoryLocations;
        }

        // GET: api/InventoryLocations/5
        [ResponseType(typeof(InventoryLocation))]
        public IHttpActionResult GetInventoryLocation(Guid id)
        {
            InventoryLocation inventoryLocation = db.InventoryLocations.Find(id);
            if (inventoryLocation == null)
            {
                return NotFound();
            }

            return Ok(inventoryLocation);
        }

        // PUT: api/InventoryLocations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutInventoryLocation(Guid id, InventoryLocation inventoryLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventoryLocation.ID)
            {
                return BadRequest();
            }

            db.Entry(inventoryLocation).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InventoryLocationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InventoryLocations
        [ResponseType(typeof(InventoryLocation))]
        public IHttpActionResult PostInventoryLocation(InventoryLocation inventoryLocation)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InventoryLocations.Add(inventoryLocation);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (InventoryLocationExists(inventoryLocation.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = inventoryLocation.ID }, inventoryLocation);
        }

        // DELETE: api/InventoryLocations/5
        [ResponseType(typeof(InventoryLocation))]
        public IHttpActionResult DeleteInventoryLocation(Guid id)
        {
            InventoryLocation inventoryLocation = db.InventoryLocations.Find(id);
            if (inventoryLocation == null)
            {
                return NotFound();
            }

            db.InventoryLocations.Remove(inventoryLocation);
            db.SaveChanges();

            return Ok(inventoryLocation);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InventoryLocationExists(Guid id)
        {
            return db.InventoryLocations.Count(e => e.ID == id) > 0;
        }
    }
}