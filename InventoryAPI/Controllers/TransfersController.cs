﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DatabaseAccessLayer.DBModels;
using DatabaseAccessLayer.Repository;
using System.Threading.Tasks;

namespace InventoryAPI.Controllers
{
    public class TransfersController : ApiController
    {
        private InventoryManagementDBContext db = new InventoryManagementDBContext();

        // GET: api/Transfers
        public async Task<IHttpActionResult> GetTransfers()
        {
            var transfers = db.Transfers.ToList();
            foreach (var transfer in transfers)
            {
                transfer.TransferedProducts = await db.TransferProductInventories.Where(tpi => tpi.TransferID == transfer.ID).ToListAsync();
                foreach(var tp in transfer.TransferedProducts)
                {
                    tp.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == tp.ProductInventoryID).FirstAsync();
                    tp.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == tp.ProductInventory.SkuNumber).FirstAsync();
                    tp.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == tp.ProductInventory.LocationID).FirstAsync();
                }
                transfer.Shippings = await (from ts in db.TransferShippings
                                            from sh in db.Shippings
                                            where ts.TransferID == transfer.ID && ts.ShippingID == sh.ID
                                            select sh).ToListAsync();
            }

            return Ok(transfers);
        }

        // GET: api/Transfers/5
        [ResponseType(typeof(Transfer))]
        public IHttpActionResult GetTransfer(Guid id)
        {
            Transfer transfer = db.Transfers.Find(id);
            if (transfer == null)
            {
                return NotFound();
            }

            return Ok(transfer);
        }

        // PUT: api/Transfers/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTransfer(Guid id, Transfer transfer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != transfer.ID)
            {
                return BadRequest();
            }

            db.Entry(transfer).State = EntityState.Modified;

            if (transfer.Status == TransferStatus.Closed) //update destination inventories
            {
                foreach(var tp in transfer.TransferedProducts)
                {
                    await TransferInventory(tp, transfer.ToInventoryLocationID);
                }
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransferExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            transfer.TransferedProducts = await db.TransferProductInventories.Where(tpi => tpi.TransferID == transfer.ID).ToListAsync();
            foreach (var tp in transfer.TransferedProducts)
            {
                tp.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == tp.ProductInventoryID).FirstAsync();
                tp.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == tp.ProductInventory.SkuNumber).FirstAsync();
                tp.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == tp.ProductInventory.LocationID).FirstAsync();
            }
            transfer.Shippings = await(from ts in db.TransferShippings
                                       from sh in db.Shippings
                                       where ts.TransferID == transfer.ID && ts.ShippingID == sh.ID
                                       select sh).ToListAsync();
            return Ok(transfer);
        }

        private async Task TransferInventory(TransferProductInventory tpi, Guid toLocationID)
        {
            tpi.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == tpi.ProductInventoryID).FirstAsync();
            var destProductInventoryList =await db.ProductInventories.Where(pi => pi.SkuNumber == tpi.ProductInventory.SkuNumber).ToListAsync();
            var product = db.Products.Find(tpi.ProductInventory.SkuNumber);

            bool found = false;
            foreach(var pi in destProductInventoryList)
            {
                if ((toLocationID == pi.LocationID)
                    && ((product.HasExpiredDate && tpi.ProductInventory.ExpiredDate == pi.ExpiredDate 
                        && tpi.ProductInventory.PurchasedPrice == pi.PurchasedPrice)
                        || (!product.HasExpiredDate && tpi.ProductInventory.PurchasedPrice == pi.PurchasedPrice)
                        ))
                {

                    pi.QuantityOnHand += tpi.TransferedQuantity;
                    db.Entry(pi).State = EntityState.Modified;
                    db.InventoryChangeRecords.Add(new InventoryChangeRecord()
                    {
                        ID = Guid.NewGuid(),
                        SkuNumber = product.SkuNumber,
                        ProductInventoryID = pi.ID,
                        ChangeQuantity = tpi.TransferedQuantity,
                        ChangeDate = DateTime.Now,
                        ChangeReason = InventoryChangeReason.TransferLocation,

                        ChangeNote = "",
                        FromLocationID = tpi.ProductInventory.LocationID,
                        ToLocationID = toLocationID
                    });
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                var newProdInventory = new ProductInventory()
                {
                    ID = Guid.NewGuid(),
                    SkuNumber = product.SkuNumber,
                    LocationID = toLocationID,
                    ExpiredDate = product.HasExpiredDate ? tpi.ProductInventory.ExpiredDate : DateTime.MinValue,
                    QuantityOnHand = tpi.TransferedQuantity,
                    PurchasedPrice = tpi.ProductInventory.PurchasedPrice
                };
                db.ProductInventories.Add(newProdInventory);
                db.InventoryChangeRecords.Add(new InventoryChangeRecord()
                {
                    ID = Guid.NewGuid(),
                    SkuNumber = product.SkuNumber,
                    ProductInventoryID = newProdInventory.ID,
                    ChangeQuantity = tpi.TransferedQuantity,
                    ChangeDate = DateTime.Now,
                    ChangeReason = InventoryChangeReason.TransferLocation,
                    ChangeNote = "",
                    FromLocationID = tpi.ProductInventory.LocationID,
                    ToLocationID = toLocationID

                });
            }
        }


        // POST: api/Transfers
        [ResponseType(typeof(Transfer))]
        public IHttpActionResult PostTransfer(Transfer transfer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Transfers.Add(transfer);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TransferExists(transfer.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }
            transfer.TransferedProducts = new List<TransferProductInventory>();
            transfer.Shippings = new List<Shipping>();
            return CreatedAtRoute("DefaultApi", new { id = transfer.ID }, transfer);
        }

        // DELETE: api/Transfers/5
        [ResponseType(typeof(Transfer))]
        public IHttpActionResult DeleteTransfer(Guid id)
        {
            Transfer transfer = db.Transfers.Find(id);
            if (transfer == null)
            {
                return NotFound();
            }

            transfer.TransferedProducts = db.TransferProductInventories.Where(tpi => tpi.TransferID == id).ToList();

            foreach(var tpi in transfer.TransferedProducts)
            {
                var productInventory = db.ProductInventories.Find(tpi.ProductInventoryID);
                productInventory.TotalReservedQuantity -= tpi.TransferedQuantity;
                db.Entry(productInventory).State = EntityState.Modified;
                db.TransferProductInventories.Remove(tpi);
            }

            db.Transfers.Remove(transfer);
            db.SaveChanges();

            return Ok(transfer);
        }

        // PUT: api/Transfers/Shipped
        [Route("api/Transfers/Shipped")]
        [HttpPut]
        [ResponseType(typeof(Transfer))]
        public async Task<IHttpActionResult> ShipTransfer(Transfer transfer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Transfer existedTransfer = await db.Transfers.FindAsync(transfer.ID);
            double shippingcost = 0;
            foreach (var sh in transfer.Shippings)
            {
                sh.ReceivedDate = null;
                sh.ShippingStatus = ShippingStatus.Shipped;
                db.Shippings.Add(sh);
                db.TransferShippings.Add(new TransferShipping() { ID = Guid.NewGuid(), TransferID = transfer.ID, ShippingID = sh.ID });
                shippingcost += sh.ShippingCost;
            }
            existedTransfer.ShippingCost = shippingcost;
            existedTransfer.Status = TransferStatus.Shipped;
            db.Entry(existedTransfer).State = EntityState.Modified;

            transfer.TransferedProducts = await db.TransferProductInventories.Where(tpi => tpi.TransferID == transfer.ID).ToListAsync();
            foreach(var tpi in transfer.TransferedProducts)
            {
                var productInventory = await db.ProductInventories.Where(pi => pi.ID == tpi.ProductInventoryID).FirstAsync();
                productInventory.TotalReservedQuantity -= tpi.TransferedQuantity;
                productInventory.QuantityOnHand -= tpi.TransferedQuantity;
                db.Entry(productInventory).State = EntityState.Modified;
            }

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransferExists(transfer.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            existedTransfer.Shippings = await (from ts in db.TransferShippings
                                        from sh in db.Shippings
                                        where ts.TransferID == existedTransfer.ID && ts.ShippingID == sh.ID
                                        select sh).ToListAsync();
            existedTransfer.TransferedProducts = await db.TransferProductInventories.Where(tpi => tpi.TransferID == existedTransfer.ID).ToListAsync();
            foreach (var tp in existedTransfer.TransferedProducts)
            {
                tp.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == tp.ProductInventoryID).FirstAsync();
                tp.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == tp.ProductInventory.SkuNumber).FirstAsync();
                tp.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == tp.ProductInventory.LocationID).FirstAsync();
            }
            return Ok(existedTransfer);

        }

        // PUT: api/Transfer/UpdateShipping
        [Route("api/Transfer/UpdateShipping")]
        [HttpPut]
        [ResponseType(typeof(Transfer))]
        public async Task<IHttpActionResult> UpdateShipping(Transfer transfer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Transfer existedTransfer= await db.Transfers.FindAsync(transfer.ID);

            //remove deleted shipping infos
            existedTransfer.Shippings = await (from ts in db.TransferShippings
                                            from sh in db.Shippings
                                            where ts.TransferID== existedTransfer.ID && ts.ShippingID == sh.ID
                                            select sh).ToListAsync();
            foreach (var oldSh in existedTransfer.Shippings)
            {
                Shipping foundShipping = transfer.Shippings.Where(sh => sh.ID == oldSh.ID).FirstOrDefault();
                if (foundShipping == null)
                {
                    db.Shippings.Remove(oldSh);
                    var toDeleteTS = db.TransferShippings.Where(ts => ts.ShippingID == oldSh.ID && ts.TransferID== transfer.ID).First();
                    db.TransferShippings.Remove(toDeleteTS);
                }
            }

            double shippingcost = 0;
            foreach (var sh in transfer.Shippings)
            {
                Shipping existedShipping = db.Shippings.Find(sh.ID);
                if (existedShipping != null)
                {
                    db.Entry(existedShipping).State = EntityState.Detached;
                    db.Entry(sh).State = EntityState.Modified;
                }
                else
                {
                    db.Shippings.Add(sh);
                    db.TransferShippings.Add(new TransferShipping() { ID = Guid.NewGuid(), TransferID = transfer.ID, ShippingID = sh.ID });
                }

                shippingcost += sh.ShippingCost;
            }
            existedTransfer.ShippingCost = shippingcost;
            db.Entry(existedTransfer).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransferExists(transfer.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            existedTransfer.Shippings = await (from ts in db.TransferShippings
                                            from sh in db.Shippings
                                            where ts.TransferID== existedTransfer.ID && ts.ShippingID == sh.ID
                                            select sh).ToListAsync();
            existedTransfer.TransferedProducts= await db.TransferProductInventories.Where(tpi => tpi.TransferID== existedTransfer.ID).ToListAsync();
            foreach (var transferProduct in existedTransfer.TransferedProducts)
            {
                transferProduct.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == transferProduct.ProductInventoryID).FirstAsync();
                transferProduct.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == transferProduct.ProductInventory.SkuNumber).FirstAsync();
                transferProduct.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == transferProduct.ProductInventory.LocationID).FirstAsync();
            }

            return Ok(existedTransfer);
        }

        [Route("api/Transfers/TransferedProductInventory")]
        public async Task<IHttpActionResult> PostTransferedProductInventory(TransferProductInventory tpi)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var productInventory = db.ProductInventories.Find(tpi.ProductInventoryID);
            if (productInventory.QuantityOnHand < tpi.TransferedQuantity)
            {
                return BadRequest("Quantity is over added at current inventory location");
            }

            //update transfer product cost
            var transfer = db.Transfers.Find(tpi.TransferID);
            transfer.ProductCost += tpi.ProductInventory.PurchasedPrice * tpi.TransferedQuantity;
            db.Entry(transfer).State = EntityState.Modified;

            //update reserved quantity
            productInventory.TotalReservedQuantity += tpi.TransferedQuantity;
            db.Entry(productInventory).State = EntityState.Modified;
            tpi.ProductInventory = null;
            db.TransferProductInventories.Add(tpi);


            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TransferProductInventoryExist(tpi.ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            tpi.ProductInventory = await db.ProductInventories.Where(pi => pi.ID == tpi.ProductInventoryID).FirstAsync();
            tpi.ProductInventory.Product = await db.Products.Where(p => p.SkuNumber == tpi.ProductInventory.SkuNumber).FirstAsync();
            tpi.ProductInventory.InventoryLocation = await db.InventoryLocations.Where(l => l.ID == tpi.ProductInventory.LocationID).FirstAsync();
            return Ok(tpi);
        }

        [Route("api/Transfers/TransferedProductInventoryQuantity")]
        public async Task<IHttpActionResult> PutTransferedProductInventoryQuantity(Guid id, int transferedQuantity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tpi =await db.TransferProductInventories.FindAsync(id);
            var changeQuantity = transferedQuantity - tpi.TransferedQuantity;
            var productInventory = db.ProductInventories.Find(tpi.ProductInventoryID);
            //logic is wrong here
            //if (changeQuantity > 0 && productInventory.QuantityOnHand - changeQuantity < 0)
            //{
            //    return BadRequest();
            //}
            tpi.TransferedQuantity = transferedQuantity;
            productInventory.TotalReservedQuantity += changeQuantity;
            tpi.ProductInventory = productInventory;

            db.Entry(tpi).State = EntityState.Modified;
            db.Entry(productInventory).State = EntityState.Modified;

            //update transfer product cost
            var transfer =await db.Transfers.FindAsync(tpi.TransferID);
            transfer.ProductCost += tpi.ProductInventory.PurchasedPrice * changeQuantity;
            db.Entry(transfer).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransferProductInventoryExist(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(tpi);
        }

        [ResponseType(typeof(TransferProductInventory))]
        [Route("api/Transfers/TransferedProductInventory")]
        public IHttpActionResult DeleteTransferProductInventory(Guid id)
        {
            var tpi = db.TransferProductInventories.Find(id);
            if (tpi == null)
            {
                return NotFound();
            }
            var productInventory = db.ProductInventories.Find(tpi.ProductInventoryID);

            //update order product cost
            var transfer = db.Transfers.Find(tpi.TransferID);
            transfer.ProductCost -= productInventory.PurchasedPrice * tpi.TransferedQuantity;
            db.Entry(transfer).State = EntityState.Modified;

            productInventory.TotalReservedQuantity -= tpi.TransferedQuantity;
            db.Entry(productInventory).State = EntityState.Modified;

            db.TransferProductInventories.Remove(tpi);

            db.SaveChanges();

            return Ok(tpi);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TransferExists(Guid id)
        {
            return db.Transfers.Count(e => e.ID == id) > 0;
        }

        private bool TransferProductInventoryExist(Guid id)
        {
            return db.TransferProductInventories.Count(e => e.ID == id) > 0;
        }
    }
}