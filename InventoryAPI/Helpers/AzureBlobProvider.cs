﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace InventoryAPI.Helpers
{
    public class AzureBlobProvider
    {
        private CloudStorageAccount _storageAccount;

        private const string ProductImageContainerName = "productimages";

        static Size SMALL = new Size(200, 200);
        static Size MEDIUM = new Size(400, 400);
        static Size LARGE = new Size(800, 800);

        public AzureBlobProvider()
        {
            _storageAccount = CloudStorageAccount.Parse(System.Configuration.ConfigurationManager.ConnectionStrings["InventoryBlob"].ConnectionString);
        }

        public AzureBlobProvider(CloudStorageAccount DevelopmentStorageAccount)
        {
            _storageAccount = DevelopmentStorageAccount;
        }

        public CloudBlobClient BlobClient
        {
            get { return _storageAccount.CreateCloudBlobClient(); }
        }

        async public Task<string> UploadBlob(Stream stream, string fileName)
        {

            CloudBlobContainer container = this.BlobClient.GetContainerReference(ProductImageContainerName);
            container.CreateIfNotExists();
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            CloudBlockBlob blob = container.GetBlockBlobReference(fileName);

            await blob.UploadFromStreamAsync(stream);

            string baseUrl = _storageAccount.BlobEndpoint.ToString();
            if (!baseUrl.Trim().EndsWith("/"))
            {
                baseUrl += "/";
            }
            return baseUrl + ProductImageContainerName;
        }

        async public Task<ImageUploadResult> UploadImageAsync(Stream filestream, string mimeType)
        {
            ImageUploadResult response = new ImageUploadResult();
            response.Error = "";
            try
            {
                string mainName = Guid.NewGuid().ToString();
                string extension = MimeToExtension(mimeType);
                string result = "";


                foreach (ImageSize size in Enum.GetValues(typeof(ImageSize)))
                {
                    Stream imageCopy = new MemoryStream();
                    filestream.Position = 0;
                    await filestream.CopyToAsync(imageCopy).ConfigureAwait(false);
                    imageCopy.Position = 0;

                    if (size == ImageSize.Original)
                    {
                        result = await UploadBlob(imageCopy, MakeFileName(extension, mainName, size)).ConfigureAwait(false);
                        continue;
                    }

                    MemoryStream resizeMemory = new MemoryStream();
                    string resizeError = ResizeImageThumbnail(imageCopy, resizeMemory, size, extension);
                    if (resizeError != "")
                    {
                        response.Error = resizeError;
                        return response;
                    }

                    resizeMemory.Position = 0;
                    result = await UploadBlob(resizeMemory, MakeFileName(extension, mainName, size)).ConfigureAwait(false);
                }
                response.Data = new ImageSummary();
                response.Data.imageID = mainName;
                response.Data.extension = extension;
                response.Data.imageBaseUrl = result;
                return response;
            }
            catch (Exception e)
            {
                // Exception is handled in a specific way. That's why we do not use Global Exception Handler. We will fix exception handling later
                response.Error = "Image upload failed. " + e;
                return response;
            }
        }

        //async public Task<FileContent> DownloadBlobImage(Guid canvasID, string fileName, ImageSize size)
        //{
        //    string sizeExtension = "";
        //    if (size != ImageSize.Original)
        //    {
        //        sizeExtension = "_" + size.ToString()[0];
        //    }
        //    // Everything else is going to return the original version

        //    string[] temp = fileName.Split('.');
        //    if (temp.Length != 2)
        //    {
        //        return null;
        //    }

        //    string imageFormat = temp[1];
        //    if (imageFormat == "bmp")
        //    {
        //        sizeExtension = ""; // We do not support tif and bmp yet
        //    }

        //    fileName = temp[0] + sizeExtension + "." + imageFormat;

        //    string imagef = "";
        //    switch (imageFormat.ToLower())
        //    {
        //        case "jpeg":
        //        case "jpg":
        //            imagef = "image/jpeg";
        //            break;
        //        case "png":
        //            imagef = "image/png";
        //            break;
        //        case "gif":
        //            imagef = "image/gif";
        //            break;
        //        case "bmp":
        //            imagef = "image/bmp";
        //            break;
        //        case "tif":
        //            imagef = "image/tiff";
        //            break;
        //        default:
        //            return null;
        //    }

        //    MemoryStream memoryStream = new MemoryStream();

        //    // Download the encrypted blob.
        //    BlobEncryptionPolicy downloadPolicy = new BlobEncryptionPolicy(null, cachingResolver);
        //    // Set the decryption policy on the request options.
        //    BlobRequestOptions downloadOptions = new BlobRequestOptions() { EncryptionPolicy = downloadPolicy };
        //    try
        //    {
        //        CloudBlobContainer container = this.BlobClient.GetContainerReference(ImageContainerName);
        //        CloudBlockBlob blob = container.GetBlockBlobReference(fileName);
        //        await blob.DownloadToStreamAsync(memoryStream, null, downloadOptions, null);
        //    }
        //    catch (Exception e)
        //    {
        //        // Exception is handled in a specific way. That's why we do not use Global Exception Handler. We will fix exception handling later
        //        WebApiConfig._safeBugSnag.NotifyError(e, HttpContext.Current?.Request?.Url);
        //        return null;
        //    }
        //    return new FileContent(memoryStream, imagef);
        //}

        private string ResizeImageThumbnail(Stream original, Stream copy, ImageSize size, string extension)
        {
            try
            {
                extension = extension.TrimStart('.');
                string config = "";
                switch (size)
                {
                    case ImageSize.Small:
                        config = "width=" + SMALL.Width.ToString() + ";height=" + SMALL.Height.ToString() + ";format=" + extension + ";mode=max";
                        break;
                    case ImageSize.Medium:
                        config = "width=" + MEDIUM.Width.ToString() + ";height=" + MEDIUM.Height.ToString() + ";format=" + extension + ";mode=max";
                        break;
                    case ImageSize.Large:
                        config = "width=" + LARGE.Width.ToString() + ";height=" + LARGE.Height.ToString() + ";format=" + extension + ";mode=max";
                        break;
                    case ImageSize.Original:
                        return "";
                    default:
                        return "Invalid size requested.";
                }

                ImageResizer.Instructions settings = new ImageResizer.Instructions(config);
                ImageResizer.ImageJob job = new ImageResizer.ImageJob(original, copy, settings);
                job.Build();
                return "";
            }
            catch (Exception ex)
            {
                return "Resize failed. " + ex;
            }
        }

        private string MakeFileName(string extension, string mainName, ImageSize size)
        {
            string mark = "_";
            switch (size)
            {
                case ImageSize.Original:
                    mark = "";
                    break;
                case ImageSize.Large:
                    mark += "L";
                    break;
                case ImageSize.Medium:
                    mark += "M";
                    break;
                case ImageSize.Small:
                    mark += "S";
                    break;
                default:
                    break;
            }
            return mainName + mark + extension;
        }

        private string MimeToExtension(string mime)
        {
            switch (mime)
            {
                case "image/bmp":
                    return ".bmp";
                case "image/gif":
                    return ".gif";
                case "image/jpeg":
                case "image/jpg":
                    return ".jpg";
                case "image/png":
                    return ".png";
            }
            return "";
        }
    }



    public class ImageUploadResult
    {
        public string Error
        {
            get;
            set;
        }
        public ImageSummary Data
        {
            get;
            set;
        }
    }

    public class ImageSummary
    {
        public string imageID { get; set; }
        public string extension { get; set; }
        public string imageBaseUrl { get; set; }
    }

    public enum ImageSize
    {
        Small,
        Medium,
        Large,
        Original
    }
}