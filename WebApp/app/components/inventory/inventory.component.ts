/**
 * Created by yangli on 12/9/2016.
 */
import {Component, OnInit} from '@angular/core'

import {Product, ProductImage, ProductInventory, InventoryLocation, Transfer} from '../../models'
import {ProductService, ProductInventoryService, InventoryLocationService, ImageService} from '../../services'
import {GUID} from '../../utilities'
import {DateHelper} from "../../utilities";
import {FileUploader, FileItem} from 'ng2-file-upload/ng2-file-upload';
import any = jasmine.any;

declare var jQuery: any;

@Component({
  selector: 'app-inventory',
  templateUrl: 'app/components/inventory/inventory.component.html',
  styleUrls: ['app/components/inventory/inventory.component.css', 'styles.css'],
  providers: [ProductService, ProductInventoryService, InventoryLocationService, ImageService]
})

export class InventoryComponent implements OnInit {
  modalProduct: Product;
  isEditing: boolean;
  products: Product[];
  //inventoryLocations:InventoryLocation[];
  tableErrorMessage: string;
  newTransfer: Transfer;

  public imageUploader: FileUploader;


  constructor(private productService: ProductService,
              private productInventoryService: ProductInventoryService,
              private inventoryLocationService: InventoryLocationService,
              private imageService: ImageService) {

  }

  private loadModalWithProduct(loadProduct: Product): void {

    this.modalProduct = loadProduct;
    jQuery("#modalProduct").modal("show");
  }

  ngOnInit(): void {
    this.isEditing = false;
    this.modalProduct = new Product();
    this.products = [];
    //this.inventoryLocations=[];
    this.newTransfer = new Transfer();
    this.tableErrorMessage = "";

    this.imageUploader = new FileUploader({
      url: this.imageService.imageAPIURL,
      method: "POST",
      allowedMimeType: ['image/png', 'image/jpg', 'image/jpeg'],
      maxFileSize: 20 * 1024 * 1024, // 20 MB
      isHTML5: true
    });
    var self = this;
    this.imageUploader.onAfterAddingAll = function (fileItems: FileItem[]) {
      self.autoStartUploadImages(fileItems);
    };

    this.getAllProducts();
    //this.getAllInventoryLocations();

  }

  getAllProducts(): void {
    var self = this;
    this.productService.getAllProducts().then(
      function (products) {
        self.products = [];
        for (let p of products) {
          // if(!p.productImages){
          //   p.productImages=[];
          // }
          if (!p.productInventories) {
            p.productInventories = [];
          }
          for (let pi of p.productInventories) {
            pi.expiredDate = new Date(pi.expiredDate);
          }

          self.products.push(p);
        }
        if (self.products.length == 0) {
          self.tableErrorMessage = "暂无产品信息";
        }
      },
      function (error) {
        console.log("Error when loading all product info: " + error);
        self.tableErrorMessage = "无法读取产品资料,请稍后再试";
      });
  }

  // getAllInventoryLocations():void{
  //   var self=this;
  //   this.inventoryLocationService.getAllInventoryLocations().then(
  //     function(inventoryLocations){
  //       self.inventoryLocations=[];
  //       for(let il of inventoryLocations){
  //         self.inventoryLocations.push(il);
  //       }
  //     },
  //     function(error){
  //       console.log("Error when loading all Inventory Location info: "+error);
  //     }
  //   )
  // }

  onAddNewProduct(): void {
    this.isEditing = false;
    var newProduct = new Product();
    newProduct.weight = 0;
    newProduct.hasExpiredDate = false;
    this.loadModalWithProduct(newProduct);
  }

  saveNewProduct(prod: Product): void {
    jQuery("#modalProduct").modal("hide");

    let self = this;
    this.productService.addProduct(prod).then(
      function (product) {
        product.productInventories = [];
        self.products.push(product);

      },
      function (error) {
        console.log("Error when saving added new product: " + error);
      }
    )
  }

  onEditProduct(editProd: Product): void {
    this.isEditing = true;
    let product = JSON.parse(JSON.stringify(editProd));
    this.loadModalWithProduct(product);
  }

  saveEditProduct(editprod: Product): void {
    this.isEditing = false;
    jQuery("#modalProduct").modal("hide");

    var self = this;
    this.productService.updateProduct(editprod).then(
      function (prod) {
        self.updateProductInfo(prod);
      },
      function (error) {
        //todo: show error modal
        console.log("Error when saving edited product info: " + error);
      }
    )

  }

  // onTransferInventory():void{
  //   this.newTransfer=new Transfer();
  //   //this.newTransfer.fromInventoryLocationID=GUID.getNewGUIDString();
  //   jQuery("#modalTransferProducts").modal("show");
  //
  // }

  getTotalQuantity(prod: Product): number {
    let total = 0;
    for (let pi of prod.productInventories) {
      total += pi.quantityOnHand;
    }
    return total;
  }

  isExpiredInOneYear(expiredDate: string): boolean {
    return DateHelper.isExpiredInOneYear(expiredDate);
  }

  isExpiredAlready(expiredDate: string): boolean {
    return DateHelper.isExpiredAlready(expiredDate);
  }

  getClosestExpiredDate(product: Product): Date {
    var result: Date = new Date(9999, 12, 30);
    for (let record of product.productInventories) {
      var exDate = new Date(record.expiredDate);
      if (exDate.getTime() < result.getTime()) {
        result = exDate;
      }
    }

    if (result != new Date(9999, 12, 30)) {
      return result;
    }
    return null;
  }

  onWeightChange(weight: number) {
    this.modalProduct.weight = weight;
  }

  selectedFileOnChanged(event: any) {
    console.log(event.target.value);
  }

  autoStartUploadImages(FileItems: FileItem[]) {
    var self = this;
    for (let fileItem of FileItems) {
      this.imageService.postImage(fileItem._file, fileItem._file.type).then(
        function (response) {
          console.log(response);
          var productImage = new ProductImage();
          productImage.id = response.imageID;
          productImage.extension = response.extension;
          productImage.imageBaseUrl = response.imageBaseUrl;
          productImage.skuNumber = self.modalProduct.skuNumber;
          self.modalProduct.productImages = self.modalProduct.productImages || [];
          productImage.isDefault = !self.hasDefaultImage(self.modalProduct.productImages);
          self.modalProduct.productImages.push(productImage);
        }, function (error) {
          console.log(error);
        })
    }
  }

  getProductImageUrl(productImage: ProductImage, requiredSize: string = '_S') {
    return productImage.imageBaseUrl + '/' + productImage.id + requiredSize + productImage.extension;
  }

  getDefaultProductImage(product: Product) {
    for (let pi of product.productImages) {
      if (pi.isDefault) {
        return this.getProductImageUrl(pi);
      }
    }
  }

  onRemoveImageByIndex(index: number): void {
    var removedDefault = this.modalProduct.productImages[index].isDefault;
    this.modalProduct.productImages.splice(index, 1);
    if (removedDefault && this.modalProduct.productImages.length > 0) {
      this.modalProduct.productImages[0].isDefault = true;
    }
  }

  onSetDefaultImageByIndex(index: number): void {
    if (this.modalProduct.productImages[index].isDefault) {
      return; //do nothing if default already
    }
    for (let pi of this.modalProduct.productImages) {
      if (pi.isDefault) {
        pi.isDefault = false;
      }
    }
    this.modalProduct.productImages[index].isDefault = true;
  }

  private getProductBySku(sku: string): Product {
    for (let p of this.products) {
      if (p.skuNumber === sku) {
        return p;
      }
    }
    return null;
  }

  private updateProductInfo(newProd: Product): void {
    let foundProduct = this.getProductBySku(newProd.skuNumber);
    if (foundProduct != null) {
      foundProduct.name = newProd.name;
      foundProduct.brand = newProd.brand;
      foundProduct.hasExpiredDate = newProd.hasExpiredDate;
      foundProduct.weight = newProd.weight;
      foundProduct.productImages = newProd.productImages;
    }
  }

  private hasDefaultImage(productImages: ProductImage[]): boolean {
    for (let pi of productImages) {
      if (pi.isDefault) {
        return true;
      }
    }
    return false;
  }

}
