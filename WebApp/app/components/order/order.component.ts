/**
 * Created by yangli on 12/9/2016.
 */
import { Component,OnInit } from '@angular/core';
import { CompleterService, CompleterData } from 'ng2-completer';


import { Order,Customer,Product, OrderStatus,OrderProductInventory,PurchaseRequiredProduct,PaymentMethod,ExpressCompany, Shipping, ShippingStatus } from '../../models';
import {ProductService,OrderService,CustomerService,PaymentMethodService,ExpressCompanyService} from '../../services';
import {DateHelper} from '../../utilities';
import {ProductInventory} from "../../models/product-inventory.model";
import {GUID} from "../../utilities/GUID";
import {ShippingService} from "../../services/shipping.service";


declare var jQuery:any;

@Component({
  selector: 'app-order',
  templateUrl: 'app/components/order/order.component.html',
  styleUrls:['app/components/order/order.component.css','styles.css'],
  providers:[OrderService,ProductService,CustomerService,PaymentMethodService,ExpressCompanyService,ShippingService]

})

export class OrderComponent implements OnInit{
  OrderStatus=OrderStatus;
  ShippingStatus = ShippingStatus;
  allOrders: Order[];
  products: Product[];
  customers:Customer[];
  paymentMethods:PaymentMethod[];
  expressCompanies:ExpressCompany[];


  modalOrder: Order;
  modalCustomer: Customer;
  modalOrderExistedProduct:OrderProductInventory;
  modalOrderExistedProductExtra:PurchaseRequiredProduct;
  modalPurchaseRequiredProduct:PurchaseRequiredProduct;
  modalExistedProduct: Product;
  modalSelectAllProductInventory:boolean;
  modalAllPurchaseRequiredItems:PurchaseRequiredProduct[];
  modalNewShipping:Shipping;
  modalGeneralTitle:string;
  modalGeneralBody:string;


  tableErrorMessage:string;

  private showingOrder:OrderStatus;
  private isEditing: boolean;
  private isEditingModalCost:boolean;
  allCollapse:boolean;

  private autoCompleteCustomerService: CompleterData;
  searchCustomerStr:string;

  private autoCompleteProductService: CompleterData;
  searchProductStr:string;

  constructor(
    private completerService: CompleterService,
    private orderService: OrderService,
    private productService: ProductService,
    private customerService: CustomerService,
    private paymentService: PaymentMethodService,
    private expressCompanyService: ExpressCompanyService,
    private shippingService: ShippingService

  ){}

  ngOnInit(): void {
    this.showingOrder=OrderStatus.Preparing;
    this.allOrders=[];
    this.products=[];
    this.customers=[];
    this.paymentMethods=[];
    this.expressCompanies=[];
    this.isEditing=false;
    this.isEditingModalCost=false;

    this.modalOrder=new Order();
    this.modalCustomer = new Customer();
    this.modalOrderExistedProduct= null;
    this.modalOrderExistedProductExtra=null;
    this.modalPurchaseRequiredProduct=null;
    this.modalExistedProduct=null;
    this.modalSelectAllProductInventory=false;
    this.modalAllPurchaseRequiredItems=[];
    this.modalNewShipping=new Shipping();

    this.tableErrorMessage="";
    this.allCollapse=true;

    this.searchCustomerStr='';
    this.autoCompleteCustomerService=this.completerService.local(this.customers,'name,cellPhone,address','name,cellPhone').descriptionField('address');

    this.searchProductStr='';
    this.autoCompleteProductService=this.completerService.local(this.products,'name,brand,skuNumber','name,brand').descriptionField("skuNumber");

    this.getAllOrders();
    this.loadAllProducts();
    this.loadAllCustomers();
    this.getAllPaymentMethods();
    this.getAllExpressCompanies();
  }

  getAllOrders():void{
    var self =this;
    this.orderService.getAllOrders().then(
      function(orders){
        for(let order of orders){
          order.placedDate=new Date(order.placedDate);
          order.paidDate=new Date(order.paidDate);
          order.closedDate=new Date(order.closedDate);
          for (let oep of order.orderedExistedProducts) {
            oep.productInventory.expiredDate = new Date(oep.productInventory.expiredDate);
          }

          for (let sh of order.shippings) {
            sh.shippedDate = new Date(sh.shippedDate);
            sh.receivedDate = new Date(sh.receivedDate);
          }
          if(!order.note){
              order.note="";
          }
          self.allOrders.push(order);
        }
      },
      function(error){
        console.log("Error when loading all customers info: "+error);
        self.tableErrorMessage="无法读取订单信息,请稍后再试";
      });
  }

  loadAllProducts():void{
    var self=this;
    this.productService.getAllProducts().then(
      function(products){
        for(let prod of products) {
          if(!prod.productInventories){
            prod.productInventories=[];
          }
          for(let pi of prod.productInventories){
            pi.expiredDate=new Date(pi.expiredDate);
          }
          self.products.push(prod);
        }
      },
      function(error){
        console.log("Error when loading all product info: "+error);
        //self.tableErrorMessage="无法读取产品信息,请稍后再试";
      }
    );
  }

  loadAllCustomers(): void{
    var self=this;
    this.customerService.getAllCustomers().then(
      function(customers){
        for( let cu of customers) {
          self.customers.push(cu);
        }
        if(self.customers.length==0){
        }
      },
      function (error) {
        console.log("Error when loading all customers info: "+error);
      });
  }

  getAllPaymentMethods():void{
      var self=this;
      this.paymentService.getAllPaymentMethods().then(
        function(paymentMethods){
          for(let pm of paymentMethods){
            self.paymentMethods.push(pm);
          }
        },
        function(error){
          console.log("Error when loading all payment methods: "+error);
        }
      )
  }

  getAllExpressCompanies():void{
    var self=this;
    this.expressCompanyService.getAllExpressCompanies().then(
      function(expressCompanies){
        for(let ec of expressCompanies){
          self.expressCompanies.push(ec);
        }
      },
      function(error){
        console.log("Error when loading all express companies: "+error);
      }
    )
  }

  getOrdersByStatus(status:OrderStatus):Order[]{
    if(status === OrderStatus.Preparing) {
      return this.allOrders.filter(o => o.status === status || o.status === OrderStatus.ReadyToShip)
    }
    else if(status === OrderStatus.Shipped){
      return this.allOrders.filter(o => o.status === status)
    }
    else{
      return this.allOrders.filter(o => o.status === status )
    }
  }


  onAddNewOrder():void{
    this.modalOrder=new Order();
    //this.modalOrder.closedDate=null;
    //this.modalOrder.paidDate=null;
    this.modalOrder.placedDate=new Date();
    this.modalCustomer=null;
    this.searchCustomerStr='';
    jQuery("#modalAddOrder").modal("show");
  }

  onNewOrderCustomerSelected(selectedCustomer:any):void{
    this.modalOrder.customer=selectedCustomer?selectedCustomer.originalObject:null;
    this.searchCustomerStr="";
  }

  confirmAddNewOrder(modalOrder: Order):void{
    jQuery("#modalAddOrder").modal("hide");
    modalOrder.customerID=modalOrder.customer.id;

    var self = this;
    this.orderService.addOrder(modalOrder).then(
      function(savedOrder){
        savedOrder.placedDate=new Date(savedOrder.placedDate);
        savedOrder.paidDate=new Date(savedOrder.paidDate);
        savedOrder.closedDate=new Date(savedOrder.closedDate);
        savedOrder.shippings=[];
        self.allOrders.push(savedOrder);
      },
      function(error){
        console.log("Error when adding new order: "+error);
      }
    )
  }

  onSwitchReadyToShipOrder(selectedOrder: Order):void{
      selectedOrder.status=selectedOrder.status==OrderStatus.Preparing?OrderStatus.ReadyToShip:OrderStatus.Preparing;
      this.orderService.updateOrder(selectedOrder).then(
        function(updatedOrder){

        },
        function(error){

        }
      )
  }

  onSwitchIsPaid(selectedOrder:Order):void{
    selectedOrder.isPaid=!selectedOrder.isPaid;
    this.orderService.updateOrder(selectedOrder).then(
      function(updatedOrder){

      },
      function(error){

      }
    )
  }

  onPayOrder(selectedOrder:Order):void{
    this.loadModalWithSelectedOrder(selectedOrder);
    this.modalOrder.paymentMethodID=GUID.getNewGUIDString();
    jQuery("#modalPayOrder").modal("show");
  }

  // confirmOrderCost(modalOrder: Order):void{
  //   jQuery("#modalOrderCost").modal("hide");
  //
  //   //modalOrder.paymentMethod = this.findPaymentMethodByID(modalOrder.paymentMethodID);
  //
  //   var self=this;
  //   this.orderService.updateOrder(modalOrder).then(
  //     function(updatedOrder){
  //       self.updateEntireOrder(updatedOrder);
  //     },
  //     function(error){
  //       console.log("Error when updating order cost info: "+error);
  //     }
  //   )
  //
  // }
  // }

  confirmPaidOrder():void{
    jQuery("#modalPayOrder").modal("hide");
    this.modalOrder.isPaid=true;
      var self=this;
      this.orderService.updateOrder(this.modalOrder).then(
        function(updatedOrder){
          self.updateEntireOrder(updatedOrder);
        },
        function(error){
          console.log("Error when updating order cost info: "+error);
        }
      )

  }

  onDeleteOrder(selectedOrder: Order):void{
    this.loadModalWithSelectedOrder(selectedOrder);
    for(let opi of this.modalOrder.orderedExistedProducts){
      opi.productInventory.expiredDate = new Date(opi.productInventory.expiredDate);
    }
    //this.modalOrder.shippings=[];
    jQuery("#modalDeleteOrder").modal("show");
  }

  confirmDeleteOrder(selectedOrder: Order):void{
    jQuery("#modalDeleteOrder").modal("hide");
    var self =this;
    this.orderService.deleteOrder(selectedOrder).then(
      function(deletedOrder){
        var targetOrder = self.findOrderByID(deletedOrder.id);
        self.allOrders=self.allOrders.filter(o=>o.id!=deletedOrder.id);

        //update product inventory tree
        for(let opi of targetOrder.orderedExistedProducts){
          opi.productInventory.totalReservedQuantity -= opi.reservedQuantity;
          self.updateProductInventoryInProductTree(opi.productInventory);
        }

      },
      function(error){
        console.log("Error when deleting order : "+selectedOrder+" Error: "+error);
      }
    )
  }

  onShipOrder(selectedOrder:Order):void{
      if(selectedOrder.status==OrderStatus.Preparing){
        this.loadModalWithGeneralMessage("订单尚未完成备货",'请先标注此订单"已完成备货"');
        return;
      }
      this.loadModalWithSelectedOrder(selectedOrder);
      this.modalNewShipping=new Shipping();
      jQuery("#modalShipOrder").modal("show");
  }



  onModalShippingCostChange(shippingCost:number):void{
      this.modalNewShipping.shippingCost=shippingCost;
  }

  onAddNewExpressPackageToModalOrder():void{
      this.modalNewShipping.shippedDate=new Date();
      this.modalNewShipping.shippingStatus=ShippingStatus.Shipped;
      this.modalOrder.shippings.push(this.modalNewShipping);
      this.modalNewShipping=new Shipping();
  }

  onRemoveExpressPackageFromModalOrder(selectedShipping:Shipping):void{
      this.modalOrder.shippings=this.modalOrder.shippings.filter(s=>s.id!=selectedShipping.id);
  }

  confirmShippedOrder():void{
    jQuery("#modalShipOrder").modal("hide");
      var self=this;

      this.orderService.shipOrder(this.modalOrder).then(
        function(shippedOrder){
          self.updateEntireOrder(shippedOrder);
          // for(let sh of shippedOrder.shippings){
          //   sh.shippedDate= new Date(sh.shippedDate);
          // }
        },
        function(error){
          console.log("Error when shipping order : "+self.modalOrder+" Error: "+error);
        }
      )
  }

  onEditShipping(selectedOrder:Order):void{
    this.loadModalWithSelectedOrder(selectedOrder);
    this.modalNewShipping=new Shipping();
    jQuery("#modalEditShipping").modal("show");
  }

  confirmEditShippingInfo():void{
    jQuery("#modalEditShipping").modal("hide");
    var self =this;
    this.orderService.updateShipping(this.modalOrder).then(
      function(updatedOrder){
        self.updateEntireOrder(updatedOrder);
      }
    )
  }

  onCompleteOrder(selectedOrder:Order):void{
    if(!this.hasAllPackagesArrived(selectedOrder)){
      this.loadModalWithGeneralMessage("此订单尚未完成所有包裹配送",'请先标注此订单"包裹全部到达"');
      return;
    }

    var self = this;
    selectedOrder.closedDate=new Date();
    selectedOrder.status=OrderStatus.Closed;
    this.orderService.updateOrder(selectedOrder).then(
      function(updatedOrder){
        self.updateEntireOrder(updatedOrder);
      }
    )
  }

  onReportProblem(selectedOrder:Order):void{
    var self = this;
    //selectedOrder.closedDate=new Date();
    selectedOrder.status=OrderStatus.HasProblems;
    this.orderService.updateOrder(selectedOrder).then(
      function(updatedOrder){
        self.updateEntireOrder(updatedOrder);
      }
    )
  }

  onPackageReceived(selectedPackage:Shipping):void{
    selectedPackage.shippingStatus =
      selectedPackage.shippingStatus == ShippingStatus.Received
        ? ShippingStatus.Shipped:ShippingStatus.Received;
    selectedPackage.receivedDate =
      selectedPackage.shippingStatus == ShippingStatus.Received
        ? new Date():null;
    this.shippingService.putShipping(selectedPackage).then(
      function(updatedShipping){
      },
      function (error) {
        console.log("Error when updating shipping info: "+error);
      }
    )
  }

  onPackageHasProblem(selectedPackage:Shipping):void{
    selectedPackage.shippingStatus =
    selectedPackage.shippingStatus == ShippingStatus.HasProblems
      ? ShippingStatus.Shipped:ShippingStatus.HasProblems;
    this.shippingService.putShipping(selectedPackage).then(
      function(updatedShipping){

      },
      function (error) {
        console.log("Error when updating shipping info: "+error);
      }
    )
  }

  hasAllPackagesArrived(targetOrder:Order):boolean{
      for(let sh of targetOrder.shippings){
        if(sh.shippingStatus!=ShippingStatus.Received){
          return false;
        }
      }
      return true;
  }

  // onSwitchAllPackageArrived(selectedOrder:Order):void{
  //     var hasAllArrived =this.hasAllPackagesArrived(selectedOrder);
  //
  //     for(let sh of selectedOrder.shippings){
  //       sh.shippingStatus=hasAllArrived?ShippingStatus.Received:ShippingStatus.Shipped;
  //       sh.receivedDate=hasAllArrived?new Date():null;
  //       this.shippingService.putShipping(sh).then(
  //         function(updatedShipping){
  //         },
  //         function (error) {
  //           console.log("Error when updating shipping info: "+error);
  //         }
  //       )
  //     }
  // }

  private loadModalWithSelectedOrder(selectedOrder: Order): void{
    this.modalOrder = JSON.parse(JSON.stringify(selectedOrder));
    this.modalOrder.paidDate = new Date(selectedOrder.paidDate);
    this.modalOrder.closedDate = new Date(selectedOrder.closedDate);
    this.modalOrder.placedDate = new Date(selectedOrder.placedDate);
    if(this.modalOrder.orderedExistedProducts) {
      for (let oep of this.modalOrder.orderedExistedProducts) {
        oep.productInventory.expiredDate = new Date(oep.productInventory.expiredDate);
      }
    }

  }

  private loadModalWithGeneralMessage(title:string,bodyMessage:string):void{
      this.modalGeneralTitle=title?title:"";
      this.modalGeneralBody=bodyMessage?bodyMessage:"";
      jQuery("#modalGeneralMessage").modal("show");

  }

  onEditOrderCustomer(selectedOrder: Order):void{
    this.isEditing=true;
    this.modalCustomer=null;
    this.searchCustomerStr='';

    this.loadModalWithSelectedOrder(selectedOrder);
    jQuery("#modalOrderCustomer").modal("show");
  }

  onCustomerSelected(selectedCustomer:any):void{
    this.modalCustomer=selectedCustomer?selectedCustomer.originalObject:null;
    this.searchCustomerStr="";
  }

  confirmSelectedCustomer(selectedCustomer:Customer):void{
    jQuery("#modalOrderCustomer").modal("hide");
    this.isEditing=false;
    this.modalOrder.customerID=selectedCustomer.id;
    this.modalOrder.customer=selectedCustomer;
    var self =this;
    this.orderService.updateOrder(this.modalOrder).then(
      function(returnOrder){
        var order = self.findOrderByID(returnOrder.id);
        order.customerID=returnOrder.customerID;
        order.customer=returnOrder.customer;
      },
      function(error){
        console.log("Error when updating order customer info: "+error);
      }
    )
  }

  onAddOrderProducts(selectedOrder:Order):void{
    this.searchProductStr='';
    this.loadModalWithSelectedOrder(selectedOrder);

    this.modalOrder.orderedExistedProducts=[];
    this.modalOrder.purchaseRequiredProducts=[];

    this.modalExistedProduct=null;
    this.modalOrderExistedProductExtra=null;
    this.modalPurchaseRequiredProduct=null;
    this.modalSelectAllProductInventory=false;
    jQuery("#modalAddOrderProduct").modal("show");
  }

  onFoundProductSelected(selectedProd:any):void{
    var skuNumber = selectedProd.description;
    this.modalExistedProduct = this.findProductBySku(skuNumber);

    this.modalOrderExistedProductExtra=new PurchaseRequiredProduct();
    this.modalOrderExistedProductExtra.name = this.modalExistedProduct.name+' - '+this.modalExistedProduct.brand;
    this.modalOrderExistedProductExtra.skuNumber = this.modalExistedProduct.skuNumber;
    this.modalOrderExistedProductExtra.note="";
    this.modalOrderExistedProductExtra.quantity=0;

    for(let pi of this.modalExistedProduct.productInventories){
      var newOPI = new OrderProductInventory();
      newOPI.id=GUID.getNewGUIDString();
      newOPI.orderID=this.modalOrder.id;
      newOPI.productInventoryID=pi.id;
      newOPI.productInventory=pi;
      newOPI.productInventory.expiredDate = new Date(pi.expiredDate);

      newOPI.productInventory.product=new Product();
      newOPI.productInventory.product.skuNumber=this.modalExistedProduct.skuNumber;
      newOPI.productInventory.product.hasExpiredDate=this.modalExistedProduct.hasExpiredDate;
      newOPI.productInventory.product.name=this.modalExistedProduct.name;
      newOPI.productInventory.product.brand=this.modalExistedProduct.brand;
      newOPI.productInventory.product.weight=this.modalExistedProduct.weight;
      newOPI.orderedQuantity=0;


      this.modalOrder.orderedExistedProducts.push(newOPI);
    }

  }

  onAddPurchaseRequiredProduct():void{
    this.modalPurchaseRequiredProduct=null;
    this.modalPurchaseRequiredProduct=new PurchaseRequiredProduct();
    this.modalPurchaseRequiredProduct.quantity=1;
    this.modalPurchaseRequiredProduct.skuNumber="";
    this.modalPurchaseRequiredProduct.note="";
    this.modalPurchaseRequiredProduct.orderID=this.modalOrder.id;
  }
  onPurchaseRequiredProductQuantityChange(quantity:number):void{
    this.modalPurchaseRequiredProduct.quantity=quantity;
  }

  confirmAddPurchaseRequiredProduct():void{
    jQuery("#modalAddOrderProduct").modal("hide");

    var targetOrder = this.findOrderByID(this.modalPurchaseRequiredProduct.orderID);
    this.orderService.addPurchaseRequiredProduct(this.modalPurchaseRequiredProduct).then(
      function (savedPrp) {
        targetOrder.purchaseRequiredProducts.push(savedPrp);
      },
      function(error){
        console.log("Error when adding purchaseRequiredProduct: "+error);
      });
  }

  onOrderExistedProductExtraQuantityChange(quantity:number):void{
    this.modalOrderExistedProductExtra.quantity=quantity;
  }

  confirmAddExistedOrderProduct():void{
    jQuery("#modalAddOrderProduct").modal("hide");

    var targetOrder = this.findOrderByID(this.modalOrder.id);
    var self = this;
    for(let opi of this.modalOrder.orderedExistedProducts){
      if(opi.orderedQuantity>0){
        var existedOPI = this.findOrderProductInventoryByProductInventoryID(opi.productInventoryID,targetOrder.orderedExistedProducts);
        if(!existedOPI) {
          this.orderService.addOrderProductInventory(opi).then(
            function(savedOPI){
              savedOPI.productInventory.expiredDate = new Date(savedOPI.productInventory.expiredDate);
              targetOrder.orderedExistedProducts.push(savedOPI);
              self.updateProductInventoryInProductTree(savedOPI.productInventory);
              self.selfUpdateOrderBalance(targetOrder);
            },
            function(error){
              console.log("Error when adding orderproductinventory info: "+error);
            }
          )

        }
        else{
          this.orderService.updateOrderProductInventoryQuantity(existedOPI,existedOPI.orderedQuantity+opi.orderedQuantity).then(
            function(savedOPI){
              existedOPI=self.findOrderProductInventoryByProductInventoryID(savedOPI.productInventoryID,targetOrder.orderedExistedProducts);
              existedOPI.orderedQuantity=savedOPI.orderedQuantity;
              existedOPI.reservedQuantity=savedOPI.reservedQuantity;
              self.updateProductInventoryInProductTree(savedOPI.productInventory);
              self.selfUpdateOrderBalance(targetOrder);
            },
            function(error){
              console.log("Error when updating orderproductinventory quantity: "+error);
            }
          );

        }
      }
    }


    if(this.modalOrderExistedProductExtra.quantity>0){
      this.modalOrderExistedProductExtra.orderID=targetOrder.id;
      this.orderService.addPurchaseRequiredProduct(this.modalOrderExistedProductExtra).then(
        function(savedPrp){
          for(let prp of targetOrder.purchaseRequiredProducts){
            if(prp.id === savedPrp.id){
              prp.quantity=savedPrp.quantity;
              return;
            }
          }
          targetOrder.purchaseRequiredProducts.push(savedPrp);
        },
        function(error){
          console.log("Error when adding purchase required existed product to the shopping list: "+error);
        }
      );
    }
  }




  onEditOrderedExistedProduct(orderExistedProduct:OrderProductInventory):void{
    var productInventory = this.findProductInventoryByID(orderExistedProduct.productInventoryID);
    this.modalOrderExistedProduct=JSON.parse(JSON.stringify(orderExistedProduct));
    this.modalOrderExistedProduct.productInventory.totalReservedQuantity=productInventory.totalReservedQuantity;
    this.modalOrderExistedProduct.productInventory.quantityOnHand=productInventory.quantityOnHand;
    this.modalOrderExistedProduct.productInventory.expiredDate = new Date(productInventory.expiredDate);
    jQuery("#modalOrderExistedProduct").modal("show");
  }

  onOrderQuantityChange(orderQuantity:number):void{
    var change =this.modalOrderExistedProduct.orderedQuantity-orderQuantity;
    this.modalOrderExistedProduct.orderedQuantity=orderQuantity;
    this.modalOrderExistedProduct.productInventory.totalReservedQuantity-=change;
  }

  onUpdateOrderQuantity(orderQuantity:number):number{
    return orderQuantity;
  }

  confirmOrderQuantityOnExistedProduct(orderExistedProduct:OrderProductInventory):void{
    jQuery("#modalOrderExistedProduct").modal("hide");
    var self =this;
    this.orderService.updateOrderProductInventoryQuantity(orderExistedProduct,orderExistedProduct.orderedQuantity).then(
      function(savedOPI){
        var order = self.findOrderByID(savedOPI.orderID);
        for(let opi of order.orderedExistedProducts){
          if(opi.id===savedOPI.id){
            opi.orderedQuantity=savedOPI.orderedQuantity;
            opi.reservedQuantity=savedOPI.reservedQuantity;
            opi.productInventory.quantityOnHand=savedOPI.productInventory.quantityOnHand;
            opi.productInventory.totalReservedQuantity=savedOPI.productInventory.totalReservedQuantity;
          }
        }

        //need to update product tree about savedOPI
        self.updateProductInventoryInProductTree(savedOPI.productInventory);
        self.selfUpdateOrderBalance(order);
      },
      function(error){
        console.log("Error when updating orderproductinventory quantity: "+error);
      }
    );
  }

  onDeleteOrderedExistedProduct(orderExistedProduct:OrderProductInventory):void{
    this.modalOrderExistedProduct = JSON.parse(JSON.stringify(orderExistedProduct));
    this.modalOrderExistedProduct.productInventory.expiredDate = new Date(orderExistedProduct.productInventory.expiredDate);
    jQuery("#modalDeleteOrderExistedProduct").modal("show");
  }

  confirmDeleteOrderQuantityOnExistedProduct(orderExistedProduct:OrderProductInventory):void{
    jQuery("#modalDeleteOrderExistedProduct").modal("hide");
    var self=this;
    this.orderService.deleteOrderProductInventory(orderExistedProduct).then(
      function () {
        var order = self.findOrderByID(orderExistedProduct.orderID);
        order.orderedExistedProducts=order.orderedExistedProducts.filter(opi=>opi.id!=orderExistedProduct.id);
        //update product tree
        orderExistedProduct.productInventory.totalReservedQuantity-=orderExistedProduct.reservedQuantity;
        self.updateProductInventoryInProductTree(orderExistedProduct.productInventory);
        self.selfUpdateOrderBalance(order);
      },
      function(error){
        console.log("Error when deleting orderproductinventory: "+error);
      }
    );
  }

  onRemovePurchasedRequiredProduct(prp: PurchaseRequiredProduct):void{
    var self = this;
    this.orderService.deletePurchaseRequiredProduct(prp).then(
      function(){
        var order = self.findOrderByID(prp.orderID);
        order.purchaseRequiredProducts = order.purchaseRequiredProducts.filter(p=>p.id!=prp.id);
      },
      function(error){
        console.log("Error when deleting purchaseRequiredProducts: "+error);
      }
    )
  }

  onEditCostInfo(selectedOrder: Order):void{
    this.isEditing=true;
    this.loadModalWithSelectedOrder(selectedOrder);
    this.modalOrder.paymentMethodID=GUID.getNewGUIDString();
    jQuery("#modalOrderCost").modal("show");
  }

  confirmOrderCost(modalOrder: Order):void{
    jQuery("#modalOrderCost").modal("hide");

    //modalOrder.paymentMethod = this.findPaymentMethodByID(modalOrder.paymentMethodID);

    var self=this;
    this.orderService.updateOrder(modalOrder).then(
      function(updatedOrder){
        self.updateEntireOrder(updatedOrder);
      },
      function(error){
        console.log("Error when updating order cost info: "+error);
      }
    )

  }

  onOrderShippingCostChange(shippingCost:number):void{
    this.modalOrder.shippingCost=shippingCost;
  }

  onOrderBillingChange(Billing:number):void{
    this.modalOrder.billing=Billing;
  }

  onOrderPaidChange(paid:number):void{
    this.modalOrder.paid=paid;
  }

  private updateProductInventoryInProductTree(pi:ProductInventory):void{
    for(let prod of this.products){
      for(let item of prod.productInventories){
        if(pi.id === item.id){
          item.totalReservedQuantity = pi.totalReservedQuantity;
        }
      }
    }
  }

  // private removeProductInventoryInProductTree(pi:ProductInventory):void{
  //   for(let prod of this.products){
  //     prod.productInventories =prod.productInventories.filter(productInvt => pi.id!=productInvt.id);
  //   }
  // }

  private findPaymentMethodByID(id:string):PaymentMethod{
      for(let pm of this.paymentMethods){
        if(pm.id===id){
          return pm;
        }
      }
      return null;
  }

  private findProductBySku(skuNumber:string):Product{
    for(let prod of this.products){
      if(prod.skuNumber==skuNumber){
        return prod;
      }
    }
    return null;
  }

  private findProductInventoryByID(id:string):ProductInventory{
    for(let prod of this.products){
      for(let pi of prod.productInventories){
        if(pi.id===id){
          return pi;
        }
      }
    }
    return null;
  }

  private findOrderByID(id:string):Order{
    for(let order of this.allOrders){
      if(order.id==id){
        return order;
      }
    }
    return null;
  }

  private findOrderProductInventoryByProductInventoryID(id:string,opis:OrderProductInventory[]):OrderProductInventory{
    for(let opi of opis ){
      if(opi.productInventoryID===id){
        return opi;
      }
    }
    return null;
  }


  getOrderNumber(status:OrderStatus):number{
    return this.getOrdersByStatus(status).length;
  }

  setShowOrderStatus(status: OrderStatus):void{
    if(this.showingOrder!==status) {
      this.showingOrder = status;
      this.allCollapse=true;
      this.forceAllCollapse();
    }
  }

  getShowOrderStatus():OrderStatus{
    return this.showingOrder;
  }

  toggleAllCollapse():void{
    this.allCollapse=!this.allCollapse;
    var currentShowStatus = this.getShowOrderStatus();
    var currentShowOrders=this.getOrdersByStatus(currentShowStatus);
    for(let order of currentShowOrders){
      var collapseCommand = this.allCollapse?'hide':'show';
      jQuery("#preparing-"+order.id).collapse(collapseCommand);
      jQuery("#shipped-"+order.id).collapse(collapseCommand);
      jQuery("#completed-"+order.id).collapse(collapseCommand);
      jQuery("#problem-"+order.id).collapse(collapseCommand);
    }
  }

  forceAllCollapse():void{
    var currentShowStatus = this.getShowOrderStatus();
    var currentShowOrders=this.getOrdersByStatus(currentShowStatus);
    for(let order of currentShowOrders){
      jQuery("#"+order.id).collapse('hide');
    }
  }

  private selfUpdateOrderBalance(order:Order):void{
    var totalProductCost=0;
    var totalShippingWeight=0;
    for(let opi of order.orderedExistedProducts){
      totalProductCost+=opi.reservedQuantity*opi.productInventory.purchasedPrice;
      totalShippingWeight+=opi.reservedQuantity*opi.productInventory.product.weight;
    }
    order.productCost=totalProductCost;
    order.totalWeight=totalShippingWeight;

  }

  private updateEntireOrder(newOrder:Order):void{
    let foundOrder = this.findOrderByID(newOrder.id);
    if(foundOrder!=null){
      foundOrder.customerID=newOrder.customerID;
      foundOrder.customer=newOrder.customer;
      foundOrder.status=newOrder.status;
      foundOrder.totalWeight=newOrder.totalWeight;
      foundOrder.productCost=newOrder.productCost;
      foundOrder.shippingCost=newOrder.shippingCost;
      foundOrder.billing=newOrder.billing;
      foundOrder.paid=newOrder.paid;
      foundOrder.isPaid=newOrder.isPaid;
      foundOrder.paymentMethodID=newOrder.paymentMethodID;
      foundOrder.note=newOrder.note;
      if(!foundOrder.note){
        foundOrder.note="";
      }
      foundOrder.placedDate=new Date(newOrder.placedDate);
      foundOrder.closedDate=new Date(newOrder.closedDate);
      foundOrder.paidDate=new Date(newOrder.paidDate);
      foundOrder.orderImages=newOrder.orderImages;
      foundOrder.orderedExistedProducts=[];
      foundOrder.purchaseRequiredProducts=[];
      foundOrder.shippings=[];
      for (let oep of newOrder.orderedExistedProducts) {
        oep.productInventory.expiredDate = new Date(oep.productInventory.expiredDate);
        foundOrder.orderedExistedProducts.push(oep);
      }

      for(let prp of newOrder.purchaseRequiredProducts){
        foundOrder.purchaseRequiredProducts.push(prp);
      }

      for (let sh of newOrder.shippings) {
        if(sh.receivedDate) {
          sh.receivedDate = new Date(sh.receivedDate);
        }
        if(sh.shippedDate) {
          sh.shippedDate = new Date(sh.shippedDate);
        }
        foundOrder.shippings.push(sh);
      }
    }
  }


  getTotalQuantityOnHand(productSKU: string):number{
    let total=0;
    var product = this.findProductBySku(productSKU);
    for(let pi of product.productInventories){
      total+=pi.quantityOnHand;
    }
    return total;
  }

  getTotalReserved(productSKU: string):number{
    let total=0;
    var product = this.findProductBySku(productSKU);
    for(let pi of product.productInventories){
      total+=pi.totalReservedQuantity;
    }
    return total;
  }



  getTotalQuantityOrdered(OrderProductInventories: OrderProductInventory[]):number{
    let total=0;
    if(!OrderProductInventories){
      return 0;
    }
    for(let opi of OrderProductInventories){
        total+=opi.orderedQuantity;
    }
    total+=this.modalOrderExistedProductExtra.quantity;
    return total;
  }

  onGetAllPurchaseItems():void{
    this.modalAllPurchaseRequiredItems=[];
    for(let order of this.allOrders){
      if(order.status!=OrderStatus.Preparing){
        continue;
      }
      for(let prp of order.purchaseRequiredProducts){
        if(prp.skuNumber&&prp.skuNumber!=""){ //has sku
          var existedPrps = this.modalAllPurchaseRequiredItems.filter(p =>p.skuNumber===prp.skuNumber);
          if(existedPrps.length==0){
            this.modalAllPurchaseRequiredItems.push(JSON.parse(JSON.stringify(prp)));
          }
          else{
            existedPrps[0].quantity+=prp.quantity;
          }
        }
        else{
          this.modalAllPurchaseRequiredItems.push(JSON.parse(JSON.stringify(prp)));
        }
      }
    }

    jQuery("#modalShowAllPurchaseRequiredItems").modal("show");

  }

  onRemovePurchasedRequiredProductOnList(selectedPrp: PurchaseRequiredProduct):void{
    this.modalAllPurchaseRequiredItems = this.modalAllPurchaseRequiredItems.filter(p=>p.id!=selectedPrp.id);
  }

  getClosestExpiredDate(product:Product):Date{
    var result:Date=new Date(9999,12,30);
    for(let record of product.productInventories){
      var exDate = new Date(record.expiredDate);
      if(exDate.getTime()<result.getTime()){
        result=exDate;
      }
    }
    if(result!=new Date(9999,12,30)){
      return result;
    }
    return null;
  }

  getOrderShippedDate(order:Order):Date{
      var result:Date= new Date(9999,12,30);
      for(let sh of order.shippings){
        if(result.getTime()>sh.shippedDate.getTime()){
          result=sh.shippedDate;
        }
      }
    if(result!=new Date(9999,12,30)){
      return result;
    }
    return null;
  }

  getTotalShippingCostByOrder(order:Order):number{
      var result=0;
      for(let shipping of order.shippings){
        result+=shipping.shippingCost;
      }
      return result;
  }

  getTotalExpressPackageByOrderID(orderID:string):number{
    var order = this.findOrderByID(orderID);
    return order.shippings.length;
  }

  findExpressCompanyByID(targetID:string):ExpressCompany{
      for(let ec of this.expressCompanies){
        if(ec.id === targetID){
          return ec;
        }
      }
      return null;
  }

  convertLocalDateToUTC(dateString:string): Date{
    return DateHelper.convertLocalDateToUTC(dateString);
  }
  isExpiredInOneYear(expiredDate:string):boolean{
    return DateHelper.isExpiredInOneYear(expiredDate);
  }
  isExpiredAlready(expiredDate:string):boolean{
    return DateHelper.isExpiredAlready(expiredDate);
  }
}
