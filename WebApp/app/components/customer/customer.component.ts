/**
 * Created by yangli on 12/9/2016.
 */
import { Component, OnInit } from '@angular/core'

import { GUID } from '../../utilities'
import { Customer } from '../../models'
import { CustomerService } from '../../services'

declare var jQuery:any;

@Component({
  selector: 'app-customer',
  templateUrl: 'app/components/customer/customer.component.html',
  styleUrls: ['app/components/customer/customer.component.css'],
  providers:[CustomerService]
})

export class CustomerComponent implements OnInit {
  customers: Customer[];
  modalCustomer : Customer;
  tableErrorMessage:string;

  private isEditing : boolean;

  constructor(
    private customerService: CustomerService
  ){}

  private loadModalWithCustomer(loadCustomer: Customer): void{

    this.modalCustomer = loadCustomer;
    jQuery("#modalCustomer").modal("show");
  }

  ngOnInit(): void{
      this.isEditing=false;
      this.tableErrorMessage="";
      this.modalCustomer=new Customer();
      this.customers =[];
      this.getAllCustomers();
  }

  getAllCustomers(): void{
      var self=this;
      this.customerService.getAllCustomers().then(

        function(customers){
            for( let cu of customers) {
              self.customers.push(cu);
            }
            if(self.customers.length==0){
              self.tableErrorMessage="暂无客户信息";
            }
        },
        function (error) {
            console.log("Error when loading all customers info: "+error);
            self.tableErrorMessage="无法读取客户资料,请稍后再试";
        });
  }

  onAddNewCustomer(): void{
    this.isEditing=false;
    this.loadModalWithCustomer(new Customer());
  }

  saveNewCustomer(c: Customer): void{
    jQuery("#modalCustomer").modal("hide");

    //todo: show wait spinner
    let self =this;
    this.customerService.addCustomer(c).then(
      function(customer){
        self.customers.push(customer);
      },
      function(error){
        console.log("Error when saving added new customer: "+error);
      }
    );

  }

  onEditCustomer(editCustomer: Customer): void{
    this.isEditing=true;
    let cu=JSON.parse(JSON.stringify(editCustomer));
    this.loadModalWithCustomer(cu);
  }

  saveEditCustomer(editCustomer: Customer): void{
    this.isEditing=false;
    jQuery("#modalCustomer").modal("hide");
    let self=this;

    //todo: show wait spinner
    this.customerService.updateCustomer(editCustomer).then(
      function(customer){
        self.updateCustomerInfo(customer);
      },
      function (error) {
        //todo: show error modal
        console.log("Error when saving edited customer info: "+error);
      }
    );
  }

  getCustomerById(id: string): Customer {
    for (let cu of this.customers){
      if(cu.id === id){
        return cu;
      }
    }
    return null;
  }

  onDeleteCustomer(targetCustomer: Customer): void{
    this.modalCustomer = targetCustomer;
    jQuery("#modalDeleteCustomer").modal("show");

  }

  deleteCustomer(targetCustomer: Customer): void{
    jQuery("#modalDeleteCustomer").modal("hide");
    let self = this;
    this.customerService.deleteCustomer(targetCustomer).then(
      function(){
        self.customers=self.customers.filter(c=>c!==targetCustomer);
      },
      function(error){
        //todo: show error modal
        console.log("Error when deleting customer info: "+error);
      }
    )
  }

  onRemoveIdPhoto(index: number): void{
    this.modalCustomer.customerIDPhotos.splice(index,1);
  }

  onUploadImage(): void {
    jQuery("#imageUpload").click();
  }

  private updateCustomerInfo(cu:Customer):void{
    let foundCustomer = this.getCustomerById(cu.id);
    if(foundCustomer!=null){
      foundCustomer.name=cu.name;
      foundCustomer.address = cu.address;
      foundCustomer.cellPhone = cu.cellPhone;
      foundCustomer.note = cu.note;
      foundCustomer.alterAddress = cu.alterAddress;
      foundCustomer.customerIDPhotos=cu.customerIDPhotos;
    }
  }
}
