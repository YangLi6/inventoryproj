/**
 * Created by yangli on 12/9/2016.
 */
import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-navigation-header',
  templateUrl: 'app/components/navigation-header/navigation-header.component.html'
})

export class NavigationHeaderComponent {

  constructor(
    private router: Router,
  ){}
  isSelected(viewLocation: string): boolean {
    return viewLocation === this.router.url;
  }


}
