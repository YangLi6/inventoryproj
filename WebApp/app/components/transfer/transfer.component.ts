import {Component, OnInit} from '@angular/core';
import {CompleterService, CompleterData} from 'ng2-completer';


import {
  Transfer,
  TransferStatus,
  TransferProductInventory,
  InventoryLocation,
  Product,
  ProductInventory,
  ExpressCompany,
  Shipping,
  ShippingStatus
} from '../../models';
import {
  ProductService,
  TransferService,
  InventoryLocationService,
  PaymentMethodService,
  ExpressCompanyService,
  ShippingService
} from '../../services';
import {DateHelper} from '../../utilities';
import {GUID} from "../../utilities/GUID";


declare var jQuery: any;

@Component({
  selector: 'app-transfer',
  templateUrl: 'app/components/transfer/transfer.component.html',
  styleUrls: ['app/components/transfer/transfer.component.css', 'styles.css'],
  providers: [TransferService, ProductService, InventoryLocationService, PaymentMethodService, ExpressCompanyService,ShippingService]

})

export class TransferComponent implements OnInit {
  TransferStatus = TransferStatus;
  ShippingStatus = ShippingStatus;
  allTransfers: Transfer[];
  allInventoryLocations: InventoryLocation[];
  products: Product[];
  expressCompanies: ExpressCompany[];

  modalTransfer: Transfer;
  modalTransferOriginalLocationID: string;
  modalTransferProduct: TransferProductInventory;
  modalExistedProduct: Product;
  modalNewShipping: Shipping;
  modalGeneralTitle: string;
  modalGeneralBody: string;

  tableErrorMessage: string;

  private showingTransfer: TransferStatus;
  allCollapse: boolean;

  private autoCompleteProductService: CompleterData;
  searchProductStr: string;

  constructor(private completerService: CompleterService,
              private transferService: TransferService,
              private inventoryLocationService: InventoryLocationService,
              private productService: ProductService,
              private expressCompanyService: ExpressCompanyService,
              private shippingService: ShippingService) {
  }

  ngOnInit(): void {
    this.showingTransfer = TransferStatus.Preparing;
    this.allTransfers = [];
    this.allInventoryLocations = [];
    this.products = [];
    this.expressCompanies = [];

    this.modalTransfer = new Transfer();
    this.modalExistedProduct = null;
    this.modalNewShipping = new Shipping();
    this.modalTransferOriginalLocationID = '';

    this.tableErrorMessage = "";
    this.allCollapse = true;

    this.searchProductStr = '';
    this.autoCompleteProductService = this.completerService.local(this.products, 'name,brand,skuNumber', 'name,brand').descriptionField("skuNumber");

    this.getAllTransfers();
    this.getAllInventoryLocations();
    this.loadAllProducts();
    this.getAllExpressCompanies();
  }

  getAllTransfers(): void {
    var self = this;
    this.transferService.getAllTransfers().then(
      function (transfers) {
        for (let transfer of transfers) {
          transfer.placedDate = new Date(transfer.placedDate);
          transfer.closedDate = new Date(transfer.closedDate);
          for (let tp of transfer.transferedProducts) {
            tp.productInventory.expiredDate = new Date(tp.productInventory.expiredDate);
          }

          for (let sh of transfer.shippings) {
            sh.shippedDate = new Date(sh.shippedDate);
            sh.receivedDate = new Date(sh.receivedDate);
          }
          if (!transfer.note) {
            transfer.note = "";
          }
          self.allTransfers.push(transfer);
        }
      },
      function (error) {
        console.log("Error when loading all customers info: " + error);
        self.tableErrorMessage = "无法读取订单信息,请稍后再试";
      });
  }

  getAllInventoryLocations(): void {
    var self = this;
    this.inventoryLocationService.getAllInventoryLocations().then(
      function (locations) {
        self.allInventoryLocations = locations;
      },
      function (error) {

      });
  }

  getInventoryLocationByID(id: string): InventoryLocation {
    for (let location of this.allInventoryLocations) {
      if (location.id === id) {
        return location;
      }
    }
    return null;
  }


  loadAllProducts(): void {
    var self = this;
    this.productService.getAllProducts().then(
      function (products) {
        for (let prod of products) {
          if (!prod.productInventories) {
            prod.productInventories = [];
          }
          for (let pi of prod.productInventories) {
            pi.expiredDate = new Date(pi.expiredDate);
          }
          self.products.push(prod);
        }
      },
      function (error) {
        console.log("Error when loading all product info: " + error);
        //self.tableErrorMessage="无法读取产品信息,请稍后再试";
      }
    );
  }

  getAllExpressCompanies(): void {
    var self = this;
    this.expressCompanyService.getAllExpressCompanies().then(
      function (expressCompanies) {
        for (let ec of expressCompanies) {
          self.expressCompanies.push(ec);
        }
      },
      function (error) {
        console.log("Error when loading all express companies: " + error);
      }
    )
  }


  onAddNewTransfer():void{
    this.modalTransfer=new Transfer();
    this.modalTransfer.placedDate=new Date();
    jQuery("#modalAddTransfer").modal("show");
  }


  confirmNewTransfer(modalTransfer: Transfer):void{
    jQuery("#modalAddTransfer").modal("hide");
    modalTransfer.closedDate=new Date("1970-01-20");
    var self = this;
    this.transferService.addTransfer(modalTransfer).then(
      function(savedTransfer){
        savedTransfer.placedDate=new Date(savedTransfer.placedDate);
        savedTransfer.closedDate=new Date(savedTransfer.closedDate);
        savedTransfer.shippings=[];
        self.allTransfers.push(savedTransfer);
      },
      function(error){
        console.log("Error when adding new transfer: "+error);
      }
    )
  }

  onSwitchReadyToShipTransfer(selectedTransfer: Transfer): void {
    selectedTransfer.status = selectedTransfer.status == TransferStatus.Preparing ? TransferStatus.ReadyToShip : TransferStatus.Preparing;
    this.transferService.updateTransfer(selectedTransfer).then(
      function (updatedTransfer) {
      },
      function (error) {
      }
    )
  }

  onDeleteTransfer(selectedTransfer: Transfer): void {
    this.loadModalWithSelectedTransfer(selectedTransfer);
    for (let tp of this.modalTransfer.transferedProducts) {
      tp.productInventory.expiredDate = new Date(tp.productInventory.expiredDate);
    }
    jQuery("#modalDeleteTransfer").modal("show");
  }

  confirmDeleteTransfer(selectedTransfer: Transfer): void {
    jQuery("#modalDeleteTransfer").modal("hide");
    var self = this;
    this.transferService.deleteTransfer(selectedTransfer).then(
      function (deletedTransfer) {
        var targetTransfer = self.findTransferByID(deletedTransfer.id);
        self.allTransfers = self.allTransfers.filter(t=>t.id != deletedTransfer.id);

        //update product inventory tree
        for(let tpi of targetTransfer.transferedProducts){
          tpi.productInventory.totalReservedQuantity -= tpi.transferedQuantity;
          self.updateProductInventoryInProductTree(tpi.productInventory);
        }

      },
      function (error) {
        console.log("Error when deleting order : " + selectedTransfer + " Error: " + error);
      }
    )
  }


  onShipTransfer(selectedTransfer: Transfer): void {
    if (selectedTransfer.status == TransferStatus.Preparing) {
      this.loadModalWithGeneralMessage("转运单尚未完成备货", '请先标注此订单"已完成备货"');
      return;
    }
    this.loadModalWithSelectedTransfer(selectedTransfer);
    this.modalNewShipping = new Shipping();
    jQuery("#modalShipTransfer").modal("show");
  }

  onModalShippingCostChange(shippingCost: number): void {
    this.modalNewShipping.shippingCost = shippingCost;
  }

  onAddNewExpressPackageToModalTransfer(): void {
    this.modalNewShipping.shippedDate = new Date();
    this.modalNewShipping.shippingStatus = ShippingStatus.Shipped;
    this.modalTransfer.shippings.push(this.modalNewShipping);
    this.modalNewShipping = new Shipping();
  }

  onRemoveExpressPackageFromModalTransfer(selectedShipping: Shipping): void {
    this.modalTransfer.shippings = this.modalTransfer.shippings.filter(s=>s.id != selectedShipping.id);
  }

  confirmShippedTransfer(): void {
    jQuery("#modalShipTransfer").modal("hide");
    var self = this;

    this.transferService.shipTransfer(this.modalTransfer).then(
      function (shippedTransfer) {
        self.updateEntireTransfer(shippedTransfer);
      },
      function (error) {
        console.log("Error when shipping order : " + self.modalTransfer + " Error: " + error);
      }
    )
  }

  onEditShipping(transfer: Transfer): void {
    this.loadModalWithSelectedTransfer(transfer);
    this.modalNewShipping = new Shipping();
    jQuery("#modalEditShipping").modal("show");
  }

  confirmEditShippingInfo(): void {
    jQuery("#modalEditShipping").modal("hide");
    var self = this;
    this.transferService.updateShipping(this.modalTransfer).then(
      function (updatedTransfer) {
        self.updateEntireTransfer(updatedTransfer);
      }
    )
  }

  onCompleteTransfer(transfer: Transfer): void {
    if (!this.hasAllPackagesArrived(transfer)) {
      this.loadModalWithGeneralMessage("此订单尚未完成所有包裹配送", '请先标注此订单"包裹全部到达"');
      return;
    }

    var self = this;
    transfer.closedDate = new Date();
    transfer.status = TransferStatus.Closed;
    this.transferService.updateTransfer(transfer).then(
      function (updatedTransfer) {
        self.updateEntireTransfer(updatedTransfer);
      }
    )
  }

  onReportProblem(transfer: Transfer): void {
    var self = this;
    transfer.status = TransferStatus.HasProblems;
    this.transferService.updateTransfer(transfer).then(
      function (updatedTransfer) {

        //self.updateEntireTransfer(updatedTransfer);
      }
    )
  }

  onPackageReceived(selectedPackage: Shipping): void {
    selectedPackage.shippingStatus =
      selectedPackage.shippingStatus == ShippingStatus.Received
        ? ShippingStatus.Shipped:ShippingStatus.Received;
    selectedPackage.receivedDate =
      selectedPackage.shippingStatus == ShippingStatus.Received
        ? new Date():null;
    this.shippingService.putShipping(selectedPackage).then(
      function(updatedShipping){
      },
      function (error) {
        console.log("Error when updating shipping info: "+error);
      }
    )
  }

  onPackageHasProblem(selectedPackage: Shipping): void {
    selectedPackage.shippingStatus =
      selectedPackage.shippingStatus == ShippingStatus.HasProblems
        ? ShippingStatus.Shipped:ShippingStatus.HasProblems;
    this.shippingService.putShipping(selectedPackage).then(
      function(updatedShipping){

      },
      function (error) {
        console.log("Error when updating shipping info: "+error);
      }
    )
  }

  hasAllPackagesArrived(transfer: Transfer): boolean {
    for (let sh of transfer.shippings) {
      if (sh.shippingStatus != ShippingStatus.Received) {
        return false;
      }
    }
    return true;
  }

  // onSwitchAllPackageArrived(transfer: Transfer): void {
  //   var hasAllArrived = this.hasAllPackagesArrived(transfer);
  //
  //   for (let sh of transfer.shippings) {
  //     sh.shippingStatus=hasAllArrived?ShippingStatus.Received:ShippingStatus.Shipped;
  //     sh.receivedDate=hasAllArrived?new Date():null;
  //     this.shippingService.putShipping(sh).then(
  //       function(updatedShipping){
  //       },
  //       function (error) {
  //         console.log("Error when updating shipping info: "+error);
  //       }
  //     )
  //   }
  // }

  private loadModalWithSelectedTransfer(selectedTransfer: Transfer): void {
    this.modalTransfer = JSON.parse(JSON.stringify(selectedTransfer));
    this.modalTransfer.closedDate = new Date(selectedTransfer.closedDate);
    this.modalTransfer.placedDate = new Date(selectedTransfer.placedDate);
  }

  private loadModalWithGeneralMessage(title: string, bodyMessage: string): void {
    this.modalGeneralTitle = title ? title : "";
    this.modalGeneralBody = bodyMessage ? bodyMessage : "";
    jQuery("#modalGeneralMessage").modal("show");

  }

  onEditTargetInventoryLocation(selectedTransfer: Transfer): void {
    this.loadModalWithSelectedTransfer(selectedTransfer);
    this.modalTransferOriginalLocationID = selectedTransfer.toInventoryLocationID;
    jQuery("#modalTransferLocation").modal("show");
  }

  confirmSelectedDestination(updateTransfer: Transfer): void {
    jQuery("#modalTransferLocation").modal("hide");
    var self = this;
    this.transferService.updateTransfer(updateTransfer).then(
      function (savedTransfer) {
        var order = self.findTransferByID(savedTransfer.id);
        order.toInventoryLocationID = savedTransfer.toInventoryLocationID;
      },
      function (error) {
        console.log("Error when updating transfer toInventoryLocation info: " + error);
      }
    )
  }

  onAddTransferProducts(selectedTransfer: Transfer): void {
    this.searchProductStr = '';
    this.loadModalWithSelectedTransfer(selectedTransfer);
    this.modalTransfer.transferedProducts = [];
    this.modalExistedProduct = null;
    jQuery("#modalAddTransferProduct").modal("show");
  }

  onFoundProductSelected(selectedProd: any): void {
    var skuNumber = selectedProd.description;
    this.modalExistedProduct = this.findProductBySku(skuNumber);

    for (let pi of this.modalExistedProduct.productInventories) {
      var newTPI = new TransferProductInventory();
      newTPI.id = GUID.getNewGUIDString();
      newTPI.transferID = this.modalTransfer.id;
      newTPI.productInventoryID = pi.id;
      newTPI.productInventory = pi;
      newTPI.productInventory.expiredDate = new Date(pi.expiredDate);

      newTPI.productInventory.product = new Product();
      newTPI.productInventory.product.skuNumber = this.modalExistedProduct.skuNumber;
      newTPI.productInventory.product.hasExpiredDate = this.modalExistedProduct.hasExpiredDate;
      newTPI.productInventory.product.name = this.modalExistedProduct.name;
      newTPI.productInventory.product.brand = this.modalExistedProduct.brand;
      newTPI.productInventory.product.weight = this.modalExistedProduct.weight;
      newTPI.transferedQuantity = 0;
      this.modalTransfer.transferedProducts.push(newTPI);
    }

  }

  confirmAddExistedTransferProduct(): void {
    this.modalExistedProduct = null;
    jQuery("#modalAddTransferProduct").modal("hide");
    var targetTransfer = this.findTransferByID(this.modalTransfer.id);
    var self = this;
    for (let tpi of this.modalTransfer.transferedProducts) {
      if (tpi.transferedQuantity > 0) {
        var existedTPI = this.findTransferProductInventoryByProductInventoryID(tpi.productInventoryID, targetTransfer.transferedProducts);
        if (!existedTPI) {
          this.transferService.addTransferProductInventory(tpi).then(
            function (savedTPI) {
              savedTPI.productInventory.expiredDate = new Date(savedTPI.productInventory.expiredDate);
              targetTransfer.transferedProducts.push(savedTPI);
              self.updateProductInventoryInProductTree(savedTPI.productInventory);
              self.selfUpdateTransferBalance(targetTransfer);
            },
            function (error) {
              console.log("Error when adding transferproductinventory info: " + error);
            }
          )

        }
        else {
          this.transferService.updateTransferedProductInventoryQuantity(existedTPI, existedTPI.transferedQuantity + tpi.transferedQuantity).then(
            function (savedTPI) {
              existedTPI = self.findTransferProductInventoryByProductInventoryID(savedTPI.productInventoryID, targetTransfer.transferedProducts);
              existedTPI.transferedQuantity = savedTPI.transferedQuantity;
              self.updateProductInventoryInProductTree(savedTPI.productInventory);
              self.selfUpdateTransferBalance(targetTransfer);
            },
            function (error) {
              console.log("Error when updating orderproductinventory quantity: " + error);
            }
          );
        }
      }
    }
  }

  onEditTransferProduct(transferProduct: TransferProductInventory): void {
    var productInventory = this.findProductInventoryByID(transferProduct.productInventoryID);
    this.modalTransferProduct = JSON.parse(JSON.stringify(transferProduct));
    this.modalTransferProduct.productInventory.totalReservedQuantity = productInventory.totalReservedQuantity;
    this.modalTransferProduct.productInventory.quantityOnHand = productInventory.quantityOnHand;
    this.modalTransferProduct.productInventory.expiredDate = new Date(transferProduct.productInventory.expiredDate);
    jQuery("#modalTransferProduct").modal("show");
  }

  onTransferQuantityChange(transferQuantity: number): void {
    var change = this.modalTransferProduct.transferedQuantity - transferQuantity;
    this.modalTransferProduct.transferedQuantity = transferQuantity;
    this.modalTransferProduct.productInventory.totalReservedQuantity -= change;
  }

  onUpdateTransferQuantity(transferQuantity: number): number {
    return transferQuantity;
  }

  confirmTransferQuantityOnExistedProduct(): void {
    jQuery("#modalTransferProduct").modal("hide");
    var self = this;
    this.transferService.updateTransferedProductInventoryQuantity(this.modalTransferProduct, this.modalTransferProduct.transferedQuantity).then(
      function (savedTPI) {
        var transfer = self.findTransferByID(savedTPI.transferID);
        for (let tpi of transfer.transferedProducts) {
          if (tpi.id === savedTPI.id) {
            tpi.transferedQuantity = savedTPI.transferedQuantity;
            tpi.productInventory.quantityOnHand = savedTPI.productInventory.quantityOnHand;
            tpi.productInventory.totalReservedQuantity = savedTPI.productInventory.totalReservedQuantity;
          }
        }

        //need to update product tree about savedOPI
        self.updateProductInventoryInProductTree(savedTPI.productInventory);
        self.selfUpdateTransferBalance(transfer);
      },
      function (error) {
        console.log("Error when updating TransferProductInventory quantity: " + error);
      }
    );
  }

  onDeleteTransferProduct(transferredProduct: TransferProductInventory): void {
    this.modalTransferProduct = JSON.parse(JSON.stringify(transferredProduct));
    this.modalTransferProduct.productInventory.expiredDate = new Date(transferredProduct.productInventory.expiredDate);
    jQuery("#modalDeleteTransferProduct").modal("show");
  }

  confirmDeleteTransferQuantity(transferredProduct: TransferProductInventory): void {
    jQuery("#modalDeleteTransferProduct").modal("hide");
    var self = this;
    this.transferService.deleteTransferProductInventory(transferredProduct).then(
      function () {
        var transfer = self.findTransferByID(transferredProduct.transferID);
        transfer.transferedProducts = transfer.transferedProducts.filter(tpi=>tpi.id != transferredProduct.id);

        //update product tree
        transferredProduct.productInventory.totalReservedQuantity -= transferredProduct.transferedQuantity;
        self.updateProductInventoryInProductTree(transferredProduct.productInventory);
        self.selfUpdateTransferBalance(transfer);
      },
      function (error) {
        console.log("Error when deleting TransferProductInventory: " + error);
      }
    );
  }


  private findProductBySku(skuNumber: string): Product {
    for (let prod of this.products) {
      if (prod.skuNumber == skuNumber) {
        return prod;
      }
    }
    return null;
  }

  private findTransferByID(id: string): Transfer {
    for (let transfer of this.allTransfers) {
      if (transfer.id == id) {
        return transfer;
      }
    }
    return null;
  }

  private findTransferProductInventoryByProductInventoryID(id: string, tpis: TransferProductInventory[]): TransferProductInventory {
    for (let tpi of tpis) {
      if (tpi.productInventoryID === id) {
        return tpi;
      }
    }
    return null;
  }

  getTransferNumber(status: TransferStatus): number {
    return this.getTransferByStatus(status).length;
  }

  setShowTransferStatus(status: TransferStatus): void {
    if (this.showingTransfer !== status) {
      this.showingTransfer = status;
      this.allCollapse = true;
      this.forceAllCollapse();
    }
  }

  getShowTransferStatus(): TransferStatus {
    return this.showingTransfer;
  }

  toggleAllCollapse(): void {
    this.allCollapse = !this.allCollapse;
    var currentShowStatus = this.getShowTransferStatus();
    var currentShowTransfers = this.getTransferByStatus(currentShowStatus);
    for (let transfer of currentShowTransfers) {
      var collapseCommand = this.allCollapse ? 'hide' : 'show';
      jQuery("#preparing-" + transfer.id).collapse(collapseCommand);
      jQuery("#shipped-" + transfer.id).collapse(collapseCommand);
      jQuery("#completed-" + transfer.id).collapse(collapseCommand);
      jQuery("#problem-" + transfer.id).collapse(collapseCommand);
    }
  }

  forceAllCollapse(): void {
    var currentShowStatus = this.getShowTransferStatus();
    var currentShowTransfers = this.getTransferByStatus(currentShowStatus);
    for (let transfer of currentShowTransfers) {
      jQuery("#" + transfer.id).collapse('hide');
    }
  }

  getTransferByStatus(status: TransferStatus): Transfer[] {
    if (status === TransferStatus.Preparing) {
      return this.allTransfers.filter(t => t.status === status || t.status === TransferStatus.ReadyToShip)
    }
    else if (status === TransferStatus.Shipped) {
      return this.allTransfers.filter(t => t.status === status)
    }
    else {
      return this.allTransfers.filter(t => t.status === status)
    }
  }


  private selfUpdateTransferBalance(transfer: Transfer): void {
    var totalProductCost = 0;
    var totalShippingWeight = 0;
    for (let tpi of transfer.transferedProducts) {
      totalProductCost += tpi.transferedQuantity * tpi.productInventory.purchasedPrice;
      totalShippingWeight += tpi.transferedQuantity;
    }
    transfer.productCost = totalProductCost;
    transfer.totalWeight = totalShippingWeight;
  }


  private updateEntireTransfer(newTransfer: Transfer): void {
    let foundTransfer = this.findTransferByID(newTransfer.id);
    if (foundTransfer != null) {
      foundTransfer.toInventoryLocationID = newTransfer.toInventoryLocationID;
      foundTransfer.status = newTransfer.status;
      foundTransfer.totalWeight = newTransfer.totalWeight;
      foundTransfer.productCost = newTransfer.productCost;
      foundTransfer.shippingCost = newTransfer.shippingCost;
      foundTransfer.note = newTransfer.note;
      if (!foundTransfer.note) {
        foundTransfer.note = "";
      }
      foundTransfer.placedDate = new Date(newTransfer.placedDate);
      foundTransfer.closedDate = new Date(newTransfer.closedDate);
      foundTransfer.transferedProducts = [];
      foundTransfer.shippings = [];
      for (let transferProduct of newTransfer.transferedProducts) {
        transferProduct.productInventory.expiredDate = new Date(transferProduct.productInventory.expiredDate);
        foundTransfer.transferedProducts.push(transferProduct);
      }

      for (let sh of newTransfer.shippings) {
        if(sh.receivedDate) {
          sh.receivedDate = new Date(sh.receivedDate);
        }
        if(sh.shippedDate) {
          sh.shippedDate = new Date(sh.shippedDate);
        }
        foundTransfer.shippings.push(sh);
      }
    }
  }

  getTotalQuantityOnHand(productSKU: string): number {
    let total = 0;
    var product = this.findProductBySku(productSKU);
    for (let pi of product.productInventories) {
      total += pi.quantityOnHand;
    }
    return total;
  }

  getTotalReserved(productSKU: string): number {
    let total = 0;
    var product = this.findProductBySku(productSKU);
    for (let pi of product.productInventories) {
      total += pi.totalReservedQuantity;
    }
    return total;
  }

  getTotalQuantityTransfered(transferProductInventories: TransferProductInventory[]): number {
    let total = 0;
    if (!transferProductInventories) {
      return 0;
    }
    for (let tpi of transferProductInventories) {
      total += tpi.transferedQuantity;
    }
    return total;
  }

  getClosestExpiredDate(product: Product): Date {
    var result: Date = new Date(9999, 12, 30);
    for (let record of product.productInventories) {
      var exDate = new Date(record.expiredDate);
      if (exDate.getTime() < result.getTime()) {
        result = exDate;
      }
    }
    if (result != new Date(9999, 12, 30)) {
      return result;
    }
    return null;
  }

  getTransferShippedDate(transfer: Transfer): Date {
    var result: Date = new Date(9999, 12, 30);
    for (let sh of transfer.shippings) {
      if (result.getTime() > sh.shippedDate.getTime()) {
        result = sh.shippedDate;
      }
    }
    if (result != new Date(9999, 12, 30)) {
      return result;
    }
    return null;
  }

  getTotalShippingCostByTransfer(transfer: Transfer): number {
    var result = 0;
    for (let shipping of transfer.shippings) {
      result += shipping.shippingCost;
    }
    return result;
  }

  findExpressCompanyByID(targetID: string): ExpressCompany {
    for (let ec of this.expressCompanies) {
      if (ec.id === targetID) {
        return ec;
      }
    }
    return null;
  }

  convertLocalDateToUTC(dateString: string): Date {
    return DateHelper.convertLocalDateToUTC(dateString);
  }

  isExpiredInOneYear(expiredDate: string): boolean {
    return DateHelper.isExpiredInOneYear(expiredDate);
  }

  isExpiredAlready(expiredDate: string): boolean {
    return DateHelper.isExpiredAlready(expiredDate);
  }

  private findProductInventoryByID(id: string): ProductInventory {
    for (let prod of this.products) {
      for (let pi of prod.productInventories) {
        if (pi.id === id) {
          return pi;
        }
      }
    }
    return null;
  }

  private updateProductInventoryInProductTree(pi: ProductInventory): void {
    for (let prod of this.products) {
      for (let item of prod.productInventories) {
        if (pi.id === item.id) {
          item.totalReservedQuantity = pi.totalReservedQuantity;
        }
      }
    }
  }

  private selfUpdateTransferBalance(transfer: Transfer): void {
    var totalProductCost = 0;
    var totalShippingWeight = 0;
    for (let tpi of transfer.transferedProducts) {
      totalProductCost += tpi.transferedQuantity * tpi.productInventory.purchasedPrice;
      totalShippingWeight += tpi.transferedQuantity * tpi.productInventory.product.weight;
    }
    transfer.productCost = totalProductCost;
    transfer.totalWeight = totalShippingWeight;

  }

}
