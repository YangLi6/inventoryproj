import {Component, OnInit} from '@angular/core';


import {
  InventoryLocation,
  PaymentMethod,
  ExpressCompany,

} from '../../models';
import {
  InventoryLocationService,
  PaymentMethodService,
  ExpressCompanyService
} from '../../services';
import {GUID} from "../../utilities/GUID";


declare var jQuery: any;

export interface EditObject {
  targetProperty: string;
  parentType: string;
  parentID: string;
}

export interface AddNewObject {
  targetType: string;
  id: string;
  value: string;
}

@Component({
  selector: 'app-setting',
  templateUrl: 'app/components/setting/setting.component.html',
  styleUrls: ['app/components/setting/setting.component.css', 'styles.css'],
  providers: [InventoryLocationService, PaymentMethodService, ExpressCompanyService]

})


export class SettingComponent implements OnInit {

  inventoryLocations: InventoryLocation[] = [];
  paymentMethods: PaymentMethod[] = [];
  expressCompanies: ExpressCompany[] = [];

  modalEditObject: EditObject;
  modalAddedObject: AddNewObject;

  constructor(private inventoryLocationService: InventoryLocationService,
              private paymentMethodService: PaymentMethodService,
              private expressCompanyService: ExpressCompanyService,) {
  }

  ngOnInit(): void {
    this.getAllInventoryLocations();
    this.getAllPaymentMethods();
    this.getAllExpressCompanies();
  }

  getAllInventoryLocations(): void {
    var self = this;
    this.inventoryLocationService.getAllInventoryLocations().then(
      function (locations) {
        self.inventoryLocations = locations;
      },
      function (error) {
        console.log("Error when loading all inventory locations: " + error);
      });
  }

  getAllPaymentMethods(): void {
    var self = this;
    this.paymentMethodService.getAllPaymentMethods().then(
      function (paymentMethods) {
        for (let pm of paymentMethods) {
          self.paymentMethods.push(pm);
        }
      },
      function (error) {
        console.log("Error when loading all payment methods: " + error);
      }
    )
  }

  getAllExpressCompanies(): void {
    var self = this;
    this.expressCompanyService.getAllExpressCompanies().then(
      function (expressCompanies) {
        for (let ec of expressCompanies) {
          self.expressCompanies.push(ec);
        }
      },
      function (error) {
        console.log("Error when loading all express companies: " + error);
      }
    )
  }

  findInventoryLocationByID(id: string): InventoryLocation {
    for (let location of this.inventoryLocations) {
      if (location.id === id) {
        return location;
      }
    }
    return null;
  }

  findPaymentMethodByID(id: string): PaymentMethod {
    for (let pm of this.paymentMethods) {
      if (pm.id === id) {
        return pm;
      }
    }
    return null;
  }

  findExpressCompanyByID(id: string): ExpressCompany {
    for (let ec of this.expressCompanies) {
      if (ec.id === id) {
        return ec;
      }
    }
    return null;
  }

  loadModalWithEditObject(editObject: EditObject) {
    this.modalEditObject = editObject;
    jQuery("#modalEdit").modal("show");
  }

  saveEdit(): void {
    if (this.modalEditObject.parentType === "InventoryLocation") {
      this.saveInventoryLocationChanges(this.modalEditObject);
    }
    else if (this.modalEditObject.parentType === "PaymentMethod") {
      this.savePaymentMethodChanges(this.modalEditObject);
    }
    else if (this.modalEditObject.parentType === "ExpressCompany") {
      this.saveExpressCompanyChanges(this.modalEditObject);
    }
    else {
      return;
    }

  }

  saveInventoryLocationChanges(editObject: EditObject): void {
    var targetInventoryLocation = this.findInventoryLocationByID(editObject.parentID);
    if (targetInventoryLocation.locationName != editObject.targetProperty) {
      targetInventoryLocation.locationName = editObject.targetProperty;
      this.inventoryLocationService.putInventoryLocation(targetInventoryLocation).then(
        ()=> {
          jQuery("#modalEdit").modal("hide");
        }
      );
    }
  }

  savePaymentMethodChanges(editObject: EditObject): void {
    var targetPaymentMethod = this.findPaymentMethodByID(editObject.parentID);
    if (targetPaymentMethod.name != editObject.targetProperty) {

      targetPaymentMethod.name = editObject.targetProperty;
      this.paymentMethodService.putPaymentMethod(targetPaymentMethod).then(
        ()=> {
          jQuery("#modalEdit").modal("hide");
        }
      );
    }
  }

  saveExpressCompanyChanges(editObject: EditObject): void {
    var targetExpressCompany = this.findExpressCompanyByID(editObject.parentID);
    if (targetExpressCompany.name != editObject.targetProperty) {
      targetExpressCompany.name = editObject.targetProperty;
      this.expressCompanyService.putExpressCompany(targetExpressCompany).then(
        ()=> {
          jQuery("#modalEdit").modal("hide");
        }
      );
    }
  }


  editInventoryLocation(selectedInventoryLocation: InventoryLocation): void {
    var editInventoryLocation: EditObject = {
      targetProperty: selectedInventoryLocation.locationName,
      parentType: "InventoryLocation",
      parentID: selectedInventoryLocation.id
    };
    this.loadModalWithEditObject(editInventoryLocation);
  }

  editPaymentMethod(selectedPaymentMethod: PaymentMethod): void {
    var editPaymentMethod: EditObject = {
      targetProperty: selectedPaymentMethod.name,
      parentType: "PaymentMethod",
      parentID: selectedPaymentMethod.id
    };
    this.loadModalWithEditObject(editPaymentMethod);
  }

  editExpressCompany(selectedExpressCompany: ExpressCompany): void {
    var editExpressCompany: EditObject = {
      targetProperty: selectedExpressCompany.name,
      parentType: "ExpressCompany",
      parentID: selectedExpressCompany.id
    };
    this.loadModalWithEditObject(editExpressCompany);
  }


  loadModalWithAddedObject(addedObject: AddNewObject) {
    this.modalAddedObject = addedObject;
    jQuery("#modalAdd").modal("show");
  }

  addInventoryLocation(): void {
    var newInventoryLocation: AddNewObject = {
      targetType: "InventoryLocation",
      id: GUID.getNewGUIDString(),
      value: ""
    };
    this.loadModalWithAddedObject(newInventoryLocation);

  }

  addPaymentMethod(): void {
    var newPaymentMethod: AddNewObject ={
      targetType: "PaymentMethod",
      id: GUID.getNewGUIDString(),
      value: ""
    };
    this.loadModalWithAddedObject(newPaymentMethod);
  }

  addExpressCompany(): void {
    var newExpressCompany: AddNewObject ={
      targetType: "ExpressCompany",
      id: GUID.getNewGUIDString(),
      value: ""
    };
    this.loadModalWithAddedObject(newExpressCompany);
  }

  saveAdd(): void {
    if (this.modalAddedObject.targetType === "InventoryLocation") {
      this.saveNewInventoryLocation(this.modalAddedObject);
    }
    else if (this.modalAddedObject.targetType === "PaymentMethod") {
      this.saveNewPaymentMethod(this.modalAddedObject);
    }
    else if (this.modalAddedObject.targetType === "ExpressCompany") {
      this.saveNewExpressCompany(this.modalAddedObject);
    }
    else {
      return;
    }
  }

  saveNewInventoryLocation(addedObject: AddNewObject) {
    var newIL = new InventoryLocation();
    newIL.id = addedObject.id;
    newIL.locationName = addedObject.value;
    var self = this;
    this.inventoryLocationService.addInventoryLocation(newIL).then(
      function (savedIL) {
        self.inventoryLocations.push(savedIL);
        jQuery("#modalAdd").modal("hide");
      }, function (error) {

      });
  }

  saveNewPaymentMethod(addedObject: AddNewObject) {
    var newPM = new PaymentMethod();
    newPM.id = addedObject.id;
    newPM.name = addedObject.value;
    var self = this;
    this.paymentMethodService.addPaymentMethod(newPM).then(
      function (savedPM) {
        self.paymentMethods.push(savedPM);
        jQuery("#modalAdd").modal("hide");
      }, function (error) {

      });
  }

  saveNewExpressCompany(addedObject: AddNewObject) {
    var newEC = new ExpressCompany();
    newEC.id = addedObject.id;
    newEC.name = addedObject.value;
    var self = this;
    this.expressCompanyService.addExpressCompany(newEC).then(
      function (savedEC) {
        self.expressCompanies.push(savedEC);
        jQuery("#modalAdd").modal("hide");
      }, function (error) {

      });
  }

}
