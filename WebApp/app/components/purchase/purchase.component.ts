/**
 * Created by yangli on 12/9/2016.
 */
import { Component,OnInit  } from '@angular/core'
import { CompleterService, CompleterData } from 'ng2-completer'

import {Product,ProductSupply} from '../../models'
import {ProductService,ProductSupplyService} from '../../services'
import {DateHelper} from "../../utilities";

declare var jQuery:any;

@Component({
  selector: 'app-purchase',
  templateUrl: 'app/components/purchase/purchase.component.html',
  styleUrls:['app/components/purchase/purchase.component.css','styles.css'],
  providers:[ProductService,ProductSupplyService]
})

export class PurchasedComponent implements OnInit {
  products: Product[];
  productSupplies: ProductSupply[];
  modalProductSupplyRecord: ProductSupply;


  private isEditing: boolean;

  private searchProductStr: string;
  private autoCompleteDataService: CompleterData;
  tableErrorMessage:string;


  constructor(
    private completerService: CompleterService,
    private productService: ProductService,
    private productSupplyService: ProductSupplyService
  ){}

   ngOnInit(): void{
    this.isEditing=false;
    this.tableErrorMessage="";
    this.products=[];
    this.productSupplies=[];
    this.modalProductSupplyRecord = new ProductSupply();
    this.autoCompleteDataService=this.completerService.local(this.products,'name,brand,skuNumber','name,brand').descriptionField("skuNumber");
    this.loadAllProductSupplies();
    this.loadAllProducts();

   }

  loadAllProductSupplies():void{
    var self=this;
    this.productSupplyService.getAllProductSupplies().then(
      function(productSupplies){
        for( let ps of productSupplies) {
          ps.purchasedDate=new Date(ps.purchasedDate);
          ps.expiredDate=new Date(ps.expiredDate);
          self.productSupplies.push(ps);
        }
        if(self.productSupplies.length==0){
          self.tableErrorMessage="暂无产品信息";
        }
      },
      function (error) {
        console.log("Error when loading all product supplies info: "+error);
        self.tableErrorMessage="无法读取产品采购记录,请稍后再试";
      });
  }

  loadAllProducts():void{
      var self=this;
      this.productService.getAllProducts().then(
        function(products){
          for(let prod of products) {
            self.products.push(prod);
          }
        },
        function(error){
          console.log("Error when loading all product info: "+error);
          //self.tableErrorMessage="无法读取产品信息,请稍后再试";
        }
      );
  }

  private loadModalWithProduct(pr: ProductSupply): void{

    this.modalProductSupplyRecord = pr;
    jQuery("#modalPurchaseRecord").modal("show");
  }

  onAddNewPurchase(): void{
    this.isEditing=false;
    this.searchProductStr="";

    var newPurchasedRecord = new ProductSupply();
    this.loadModalWithProduct(newPurchasedRecord);
  }


  onProductSelected(selectedProd:any):void{
    var skuNumber = selectedProd.description;
    var today = new Date();
    this.modalProductSupplyRecord.product=this.findProductBySku(skuNumber);
    this.modalProductSupplyRecord.skuNumber=skuNumber;
    this.modalProductSupplyRecord.purchasedDate=today;

    if(this.modalProductSupplyRecord.product.hasExpiredDate){
      this.modalProductSupplyRecord.expiredDate=new Date();
      this.modalProductSupplyRecord.expiredDate.setFullYear(today.getFullYear()+1);
    }
    else{
      this.modalProductSupplyRecord.expiredDate=new Date('0001-01-01');
    }

    this.modalProductSupplyRecord.purchasedPrice=0;
    this.modalProductSupplyRecord.purchasedQuantity=1;

  }

  research():void{
    this.searchProductStr="";
    this.modalProductSupplyRecord.product=null;
  }

   saveNewPurchase(ps: ProductSupply): void{
     jQuery("#modalPurchaseRecord").modal("hide");

     let self=this;
     this.productSupplyService.addProductSupplyRecord(ps).then(
       function(prodSupplyRecord){
          prodSupplyRecord.expiredDate = new Date(prodSupplyRecord.expiredDate);
         prodSupplyRecord.purchasedDate = new Date(prodSupplyRecord.purchasedDate);
         self.productSupplies.push(prodSupplyRecord);
       },
       function(error){
         console.log("Error when saving added new purchased record: "+error);
       }
     );
   }

   onEditPurchasedRecord(ps: ProductSupply):void{
    this.isEditing=true;
    let editPurchasedRecord = JSON.parse(JSON.stringify(ps));
    editPurchasedRecord.purchasedDate=new Date(ps.purchasedDate);
    editPurchasedRecord.expiredDate=new Date(ps.expiredDate);
    this.loadModalWithProduct(editPurchasedRecord);
   }

   saveEditPurchasedRecord(ps:ProductSupply):void{
     jQuery("#modalPurchaseRecord").modal("hide");
     this.isEditing=false;
     var self = this;
     this.productSupplyService.updateProductSupplyRecord(ps).then(
       function(productSupply){
          self.updateProductSupplyInfo(productSupply);
       },
       function(error){
         console.log("Error when saving edit product purchased record: "+error);
       }
     )

   }

   onDeletePurchasedRecord(pr: ProductSupply):void{
     jQuery("#modalDeletePurchaseRecord").modal("show");
     this.modalProductSupplyRecord = pr;
   }

   deletePurchaseRecord(ps: ProductSupply):void{
     jQuery("#modalDeletePurchaseRecord").modal("hide");
     let self =this;
     this.productSupplyService.deleteProductSupplyRecord(ps).then(
       function(){
         self.productSupplies=self.productSupplies.filter(p=>p!==ps);
       },
       function(error){
         console.log("Error when deleting product supply info: "+error);
       }
     );
  }

   onQuantityChange(quantity:number){
    this.modalProductSupplyRecord.purchasedQuantity=quantity;
   }

   onPurchasedPriceChange(pr:number){
    this.modalProductSupplyRecord.purchasedPrice=pr;
   }



   private findProductBySku(skuNumber:string):Product{
      for(let prod of this.products){
        if(prod.skuNumber==skuNumber){
          return prod;
        }
      }
      return null;
   }

   private findProductSupplyByID(id:string):ProductSupply{
      for(let ps of this.productSupplies){
        if(ps.id==id){
          return ps;
        }
      }
      return null;
   }

  private updateProductSupplyInfo(newRecord:ProductSupply):void{
    let foundPS = this.findProductSupplyByID(newRecord.id);
    if(foundPS!=null){
      foundPS.expiredDate=newRecord.expiredDate;
      foundPS.purchasedDate=newRecord.purchasedDate;
      foundPS.purchasedQuantity=newRecord.purchasedQuantity;
      foundPS.purchasedPrice=newRecord.purchasedPrice;
      foundPS.purchasedStore=newRecord.purchasedStore;
    }
  }



  //DateHelper wrapper
  isExpiredInOneYear(expiredDate:string):boolean{
    return DateHelper.isExpiredInOneYear(expiredDate);
  }

  isExpiredAlready(expiredDate:string):boolean{
    return DateHelper.isExpiredAlready(expiredDate);
  }

  convertLocalDateToUTC(dateString:string): Date{
    return DateHelper.convertLocalDateToUTC(dateString);
  }

}
