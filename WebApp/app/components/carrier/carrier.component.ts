/**
 * Created by yangli on 12/9/2016.
 */
import {Component, OnInit} from '@angular/core';

import{Shipping,ShippingStatus,ExpressCompany}from '../../models';
import {ShippingService, ExpressCompanyService} from '../../services';
import {DateHelper} from "../../utilities";

declare var jQuery: any;

@Component({
  selector: 'app-carrier',
  templateUrl: 'app/components/carrier/carrier.component.html',
  styleUrls: ['app/components/carrier/carrier.component.css', 'styles.css'],
  providers: [ShippingService,ExpressCompanyService]
})

export class CarrierComponent implements OnInit {
  allShippings: Shipping[];
  allExpressCompanies:ExpressCompany[];
  ShippingStatus=ShippingStatus;
  tableErrorMessage = "";


  constructor(private expressCompanyService: ExpressCompanyService,
              private shippingService: ShippingService) {
  }

  ngOnInit(): void {
    this.allShippings=[];
    this.allExpressCompanies=[];
    this.getAllExpressCompanies();
    this.getAllShippings();
  }

  getAllShippings(): void {
    var self = this;
    this.shippingService.getAllShippings().then(
      function (shippings) {
        for (let sh of shippings) {
          sh.shippedDate = new Date(sh.shippedDate);
          sh.receivedDate = new Date(sh.receivedDate);
          self.allShippings.push(sh);
        }
        if (self.allShippings.length === 0) {
          self.tableErrorMessage = "暂无快递信息";
        }
      },
      function (error) {
        console.log("Error when loading all shipping info " + error);
        self.tableErrorMessage = "无法读取快递记录,请稍后再试";
      }
    );
  }

  getAllExpressCompanies(): void {
    var self = this;
    this.expressCompanyService.getAllExpressCompanies().then(
      function (expressCompanies) {
        for (let ec of expressCompanies) {
          self.allExpressCompanies.push(ec);
        }
      },
      function (error) {
        console.log("Error when loading all express companies: " + error);
      }
    )
  }

  findExpressCompanyByID(targetID:string):ExpressCompany{
    for(let ec of this.allExpressCompanies){
      if(ec.id === targetID){
        return ec;
      }
    }
    return null;
  }

  onPackageReceived(selectedShipping:Shipping):void{
    selectedShipping.shippingStatus=ShippingStatus.Received;
    selectedShipping.receivedDate=new Date();
    this.shippingService.putShipping(selectedShipping).then(
      function(updatedShipping){

      },
      function(error){
        console.log("Error when updating shipping info: "+error);
      }
    )
  }

  onPackageHasProblem(selectedShipping:Shipping):void{
    selectedShipping.shippingStatus=ShippingStatus.HasProblems;
    this.shippingService.putShipping(selectedShipping).then(
      function(updatedShipping){

      },
      function(error){
        console.log("Error when updating shipping info: "+error);
      }
    )
  }

}
