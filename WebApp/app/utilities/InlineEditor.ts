/**
 * Created by Yang on 2017-01-21.
 */

import {Component, Input, Output, EventEmitter} from '@angular/core';
import{GUID} from './'

declare var jQuery:any;

@Component({
  selector:'inline-editor',
  styles:[`
    a {
        text-decoration: none;
        color: inherit;
        border-bottom: solid 1px;
        cursor: pointer;
        /*line-height: 2;*/
        height:inherit;
        margin-right: 5px;
        margin-left: 5px;
        overflow: hidden;
        white-space: nowrap;
    }

    .inlineEditWrapper{
        display: inline-block;
    }

    [hidden] {
        display: none;
    }
    `],
  template:`<div class="inlineEditWrapper" [ngStyle]="{'max-height':maxHeight}">
    <a (click)="edit()" [hidden]="editing" [ngStyle]="{'max-width':maxWidth}">
        <!--<span>{{value}}</span>-->
        <span *ngIf="showValue()">{{value}}</span>
        <span *ngIf="!showValue()">{{value.substring(1,50)}}...</span>
    </a>
    <input id="inlineEditorInput-{{id}}" [(ngModel)]="value" [ngModelOptions]="{standalone: true}" [hidden]="!editing" (blur)="save()" (click)="selectAll()" on-focus="getFocused()"
        [ngStyle]="{'max-width':maxWidth, 'max-height':maxHeight}" style="color:black; height: inherit" />
    </div>`
})

export class InlineEditor{
  @Input() public value:any;
  @Input() public maxWidth:string;
  @Input() public maxHeight:string;
  @Output() onSave = new EventEmitter<InlineEditorArg>();

  id:string=GUID.getNewGUIDString();
  private editing:boolean = false;
  constructor() {}

  edit():void{
    this.editing=true;
    jQuery("#inlineEditorInput-"+this.id).focus();
  }
  save(){
    this.onSave.emit(new InlineEditorArg(this.value,this.value));
    this.editing = false;
  }
  selectAll(){
    jQuery("#inlineEditorInput-"+this.id).select();
  }
  getFocused(){
    this.selectAll();
  }
  showValue(){

    if(this.value===0 || String(this.value).length<=50){
      return true;
    }
    return false;

  }
}

export class InlineEditorArg{
  id:string;
  value:string;
  constructor(id:string,value:string){
    this.id=id;
    this.value=value;
  }
}
