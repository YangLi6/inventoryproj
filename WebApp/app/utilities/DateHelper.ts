/**
 * Created by Yang on 2017-01-04.
 */
export class DateHelper {

  public static convertLocalDateToUTC(dateString:string): Date{
    if (dateString) {
      var inputDate = new Date(dateString);
      var nowUTC = new Date();
      return new Date(inputDate.getTime()+nowUTC.getTimezoneOffset()*60*1000);
    } else {
      return null;
    }
  }

  public static isExpiredInOneYear(expiredDate:string):boolean{
    var oneYearFromNow = new Date();
    oneYearFromNow.setFullYear(oneYearFromNow.getFullYear()+1);
    var now = new Date();
    var exDate= new Date(expiredDate);
    if(exDate.getTime()<=oneYearFromNow.getTime() && exDate.getTime()>now.getTime()){
      return true;
    }
    return false;
  }

  public static isExpiredAlready(expiredDate:string):boolean{
    var now = new Date();
    var exDate= new Date(expiredDate);
    if(exDate.getTime()<=now.getTime()){
      return true;
    }
    return false;

  }

}



