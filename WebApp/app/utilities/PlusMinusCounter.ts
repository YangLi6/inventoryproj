import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'plus-minus-counter',
  styles: [`
    .plus-minus-counter {
      position: relative;
      display: inline-block;
    }
    .counter__input {
      border: 0;
      border-radius: 3px;
      /*height: 30px;*/
      max-width: 8rem;
      text-align: center;
    }
    .counter__button {
      outline: 0;
      cursor: pointer;
      /*height: 30px;*/
      width: 3rem;
      border: 0;
      border-radius: 3px;
      background-color: #31b0d5;
      border-color: #269abc;
      color: #fff;
    }
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button { 
      -webkit-appearance: none;
      -moz-appearance: none;
      appearance: none;
      margin: 0; 
    }
    button:disabled {
        background: #dddddd;
    }
  `],
  template: `
    <div class="plus-minus-counter">
      <div class="">
        <button (click)="decrement();" class="counter__button" [disabled]="counterValue<=minValue">
          -
        </button>
        <input type="number" step="any" class="counter__input" [(ngModel)]="counterValue" [ngModelOptions]="{standalone: true}" (ngModelChange)="onChange($event)">
        <button (click)="increment();" class="counter__button" [disabled]="counterValue>=maxValue">
          +
        </button>
      </div>
    </div>
  `,
  //inputs: ['counterValue:init','minValue:min','maxValue:max','changeValue:change']
})
export class PlusMinusCounter {
  //public counterValue = 0;
  @Input() public counterValue: number=0;
  @Input() public minValue:number=0;
  @Input() public maxValue:number=1000;
  @Input() public changeValue:number=1;
  @Output() onCounterChange = new EventEmitter<number>();
  increment() {
    if(this.counterValue<this.maxValue) {
      this.counterValue+=this.changeValue;
      this.counterValue = Math.round(this.counterValue * 100)/100;
    }
    this.onCounterChange.emit(this.counterValue);
  }
  decrement() {
    if(this.counterValue>this.minValue) {
      this.counterValue-=this.changeValue;
      this.counterValue = Math.round(this.counterValue * 100)/100;
    }
    this.onCounterChange.emit(this.counterValue);
  }

  onChange(){
   // this.counterValue=newValue;
    this.counterValue = Math.round(this.counterValue * 100) / 100
    this.onCounterChange.emit(this.counterValue);
  }
}
