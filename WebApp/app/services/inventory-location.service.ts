/**
 * Created by Yang on 2017-04-10.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import{InventoryLocation} from '../models'

@Injectable()
export class InventoryLocationService {
  private inventoryLocationAPIURL=AppSettings.API_ENDPOINT+'api/InventoryLocations';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllInventoryLocations(): Promise<InventoryLocation[]>{
    return this.http.get(this.inventoryLocationAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }


  putInventoryLocation(location:InventoryLocation ): Promise<InventoryLocation>{
    return this.http.put(this.inventoryLocationAPIURL+'/'+location.id,JSON.stringify(location),{headers:this.headers})
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);

  }

  addInventoryLocation(location:InventoryLocation): Promise<InventoryLocation>{
    return this.http.post(this.inventoryLocationAPIURL,JSON.stringify(location),{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
