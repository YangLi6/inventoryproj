/**
 * Created by Yang on 2017-01-18.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from "../app.setting";


import { Product,ProductInventory } from '../models';
import {InventoryLocation} from "../models/product-inventory.model";


@Injectable()
export class ProductInventoryService {

  private productInventoryAPIURL = AppSettings.API_ENDPOINT+'api/ProductInventories';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  // getAllProductInventories():Promise<Product[]>{
  //   return this.http.get(this.productInventoryAPIURL)
  //     .toPromise()
  //     .then(response=> JSON.parse(response['_body']))
  //     .catch(this.handleError);
  // }



  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
