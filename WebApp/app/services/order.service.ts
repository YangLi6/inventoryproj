/**
 * Created by Yang on 2017-01-13.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import { Order } from '../models';
import {OrderProductInventory, PurchaseRequiredProduct} from "../models/order.model";


@Injectable()
export class OrderService {

  private orderAPIURL = AppSettings.API_ENDPOINT+'api/orders';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllOrders(): Promise<Order[]>{
    return this.http.get(this.orderAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  addOrder(o: Order):Promise<Order>{
    return this.http.post(this.orderAPIURL,JSON.stringify(o),{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  deleteOrder(o: Order):Promise<Order>{
    return this.http.delete(this.orderAPIURL+'/'+o.id,{headers:this.headers})
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  updateOrder(o: Order):Promise<Order>{
    return this.http.put(this.orderAPIURL+'/'+o.id,JSON.stringify(o),{headers:this.headers})
      .toPromise()
      .then(response=>JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  shipOrder(o: Order):Promise<Order>{
    return this.http.put(this.orderAPIURL+'/shipped',JSON.stringify(o),{headers:this.headers})
      .toPromise()
      .then(
        response=>JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  updateShipping(o:Order):Promise<Order>{
    return this.http.put(this.orderAPIURL+'/updateshipping',JSON.stringify(o),{headers:this.headers})
      .toPromise()
      .then(
        response=>JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  addOrderProductInventory(opi:OrderProductInventory):Promise<OrderProductInventory>{
    return this.http.post(this.orderAPIURL+'/OrderProductInventory',JSON.stringify(opi),{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  updateOrderProductInventoryQuantity(opi: OrderProductInventory,totalQuantity:number):Promise<OrderProductInventory>{
    return this.http.put(this.orderAPIURL+'/OrderProductInventoryQuantity?id='+opi.id+'&orderQuantity='+totalQuantity,{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  deleteOrderProductInventory(opi: OrderProductInventory):Promise<void>{
    return this.http.delete(this.orderAPIURL+'/OrderProductInventory/?id='+opi.id,{headers:this.headers})
      .toPromise()
      .then(()=>null)
      .catch(this.handleError);
  }

  addPurchaseRequiredProduct(prp:PurchaseRequiredProduct):Promise<PurchaseRequiredProduct>{
    return this.http.post(this.orderAPIURL+'/PurchaseRequiredProduct',JSON.stringify(prp), {headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  deletePurchaseRequiredProduct(prp:PurchaseRequiredProduct):Promise<void>{
    return this.http.delete(this.orderAPIURL+'/PurchaseRequiredProduct/?id='+prp.id,{headers:this.headers})
      .toPromise()
      .then(()=>null)
      .catch(this.handleError);
  }




  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
