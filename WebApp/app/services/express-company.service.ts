/**
 * Created by Yang on 2017-03-23.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import {ExpressCompany} from '../models'

@Injectable()
export class ExpressCompanyService {
  private expressCompanyAPIURL = AppSettings.API_ENDPOINT+'api/ExpressCompanies';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllExpressCompanies(): Promise<ExpressCompany[]>{
    return this.http.get(this.expressCompanyAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  putExpressCompany(expressCompany:ExpressCompany ): Promise<ExpressCompany>{
    return this.http.put(this.expressCompanyAPIURL+'/'+expressCompany.id,JSON.stringify(expressCompany),{headers:this.headers})
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);

  }

  addExpressCompany(expressCompany:ExpressCompany): Promise<ExpressCompany>{
    return this.http.post(this.expressCompanyAPIURL,JSON.stringify(expressCompany),{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
