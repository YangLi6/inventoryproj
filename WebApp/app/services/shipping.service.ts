import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'
import {Shipping} from '../models'

@Injectable()
export class ShippingService {
  private shippingAPIURL = AppSettings.API_ENDPOINT+'api/shippings';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllShippings():Promise<Shipping[]>{
    return this.http.get(this.shippingAPIURL)
      .toPromise()
      .then(response=>JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  putShipping(shipping:Shipping):Promise<Shipping>{
    return this.http.put(this.shippingAPIURL+'/'+shipping.id,JSON.stringify(shipping),{headers:this.headers})
      .toPromise()
      .then(response=>JSON.parse(response['_body']))
      .catch(this.handleError)
  }

  deleteShipping(sh:Shipping):Promise<Shipping>{
    return this.http.delete(this.shippingAPIURL+'/'+sh.id,{headers:this.headers})
      .toPromise()
      .then(response=>JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
