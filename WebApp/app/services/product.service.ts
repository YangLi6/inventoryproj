/**
 * Created by yangli on 2016-12-14.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import { Product } from '../models';

@Injectable()
export class ProductService {

  private productAPIURL = AppSettings.API_ENDPOINT+'api/products';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllProducts():Promise<Product[]>{
    return this.http.get(this.productAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }
  //
  // getProductBySku(skuNumber:string): Promise<Product>{
  //   for(let prod of MockProducts){
  //     if(prod.skuNumber == skuNumber){
  //       return Promise.resolve(prod);
  //     }
  //   }
  //   return Promise.resolve(null);
  // }
  //
  updateProduct(p: Product): Promise<Product>{
    return this.http.put(this.productAPIURL+'/'+p.skuNumber,JSON.stringify(p),{headers:this.headers})
      .toPromise()
      .then(()=>p)
      .catch(this.handleError);

  }

  addProduct(p: Product): Promise<Product>{
    return this.http.post(this.productAPIURL,JSON.stringify(p),{headers:this.headers})
      .toPromise()
      .then(
        ()=>p
      )
      .catch(this.handleError);
  }

  deleteProduct(p: Product): Promise<void>{
    return this.http.delete(this.productAPIURL+'/'+p.skuNumber,{headers:this.headers})
      .toPromise()
      .then(()=>null)
      .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
