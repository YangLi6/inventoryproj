import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import { ProductSupply } from '../models';

@Injectable()
export class ProductSupplyService {

  private productSupplyAPIURL = AppSettings.API_ENDPOINT+'api/productsupplies';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllProductSupplies():Promise<ProductSupply[]>{
    return this.http.get(this.productSupplyAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  updateProductSupplyRecord(ps: ProductSupply): Promise<ProductSupply>{
    return this.http.put(this.productSupplyAPIURL+'/'+ps.id,JSON.stringify(ps),{headers:this.headers})
      .toPromise()
      .then(()=>ps)
      .catch(this.handleError);

  }

  addProductSupplyRecord(ps: ProductSupply): Promise<ProductSupply>{
    return this.http.post(this.productSupplyAPIURL,JSON.stringify(ps),{headers:this.headers})
      .toPromise()
      .then(
        ()=>ps
      )
      .catch(this.handleError);
  }

  deleteProductSupplyRecord(ps: ProductSupply): Promise<void>{
    return this.http.delete(this.productSupplyAPIURL+'/'+ps.id,{headers:this.headers})
      .toPromise()
      .then(()=>null)
      .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

}
