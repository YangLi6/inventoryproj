/**
 * Created by Yang on 2017-03-20.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import { PaymentMethod } from '../models';

@Injectable()
export class PaymentMethodService {

  private paymentMethodAPIURL = AppSettings.API_ENDPOINT+'api/PaymentMethods';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllPaymentMethods():Promise<PaymentMethod[]>{
    return this.http.get(this.paymentMethodAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  putPaymentMethod(paymentMethod:PaymentMethod ): Promise<PaymentMethod>{
    return this.http.put(this.paymentMethodAPIURL+'/'+paymentMethod.id,JSON.stringify(paymentMethod),{headers:this.headers})
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);

  }

  addPaymentMethod(paymentMethod:PaymentMethod): Promise<PaymentMethod>{
    return this.http.post(this.paymentMethodAPIURL,JSON.stringify(paymentMethod),{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
