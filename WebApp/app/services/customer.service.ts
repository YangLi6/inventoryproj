/**
 * Created by yangli on 2016-12-10.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Customer } from '../models';
//import { Customers } from '../mockdata';
import {AppSettings} from '../app.setting'

@Injectable()
export class CustomerService {

  private customerAPIURL = AppSettings.API_ENDPOINT+'api/customers';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllCustomers(): Promise<Customer[]> {
    return this.http.get(this.customerAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  updateCustomer(c: Customer): Promise<Customer>{
    return this.http.put(this.customerAPIURL+'/'+c.id,JSON.stringify(c),{headers:this.headers})
      .toPromise()
      .then(()=>c)
      .catch(this.handleError);
  }

  addCustomer(c: Customer): Promise<Customer>{
    return this.http.post(this.customerAPIURL,JSON.stringify(c),{headers:this.headers})
      .toPromise()
      .then(
        ()=>c
      )
      .catch(this.handleError);

  }

  deleteCustomer(c: Customer): Promise<void> {
    return this.http.delete(this.customerAPIURL+'/'+c.id,{headers:this.headers})
      .toPromise()
      .then(()=>null)
      .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  // See the "Take it slow" appendix
  // getHeroesSlowly(): Promise<Hero[]> {
  //   return new Promise<Hero[]>(resolve =>
  //     setTimeout(resolve, 2000)) // delay 2 seconds
  //     .then(() => this.getHeroes());
  //}
}
