/**
 * Created by yangli on 5/18/2017.
 */
import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {AppSettings} from '../app.setting'

@Injectable()
export class ImageService{

  public imageAPIURL = AppSettings.API_ENDPOINT+'api/Images/';
  private headers = new Headers();

  constructor(private http: Http){}

  public postImage(image:File,mimetype):Promise<any>{
    return this.http.post(this.imageAPIURL,image,{headers:this.headers})
      .toPromise()
      .then(response=>JSON.parse(response['_body']))
      .catch(this.handleError);
  }



  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
