import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import {AppSettings} from '../app.setting'

import {Transfer,TransferProductInventory} from '../models';


@Injectable()
export class TransferService {

  private transferAPIURL = AppSettings.API_ENDPOINT+'api/transfers';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http:Http){}

  getAllTransfers():Promise<Transfer[]>{
    return this.http.get(this.transferAPIURL)
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  addTransfer(t:Transfer):Promise<Transfer>{
    return this.http.post(this.transferAPIURL,JSON.stringify(t),{headers:this.headers})
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  deleteTransfer(t:Transfer):Promise<Transfer>{
    return this.http.delete(this.transferAPIURL+'/'+t.id,{headers:this.headers})
      .toPromise()
      .then(response=> JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  updateTransfer(t: Transfer):Promise<Transfer>{
    return this.http.put(this.transferAPIURL+'/'+t.id,JSON.stringify(t),{headers:this.headers})
      .toPromise()
      .then(response=>JSON.parse(response['_body']))
      .catch(this.handleError);
  }

  shipTransfer(t: Transfer):Promise<Transfer>{
    return this.http.put(this.transferAPIURL+'/shipped',JSON.stringify(t),{headers:this.headers})
      .toPromise()
      .then(
        response=>JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  addTransferProductInventory(tpi:TransferProductInventory):Promise<TransferProductInventory>{
    return this.http.post(this.transferAPIURL+'/TransferedProductInventory',JSON.stringify(tpi),{headers:this.headers})
      .toPromise()
      .then(
        response=>JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  updateTransferedProductInventoryQuantity(tpi: TransferProductInventory,transferedQuantity:number):Promise<TransferProductInventory>{
    return this.http.put(this.transferAPIURL+'/TransferedProductInventoryQuantity?id='+tpi.id+'&transferedQuantity='+transferedQuantity,{headers:this.headers})
      .toPromise()
      .then(
        response=> JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  deleteTransferProductInventory(tpi:TransferProductInventory):Promise<void>{
    return this.http.delete(this.transferAPIURL+'/TransferedProductInventory?id='+tpi.id,{headers:this.headers})
      .toPromise()
      .then(()=>null)
      .catch(this.handleError);
  }

  updateShipping(t:Transfer):Promise<Transfer>{
    return this.http.put(this.transferAPIURL+'/updateshipping',JSON.stringify(t),{headers:this.headers})
      .toPromise()
      .then(
        response=>JSON.parse(response['_body'])
      )
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
