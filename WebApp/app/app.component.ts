import { Component } from '@angular/core';

@Component({
  selector: 'inventory-app',
  template: `
    <app-navigation-header></app-navigation-header>
    <router-outlet></router-outlet>`
})
export class AppComponent  { name = 'Angular'; }


