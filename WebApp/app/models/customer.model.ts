/**
 * Created by yangli on 2016-12-10.
 */
import { GUID } from '../utilities';

export class Customer {
  id: string;
  name: string;
  address: string;
  cellPhone: string;
  note: string;
  alterAddress: string;
  customerIDPhotos: string[];

  constructor(){
    this.id = GUID.getNewGUIDString();

  }
}
