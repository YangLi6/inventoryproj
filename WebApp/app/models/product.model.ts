/**
 * Created by yangli on 2016-12-14.
 */
import {ProductInventory} from "./product-inventory.model";

export class Product{

  //key
  skuNumber: string;
  name: string;
  brand: string;
  hasExpiredDate: boolean;
  weight: number;
  productImages: ProductImage[];
  productInventories:ProductInventory[];

  constructor(){
  }
}

export class ProductImage{
  id:string;
  extension:string;
  imageBaseUrl:string;
  skuNumber:string;
  isDefault:boolean;
  constructor(){
  }
}
