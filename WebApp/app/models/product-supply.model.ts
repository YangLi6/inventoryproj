import { GUID } from '../utilities'

import { Product } from './'


export class ProductSupply {
  //key
  id:string;
  skuNumber:string;
  purchasedPrice: number;
  purchasedDate: Date;
  purchasedStore: string;
  purchasedQuantity: number;
  expiredDate: Date;
  product: Product;


  constructor(){
    this.id = GUID.getNewGUIDString();
  }
}
