/**
 * Created by Yang on 2017-01-12.
 */
import { GUID } from '../utilities'
import {Customer,Shipping} from './'
import {ProductInventory} from "./product-inventory.model";

export class Order {
  id: string;
  customerID: string;
  customer:Customer;
  status:OrderStatus;
  totalWeight:number;
  productCost:number;
  shippingCost:number;
  billing:number;
  paid:number;
  isPaid:boolean;
  paymentMethodID:string;
  note:string;
  placedDate: Date;
  closedDate: Date;
  paidDate:Date;

  orderImages:OrderImage[];
  orderedExistedProducts:OrderProductInventory[];
  purchaseRequiredProducts:PurchaseRequiredProduct[];
  shippings:Shipping[];

  constructor(){
    this.id=GUID.getNewGUIDString();
    //this.customer=null;
    //this.paymentMethod=null;

  }

}

export class PaymentMethod{
  id:string;
  name:string
}

export enum OrderStatus{
  Preparing,
  ReadyToShip,
  Shipped,
  HasProblems,
  Closed
}

export class OrderImage{
  id:string;
  orderID:string;
  imageUrl:string;
}

export class OrderProductInventory {
  id:string;
  orderID:string;
  productInventoryID:string;
  orderedQuantity:number;
  reservedQuantity:number;
  productInventory:ProductInventory;
}

export class PurchaseRequiredProduct{
  id:string;
  name:string;
  skuNumber:string;
  note:string;
  quantity:number;
  orderID:string;
  constructor(){
    this.id = GUID.getNewGUIDString();
  }
}



