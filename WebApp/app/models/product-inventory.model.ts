/**
 * Created by Yang on 2017-01-18.
 */
import { GUID } from '../utilities'
import { Product } from '../models'

export class ProductInventory {
  id: string;
  skuNumber: string;
  product: Product;
  locationID:string;
  expiredDate:Date;
  quantityOnHand:number;
  totalReservedQuantity:number;
  inventoryLocation:InventoryLocation;
  purchasedPrice:number;
  constructor() {
    this.id = GUID.getNewGUIDString();
  }
}

export class InventoryLocation {
  id:string;
  locationName:string;
}
