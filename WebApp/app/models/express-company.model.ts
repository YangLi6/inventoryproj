/**
 * Created by Yang on 2017-03-23.
 */
import { GUID } from '../utilities'

export class ExpressCompany{
  id:string;
  name:string;
  address:string;
  workPhone:string;

  constructor(){
    this.id=GUID.getNewGUIDString();
  }
}
