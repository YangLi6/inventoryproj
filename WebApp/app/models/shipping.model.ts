/**
 * Created by Yang on 2017-03-27.
 */
import { GUID } from '../utilities'

export class Shipping {
  id:string;
  shippedDate:Date;
  receivedDate:Date;
  note:string;
  expressCompanyID:string;
  shippingNumber:string;
  shippingStatus:ShippingStatus;
  shippingCost:number;
  receiver:string;

  constructor() {
    this.id = GUID.getNewGUIDString();
    this.shippingCost=0;
  }
}

export enum ShippingStatus{
  Shipped,
  Received,
  HasProblems
}
