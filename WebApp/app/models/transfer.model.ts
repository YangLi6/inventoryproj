/**
 * Created by Yang on 2017-04-10.
 */
import {GUID} from '../utilities'
import {Customer,Shipping,ProductInventory} from './'

export class Transfer{
  id:string;
  toInventoryLocationID:string;
  status:TransferStatus;
  totalWeight:number;
  productCost:number;
  shippingCost:number;
  note:string;
  placedDate:Date;
  closedDate:Date;

  transferedProducts:TransferProductInventory[];
  shippings:Shipping[];

  constructor(){
    this.id = GUID.getNewGUIDString();
    this.fromInventoryLocationID='';
    this.toInventoryLocationID='';
    this.totalWeight=0;
    this.productCost=0;
    this.shippingCost=0;
    this.note='';
    this.placedDate=new Date();
    this.closedDate=new Date(9999,12,31);
  }

}

export enum TransferStatus{
  Preparing,
  ReadyToShip,
  Shipped,
  HasProblems,
  Closed
}

export class TransferProductInventory {
  id:string;
  transferID:string;
  productInventoryID:string;
  productInventory:ProductInventory;
  transferedQuantity:number;
}

export class TransferShipping{
  id:string;
  transferID:string;
  shippingID:string;

  constructor(){
    this.id=GUID.getNewGUIDString();
    this.transferID='';
    this.shippingID='';
  }
}
