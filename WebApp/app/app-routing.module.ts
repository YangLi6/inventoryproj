/**
 * Created by yangli on 12/9/2016.
 */
import {NgModule}                   from '@angular/core';
import {RouterModule, Routes}     from '@angular/router';

import {CarrierComponent}           from './components/carrier/carrier.component';
import {CustomerComponent}          from './components/customer/customer.component';
import {InventoryComponent}         from './components/inventory/inventory.component';
import {OrderComponent}             from './components/order/order.component';
import {PurchasedComponent}          from './components/purchase/purchase.component';
import {TransferComponent}            from './components/transfer/transfer.component';
import {SettingComponent}           from "./components/setting/setting.component";

const routes: Routes = [
  {path: '', redirectTo: '/order', pathMatch: 'full'},
  {path: 'order', component: OrderComponent},
  {path: 'customer', component: CustomerComponent},
  {path: 'carrier', component: CarrierComponent},
  {path: 'inventory', component: InventoryComponent},
  {path: 'purchase', component: PurchasedComponent},
  {path: 'transfer', component: TransferComponent},
  {path: 'setting', component: SettingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
