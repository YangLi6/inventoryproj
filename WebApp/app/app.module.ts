import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule,JsonpModule  }    from '@angular/http';

import {Ng2CompleterModule} from 'ng2-completer';
import {FileUploadModule } from 'ng2-file-upload';

import { AppComponent }  from './app.component';
import { NavigationHeaderComponent }  from './components/navigation-header/navigation-header.component';
import { CarrierComponent }           from './components/carrier/carrier.component';
import { CustomerComponent }          from './components/customer/customer.component';
import { InventoryComponent }         from './components/inventory/inventory.component';
import { OrderComponent }             from './components/order/order.component';
import { PurchasedComponent }          from './components/purchase/purchase.component';
import { TransferComponent}           from './components/transfer/transfer.component';
import { SettingComponent }         from'./components/setting/setting.component';
import {ImagePreview} from './share-components/image-preview.directive';

import { PlusMinusCounter, OrderByDate,InlineEditor }           from './utilities';

import { AppRoutingModule } from './app-routing.module'


@NgModule({
  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    Ng2CompleterModule,
    FileUploadModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    NavigationHeaderComponent,
    CarrierComponent,
    CustomerComponent,
    InventoryComponent,
    OrderComponent,
    PurchasedComponent,
    TransferComponent,
    SettingComponent,
    PlusMinusCounter,
    OrderByDate,
    InlineEditor,
    ImagePreview
  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
