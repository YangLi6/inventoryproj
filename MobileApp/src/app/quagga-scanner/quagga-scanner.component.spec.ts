import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuaggaScannerComponent } from './quagga-scanner.component';

describe('QuaggaScannerComponent', () => {
  let component: QuaggaScannerComponent;
  let fixture: ComponentFixture<QuaggaScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuaggaScannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuaggaScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
