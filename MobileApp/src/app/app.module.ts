import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AngularMaterialModule} from './angular-material/angular-material.module';

import { AppComponent } from './app.component';
import { QuaggaScannerComponent } from './quagga-scanner/quagga-scanner.component';
import { HeadToolbarComponent } from './head-toolbar/head-toolbar.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { StockPageComponent } from './stock-page/stock-page.component';


@NgModule({
  declarations: [
    AppComponent,
    QuaggaScannerComponent,
    HeadToolbarComponent,
    OrderPageComponent,
    StockPageComponent
  ],
  imports: [
    BrowserModule,
    AngularMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
