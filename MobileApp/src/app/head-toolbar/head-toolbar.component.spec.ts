import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeadToolbarComponent } from './head-toolbar.component';

describe('HeadToolbarComponent', () => {
  let component: HeadToolbarComponent;
  let fixture: ComponentFixture<HeadToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeadToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeadToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
