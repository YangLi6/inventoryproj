﻿using DatabaseAccessLayer.DBModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DatabaseAccessLayer.Repository
{
    public class InventoryManagementDBContext : DbContext, IInventoryManagementDBContext
    {
        public InventoryManagementDBContext() : base("InventoryDB")
        {
            Configuration.LazyLoadingEnabled = false; // Use eager loading.
            Database.SetInitializer<InventoryManagementDBContext>(null); // Prevent database initialization, e.g for production databases.
        }

        //public DbSet<Category> Categories { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerIDPhoto> CustomerIDPhotos { get; set; }
        //public DbSet<CustomerOrderProduct> CustomerOrderProducts { get; set; }
        
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImage> ProductImages { get; set; }

        public DbSet<ProductSupply> ProductSupplies { get; set; }
        public DbSet<ProductInventory> ProductInventories { get; set; }
        public DbSet<InventoryChangeRecord> InventoryChangeRecords { get; set; }
        public DbSet<InventoryLocation> InventoryLocations { get; set; }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderImage> OrderImages { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<OrderProductInventory> OrderProductInventories { get; set; }
        public DbSet<PurchaseRequiredProduct> PurchaseRequiredProducts { get; set; }


        //public DbSet<Receipt> Receipts { get; set; }
        //public DbSet<ReceiptImage> ReceiptImages { get; set; }
        public DbSet<OrderShipping> OrderShippings { get; set; }
        public DbSet<Shipping> Shippings { get; set; }
        public DbSet<ShippingImage> ShippingImages { get; set; }
        public DbSet<ExpressCompany> ExpressCompanies { get; set; }

        public DbSet<Transfer> Transfers { get; set; }
        public DbSet<TransferProductInventory> TransferProductInventories { get; set; }
        public DbSet<TransferShipping> TransferShippings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentNullException("modelBuilder");
            }

           
        }

        public void MarkAsModified(object elem)
        {
            Entry(elem).State = EntityState.Modified;
        }

        public void DetachEntity(object elem)
        {
            //other states are all being tracked
            if (Entry(elem).State != EntityState.Detached)
            {
                Entry(elem).State = EntityState.Detached;
            }
        }

    }
}
