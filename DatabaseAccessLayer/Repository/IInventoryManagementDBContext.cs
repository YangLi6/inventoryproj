﻿using DatabaseAccessLayer.DBModels;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.Repository
{
    public interface IInventoryManagementDBContext : IDisposable
    {
        DbSet<Customer> Customers { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<OrderImage> OrderImages { get; set; }
        DbSet<PaymentMethod> PaymentMethods { get; set; }
        DbSet<OrderProductInventory> OrderProductInventories { get; set; }
        DbSet<PurchaseRequiredProduct> PurchaseRequiredProducts { get; set; }

        DbSet<Product> Products { get; set; }
        DbSet<ProductImage> ProductImages { get; set; }

        DbSet<ProductSupply> ProductSupplies { get; set; }
        DbSet<ProductInventory> ProductInventories { get; set; }
        DbSet<InventoryChangeRecord> InventoryChangeRecords { get; set; }
        DbSet<InventoryLocation> InventoryLocations { get; set; }
        DbSet<OrderShipping> OrderShippings { get; set; }
        DbSet<Shipping> Shippings { get; set; }
        DbSet<ShippingImage> ShippingImages { get; set; }
        DbSet<ExpressCompany> ExpressCompanies { get; set; }
        DbSet<Transfer> Transfers { get; set; }
        DbSet<TransferShipping> TransferShippings { get; set; }
        DbSet<TransferProductInventory> TransferProductInventories { get; set; }


        #region Entity Framework Stuff
        /// <summary>
        /// Saves changes made to the model into the database
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
       
        /// <summary>
        /// Save changes made to the model into the database asynchronously
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        void MarkAsModified(object elem);
        //        void MarkAsNotModified(object elem);
        void DetachEntity(object elem);
        #endregion
    }
}
