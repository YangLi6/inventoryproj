﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("TransferShipping", Schema = "Instance")]
    public class TransferShipping
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid TransferID { get; set; }

        [Required]
        public Guid ShippingID { get; set; }

    }
}
