﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("PurchaseRequiredProduct", Schema = "Instance")]
    public class PurchaseRequiredProduct
    {
        [Key]
        public Guid ID { get; set; }
        public string Name { get; set; }
        public string SkuNumber { get; set; }
        public string Note { get; set; }
        public int Quantity { get; set; }

        [Required]
        public Guid OrderID { get; set; }
    }
}
