﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Transfer",Schema="Instance")]
    public class Transfer
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid ToInventoryLocationID { get; set; }

        [Required]
        public TransferStatus Status { get; set; }

        public double TotalWeight { get; set; }

        public double ProductCost { get; set; }
        public double ShippingCost { get; set; }

        //public bool IsCompleted { get; set; }

        public string Note { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? PlacedDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ClosedDate { get; set; }

        public ICollection<TransferProductInventory> TransferedProducts;
        public ICollection<Shipping> Shippings;
    }

    public enum TransferStatus
    {
        Preparing,
        ReadyToShip,
        Shipped,
        HasProblems,
        Closed
    }
}
