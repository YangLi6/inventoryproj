﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Customer", Schema = "User")]
    public class Customer
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }


        public string Address { get; set; }

        public string AlterAddress{ get; set; }

        public string CellPhone { get; set; }

        public string Note { get; set; }

        public ICollection<CustomerIDPhoto> CustomerIDPhotos { get; set; }
        
    }
}
