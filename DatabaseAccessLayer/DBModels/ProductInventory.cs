﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("ProductInventory", Schema = "Instance")]
    public class ProductInventory
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string SkuNumber { get; set; }

        [ForeignKey("SkuNumber")]
        public virtual Product Product { get; set; }

        [Required]
        public Guid LocationID { get; set; }

        [ForeignKey("LocationID")]
        public virtual InventoryLocation InventoryLocation { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ExpiredDate { get; set; }

        [Required]
        public int QuantityOnHand { get; set; }

        [Required]
        public int TotalReservedQuantity { get; set; }

        [Required]
        public double PurchasedPrice { get; set; }


        //public ICollection<ProductSupply> ProductSupplies { get; set; }

        //public ICollection<Order> Orders{ get; set; }

        //public ICollection<InventoryChangeRecord> InventoryChangeRecords { get; set; }
    }
}
