﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("ExpressCompany", Schema = "Resource")]
    public class ExpressCompany
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string WorkPhone { get; set; }

        public string Address { get; set; }

    }
}
