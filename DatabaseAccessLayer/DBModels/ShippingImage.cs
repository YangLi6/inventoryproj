﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("ShippingImage", Schema = "Instance")]
    public class ShippingImage
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid ShippingID { get; set; }

        [ForeignKey("ShippingID")]
        public virtual Shipping Shipping{ get; set; }

        public string ImageUrl { get; set; }

    }
}
