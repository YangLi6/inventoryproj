﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Receipt", Schema = "Instance")]
    public class Receipt
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime PlacedDate { get; set; }

        [Required]
        public float Total { get; set; }

        public string CompanyName { get; set; }

        public int NumberItem { get; set; }
    }
}
