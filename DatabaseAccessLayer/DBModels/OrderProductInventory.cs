﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("OrderProductInventory", Schema = "Instance")]
    public class OrderProductInventory
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid OrderID { get; set; }

        [Required]
        public Guid ProductInventoryID { get; set; }
        public virtual ProductInventory ProductInventory { get; set; }


        [Required]
        public int OrderedQuantity { get; set; }

        [Required]
        public int ReservedQuantity { get; set; }


        //[Required]
        //public string SkuNumber { get; set; }

        //[ForeignKey("SkuNumber")]
        //public virtual Product Product { get; set; }
    }
}
