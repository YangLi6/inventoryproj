﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Category", Schema = "Resource")]
    public class Category
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
