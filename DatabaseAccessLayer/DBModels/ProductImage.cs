﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("ProductImage", Schema = "Instance")]
    public class ProductImage
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string Extension { get; set; }

        [Required]
        public string ImageBaseUrl { get; set; }

        [Required]
        public string SkuNumber { get; set; }

        [Required]
        public bool IsDefault { get; set; }

        [ForeignKey("SkuNumber")]
        public virtual Product Product { get; set; }


    }
}
