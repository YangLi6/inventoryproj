﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Product", Schema = "Instance")]
    public class Product
    {
        [Key]
        public string SkuNumber { get; set; }

        [Required]
        public string Name { get; set; }

        //[Required]
        public string Brand { get; set; }

        //public Guid CategoryID { get; set; }

        [Required]
        public bool HasExpiredDate { get; set; }

        //[Required]
        public double Weight { get; set; }

        public ICollection<ProductImage> ProductImages { get; set; }

        public ICollection<ProductInventory> ProductInventories{ get; set; }
    }
}
