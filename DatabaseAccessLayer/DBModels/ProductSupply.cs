﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("ProductSupply", Schema = "Instance")]
    public class ProductSupply
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string SkuNumber { get; set; }

        [ForeignKey("SkuNumber")]
        public virtual Product Product { get; set; }

        [Required]
        public double PurchasedPrice { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime PurchasedDate { get; set; }

        public string PurchasedStore { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ExpiredDate { get; set; }

        [Required]
        public int PurchasedQuantity { get; set; }
    }
}
