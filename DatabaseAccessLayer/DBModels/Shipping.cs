﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Shipping", Schema = "Instance")]
    public class Shipping
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ShippedDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ReceivedDate { get; set; }

        public string Note { get; set; }

        [Required]
        public Guid ExpressCompanyID { get; set; }

        [Required]
        public string ShippingNumber { get; set; }

        public ShippingStatus ShippingStatus{ get; set; }

        public double ShippingCost { get; set; }

        public string Receiver { get; set; }

    }

    public enum ShippingStatus
    {
        Shipped,
        Received,
        HasProblems
    }
}
