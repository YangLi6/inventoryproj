﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("InventoryLocation", Schema = "Instance")]
    public class InventoryLocation
    {
        [Key]
        public Guid ID { get; set; }

        public string LocationName { get; set; }

    }
}
