﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("CustomerIDPhoto", Schema = "User")]
    public class CustomerIDPhoto
    {
        [Key]
        public Guid ID { get; set; }
        public string URL { get; set; }
        
        public Guid CustomerID { get; set; }

        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }
    }
}
