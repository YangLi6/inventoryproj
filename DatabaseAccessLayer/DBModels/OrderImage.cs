﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("OrderImage", Schema = "Instance")]

    public class OrderImage
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid OrderID { get; set; }

        public string ImageUrl { get; set; }
    }
}
