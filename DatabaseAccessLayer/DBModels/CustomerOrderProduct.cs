﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("CustomerOrderProduct", Schema = "Instance")]
    public class CustomerOrderProduct
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid OrderID { get; set; }

        
        public Guid ProductID { get; set; }

        public string NonExistedProductName { get; set; }

        [Required]
        public int Quantity { get; set; }
        
        public float EachSoldPrice { get; set; }

        public string Notes { get; set; }

    }
}
