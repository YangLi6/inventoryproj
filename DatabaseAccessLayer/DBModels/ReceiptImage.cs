﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("ReceiptImage", Schema = "Instance")]
    public class ReceiptImage
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid ReceiptID { get; set; }

        [ForeignKey("ReceiptID")]
        public virtual Receipt Receipt{ get; set; }

        public string ImageUrl { get; set; }
    }
}
