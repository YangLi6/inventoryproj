﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("Order", Schema = "Instance")]
    public class Order
    {
        [Key]
        public Guid ID { get; set; }

        //Client Info
        [Required]
        public Guid CustomerID { get; set; }

        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }

        [Required]
        public OrderStatus Status { get; set; }

        public double TotalWeight { get; set; }

        public double ProductCost{ get; set; }

        public double ShippingCost { get; set; }

        public double Billing { get; set; }

        public double Paid { get; set; }

        public bool IsPaid { get; set; }

        
        public Guid? PaymentMethodID { get; set; }

        //public virtual PaymentMethod PaymentMethod { get; set; }

        public string Note{ get; set; }


        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime PlacedDate { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime ClosedDate { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime PaidDate { get; set; }

        public ICollection<OrderImage> OrderImages;

        public ICollection<OrderProductInventory> OrderedExistedProducts;

        public ICollection<PurchaseRequiredProduct> PurchaseRequiredProducts;

        public ICollection<Shipping> Shippings;

    }

    public enum OrderStatus
    {
        Preparing,
        ReadyToShip,
        Shipped,
        HasProblems,
        Closed
    }
}
