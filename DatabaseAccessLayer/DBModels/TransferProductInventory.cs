﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("TransferProductInventory", Schema = "Instance")]
    public class TransferProductInventory
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public Guid TransferID { get; set; }

        [Required]
        public Guid ProductInventoryID { get; set; }
        public virtual ProductInventory ProductInventory { get; set; }


        [Required]
        public int TransferedQuantity { get; set; }

    }
}
