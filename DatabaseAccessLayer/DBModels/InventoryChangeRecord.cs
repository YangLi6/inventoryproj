﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccessLayer.DBModels
{
    [Table("InventoryChangeRecord", Schema = "Instance")]
    public class InventoryChangeRecord
    {
        [Key]
        public Guid ID { get; set; }

        [Required]
        public string SkuNumber { get; set; }

        //[ForeignKey("SkuNumber")]
        //public virtual Product Product { get; set; }

        [Required]
        public Guid ProductInventoryID { get; set; }

        [ForeignKey("ProductInventoryID")]
        public virtual ProductInventory ProductInventory{ get; set; }

        [Required]
        public int ChangeQuantity { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime ChangeDate { get; set; }

        [Required]
        public InventoryChangeReason ChangeReason { get; set; }

        public string ChangeNote { get; set; }

        public Guid FromLocationID { get; set; }

        //public virtual InventoryLocation FromLocation { get; set; }

        public Guid ToLocationID { get; set; }
        //public virtual InventoryLocation ToLocation { get; set; }

    }


    public enum InventoryChangeReason
    {
        Purchase,
        PurchaseAdjust,
        Order,
        OrderAdjust,
        TransferLocation,
        DecreaseWithCost,
        DecreaseWithoutCost,
        IncreaseWithoutCost
    }
}
