namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTotalReservedNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.ProductInventory", "TotalReservedQuantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Instance.ProductInventory", "TotalReservedQuantity");
        }
    }
}
