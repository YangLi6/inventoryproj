namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameNoRecordProductTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Instance.NoRecordProduct", newName: "PurchaseRequiredProduct");
            AddColumn("Instance.PurchaseRequiredProduct", "SkuNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Instance.PurchaseRequiredProduct", "SkuNumber");
            RenameTable(name: "Instance.PurchaseRequiredProduct", newName: "NoRecordProduct");
        }
    }
}
