namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMoreInfoInOPI : DbMigration
    {
        public override void Up()
        {
            CreateIndex("Instance.OrderProductInventory", "ProductInventoryID");
            AddForeignKey("Instance.OrderProductInventory", "ProductInventoryID", "Instance.ProductInventory", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Instance.OrderProductInventory", "ProductInventoryID", "Instance.ProductInventory");
            DropIndex("Instance.OrderProductInventory", new[] { "ProductInventoryID" });
        }
    }
}
