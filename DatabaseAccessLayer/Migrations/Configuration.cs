﻿namespace DatabaseAccessLayer.Migrations
{
    using DBModels;
    using Repository;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.InventoryManagementDBContext>
    {
        private const int localtimeOffset = 7;

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(Repository.InventoryManagementDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            // Deletes all data, from all tables, except for __MigrationHistory
            ClearAllData(context);

            SeedCustomer(context);
            SeedProduct(context);
            SeedProductSupply(context);
            SeedInventoryLocation(context);
            SeedProductInventory(context);
            SeedOrders(context);
            SeedShippings(context);
            SeedTransfer(context);


        }

        public void ClearAllData(InventoryManagementDBContext context)
        {
            context.Database.ExecuteSqlCommand("sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'");
            context.Database.ExecuteSqlCommand("sp_MSForEachTable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[dbo].[__MigrationHistory]''),0)) DELETE FROM ?'");
            context.Database.ExecuteSqlCommand("EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'");
        }

        public void SeedCustomer(InventoryManagementDBContext context)
        {
            
            context.Customers.AddOrUpdate(
                c => c.ID,
                new Customer() { ID = Guid.Parse("E46E5038-3E2E-4BC5-ABB8-DB2B5EF1FD1F"), Name = "李先生", Address = "河北省邯郸市滏东北大街187号12号楼74号", CellPhone = "0123456789" },
                new Customer() { ID = Guid.Parse("FB926DF3-9221-46E1-8E88-E94447705E90"), Name = "张先生", Address = "上海市松江区江学路", CellPhone = "0123456789" },
                new Customer() { ID = Guid.Parse("EC3A2256-5364-42F4-854E-876F04FD6206"), Name = "王先生", Address = "山东省青岛市一二三街五六七号楼八九十号", CellPhone = "0123456789" },
                new Customer() { ID = Guid.Parse("20F3A626-A2A5-419C-BEAA-A4651B6675BA"), Name = "赵先生", Address = "广东省广州市阳光大街981号湖畔小区85号楼2015室", CellPhone = "0123456789" }
                );

            context.CustomerIDPhotos.AddOrUpdate(
                cp => cp.ID,
                new CustomerIDPhoto() { ID = Guid.Parse("5F543D12-F4C6-4360-8133-094D48853729"), CustomerID = Guid.Parse("E46E5038-3E2E-4BC5-ABB8-DB2B5EF1FD1F"), URL = "http://www.xj.xinhuanet.com/2010-11/28/xin_0631107281218296774814.jpg" },
                new CustomerIDPhoto() { ID = Guid.Parse("4DCC994A-D719-412E-9489-F94C1126A28C"), CustomerID = Guid.Parse("E46E5038-3E2E-4BC5-ABB8-DB2B5EF1FD1F"), URL = "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR6KRRMHEW3nqp2Jc93YuCOeKKnE5YIyZj4pYw-f9lOexPffHExvQ" },
                new CustomerIDPhoto() { ID = Guid.Parse("1A8F644F-0242-4F1A-91F4-FFC3E538FA3D"), CustomerID = Guid.Parse("20F3A626-A2A5-419C-BEAA-A4651B6675BA"), URL = "http://www.xj.xinhuanet.com/2010-11/28/xin_0631107281218296774814.jpg" },
                new CustomerIDPhoto() { ID = Guid.Parse("0247D2A9-E3B4-4704-BC45-91A16ED80E4F"), CustomerID = Guid.Parse("20F3A626-A2A5-419C-BEAA-A4651B6675BA"), URL = "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR6KRRMHEW3nqp2Jc93YuCOeKKnE5YIyZj4pYw-f9lOexPffHExvQ" }
                );
        }

        public void SeedProduct(InventoryManagementDBContext context)
        {
            context.Products.AddOrUpdate(
                p => p.SkuNumber,
                new Product { SkuNumber = "12345678", Name = "iPhone 7", Brand = "Apple", HasExpiredDate = false, Weight = 1 },
                new Product { SkuNumber = "12345679", Name = "iPhone 6s", Brand = "Apple", HasExpiredDate = false, Weight = 1 },
                new Product { SkuNumber = "12345680", Name = "iPhone 6", Brand = "Apple", HasExpiredDate = false, Weight = 1 },
                new Product { SkuNumber = "12345681", Name = "iPhone 5s", Brand = "Apple", HasExpiredDate = false, Weight = 1 },
                new Product { SkuNumber = "12345682", Name = "iPhone 5", Brand = "Apple", HasExpiredDate = false, Weight = 1 },
                new Product { SkuNumber = "12345683", Name = "小铁人鱼油", Brand = "IRONKIDS", HasExpiredDate = true, Weight = 2 },
                new Product { SkuNumber = "12345684", Name = "小铁人鱼油(快过期了)", Brand = "IRONKIDS", HasExpiredDate = true, Weight = 2 }
                );

            //context.ProductImages.AddOrUpdate(
            //    pi => pi.ID,
            //    new ProductImage() { ID = Guid.Empty, Extension = ".jpg", SkuNumber = "EmptyProductImage", ImageBaseUrl = "https://sandbox7647.blob.core.windows.net/productimages" }
            //    //new ProductImage() { ID = Guid.Parse("885391F1-B185-4B07-B279-DDE1ADF9D10F"), SkuNumber = "12345678", ImageUrl = "http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s1.jpg" },
            //    //new ProductImage() { ID = Guid.Parse("A1B7CD05-0918-4320-9E57-AEAC6BE21D57"), SkuNumber = "12345679", ImageUrl = "http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s1.jpg" },
            //    //new ProductImage() { ID = Guid.Parse("9D4A417A-EAF7-4B49-A0C0-DEEE46D701F2"), SkuNumber = "12345680", ImageUrl = "https://hurtel.pl/pol_pl_Atrapa-iPhone-6-zloty-4820_5.jpg" },
            //    //new ProductImage() { ID = Guid.Parse("E5B0DF68-892C-4FED-A704-45B6946BD97E"), SkuNumber = "12345681", ImageUrl = "http://drop.ndtv.com/TECH/product_database/images/918201370949PM_635_iPhone-5s.jpeg" },
            //    //new ProductImage() { ID = Guid.Parse("25941269-B66D-4AEF-A870-63A41733DE7E"), SkuNumber = "12345682", ImageUrl = "http://drop.ndtv.com/TECH/product_database/images/918201370949PM_635_iPhone-5s.jpeg" },
            //    //new ProductImage() { ID = Guid.Parse("88F45B99-71C9-4697-B63F-712CE6E407CC"), SkuNumber = "12345683", ImageUrl = "https://d3t32hsnjxo7q6.cloudfront.net/i/3979abce0e83e96ac0ef668c7f5a1136_ra,w380,h380_pa,w380,h380.jpg" },
            //    //new ProductImage() { ID = Guid.Parse("9CD46119-D9EE-4D8D-8D19-BD33F1EECE3F"), SkuNumber = "12345684", ImageUrl = "https://d3t32hsnjxo7q6.cloudfront.net/i/3979abce0e83e96ac0ef668c7f5a1136_ra,w380,h380_pa,w380,h380.jpg" }
            //    );
        }

        public void SeedProductSupply(InventoryManagementDBContext context)
        {
            context.ProductSupplies.AddOrUpdate(
                ps => ps.ID,
                new ProductSupply() { ID = Guid.Parse("C204C908-4FA2-435F-8237-B21A5C5515DC"), SkuNumber = "12345678", PurchasedPrice = 890, PurchasedStore = "Chinook Apple Store", PurchasedDate = new DateTime(2016, 11, 3).AddHours(localtimeOffset), PurchasedQuantity = 1 },
                new ProductSupply() { ID = Guid.Parse("290ABB40-B33C-4AEB-AF21-4DF08F105A09"), SkuNumber = "12345679", PurchasedPrice = 790, PurchasedStore = "Chinook Apple Store", PurchasedDate = new DateTime(2015, 11, 7).AddHours(localtimeOffset), PurchasedQuantity = 1 },
                new ProductSupply() { ID = Guid.Parse("D0DFC5B5-E805-41BD-AA3A-698136F2CDD1"), SkuNumber = "12345680", PurchasedPrice = 690, PurchasedStore = "Chinook Apple Store", PurchasedDate = new DateTime(2014, 12, 10).AddHours(localtimeOffset), PurchasedQuantity = 1 },
                new ProductSupply() { ID = Guid.Parse("2D1565BC-DA5A-409B-A2B2-DF5E551EA5B0"), SkuNumber = "12345681", PurchasedPrice = 590, PurchasedStore = "Chinook Apple Store", PurchasedDate = new DateTime(2013, 11, 30).AddHours(localtimeOffset), PurchasedQuantity = 1 },
                new ProductSupply() { ID = Guid.Parse("E4D92FE7-327C-436F-9518-CBD548885845"), SkuNumber = "12345682", PurchasedPrice = 590, PurchasedStore = "MarketMall Apple Store", PurchasedDate = new DateTime(2013, 11, 24).AddHours(localtimeOffset), PurchasedQuantity = 3 },
                new ProductSupply() { ID = Guid.Parse("03F968BB-F02A-4BC6-B9A8-23E67EB349E1"), SkuNumber = "12345682", PurchasedPrice = 590, PurchasedStore = "MarketMall Apple Store", PurchasedDate = new DateTime(2013, 12, 1).AddHours(localtimeOffset), PurchasedQuantity = 1 },
                new ProductSupply() { ID = Guid.Parse("CC6C18E2-C5C0-42DE-8A9F-EF17B503E86F"), SkuNumber = "12345683", PurchasedPrice = 12.99, PurchasedStore = "Costco", PurchasedDate = new DateTime(2016, 12, 31).AddHours(localtimeOffset), ExpiredDate = new DateTime(2018, 5, 1).AddHours(localtimeOffset), PurchasedQuantity = 1 },
                new ProductSupply() { ID = Guid.Parse("FF88C822-A547-4B2C-8756-9328F37AB4D1"), SkuNumber = "12345683", PurchasedPrice = 14.99, PurchasedStore = "Costco", PurchasedDate = new DateTime(2017, 1, 1).AddHours(localtimeOffset), ExpiredDate = new DateTime(2018, 6, 1).AddHours(localtimeOffset), PurchasedQuantity = 2 },
                new ProductSupply() { ID = Guid.Parse("C84356AA-1F85-473E-90E2-97B9BCF8EE8D"), SkuNumber = "12345683", PurchasedPrice = 13.99, PurchasedStore = "Costco", PurchasedDate = new DateTime(2017, 1, 4).AddHours(localtimeOffset), ExpiredDate = new DateTime(2017, 1, 1).AddHours(localtimeOffset), PurchasedQuantity = 3 },
                new ProductSupply() { ID = Guid.Parse("B7E0F808-A582-4445-B508-CFFD5BFFFE65"), SkuNumber = "12345684", PurchasedPrice = 11.99, PurchasedStore = "Costco", PurchasedDate = new DateTime(2017, 1, 5).AddHours(localtimeOffset), ExpiredDate = new DateTime(2017, 5, 1).AddHours(localtimeOffset), PurchasedQuantity = 4 },
                new ProductSupply() { ID = Guid.Parse("EE7C8BB7-FE00-4A2D-8987-67BA0963052D"), SkuNumber = "12345684", PurchasedPrice = 12.99, PurchasedStore = "Costco", PurchasedDate = new DateTime(2017, 1, 6).AddHours(localtimeOffset), ExpiredDate = new DateTime(2017, 5, 1).AddHours(localtimeOffset), PurchasedQuantity = 10 }

                );
        }

        public void SeedInventoryLocation(InventoryManagementDBContext context)
        {
            context.InventoryLocations.AddOrUpdate(
                il=> il.ID,
                new InventoryLocation() { ID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), LocationName = "卡尔加里-Fiona" },
                new InventoryLocation() { ID = Guid.Parse("D4AEB19B-44D5-4B38-B44E-8F8DAC9847F1"), LocationName = "上海-潘赛芬" },
                new InventoryLocation() { ID = Guid.Parse("7F614409-3C1E-4C62-AFC6-AFFD7D92ECE9"), LocationName = "上海-康美红" },
                new InventoryLocation() { ID = Guid.Parse("CBC3623F-0A7D-4265-B980-FF63C24D6A69"), LocationName = "上海-金衍亮" },
                new InventoryLocation() { ID = Guid.Parse("29973BD4-6237-4C79-84F3-AB179B5DCAA3"), LocationName = "上海-爸妈" },
                new InventoryLocation() { ID = Guid.Parse("525B0883-B064-41F8-9380-BE7028AD7EF4"), LocationName = "邯郸-阳家" }
                );
        }


        public void SeedProductInventory(InventoryManagementDBContext context)
        {
            context.ProductInventories.AddOrUpdate(
                pi => pi.ID,
                new ProductInventory() { ID = Guid.Parse("241E3EDC-900A-4836-924A-AD471E263A7D"), SkuNumber = "12345678", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = DateTime.MinValue, QuantityOnHand = 1, PurchasedPrice = 890, TotalReservedQuantity = 1 },
                new ProductInventory() { ID = Guid.Parse("728AB5E2-FF2F-4DBA-B634-9B0F19C9F6D5"), SkuNumber = "12345679", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = DateTime.MinValue, QuantityOnHand = 1, PurchasedPrice = 790, TotalReservedQuantity = 1 },
                new ProductInventory() { ID = Guid.Parse("2EA4B94E-E404-4B68-8783-5497B3099EDF"), SkuNumber = "12345680", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = DateTime.MinValue, QuantityOnHand = 1, PurchasedPrice = 690, TotalReservedQuantity = 1 },
                new ProductInventory() { ID = Guid.Parse("B614E84F-561D-4C3B-9DC8-4AFAF0A97847"), SkuNumber = "12345681", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = DateTime.MinValue, QuantityOnHand = 1, PurchasedPrice = 590, TotalReservedQuantity = 1 },
                new ProductInventory() { ID = Guid.Parse("B0DAD4AD-3A1A-4302-9101-88C956A69EEF"), SkuNumber = "12345682", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = DateTime.MinValue, QuantityOnHand = 4, PurchasedPrice = 590, TotalReservedQuantity = 4},
                new ProductInventory() { ID = Guid.Parse("C7D8A3E2-CAA5-42C6-B0A2-FBD2962253A5"), SkuNumber = "12345683", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = new DateTime(2018, 5, 1).AddHours(localtimeOffset), QuantityOnHand = 2, PurchasedPrice = 12.99, TotalReservedQuantity = 2 },
                new ProductInventory() { ID = Guid.Parse("CD4E9B47-AFE7-40C6-9AC9-F659FDC41116"), SkuNumber = "12345683", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = new DateTime(2018, 6, 1).AddHours(localtimeOffset), QuantityOnHand = 2, PurchasedPrice = 14.99, TotalReservedQuantity = 1 },
                new ProductInventory() { ID = Guid.Parse("F681978E-F7B2-4C2A-914B-104763913094"), SkuNumber = "12345683", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = new DateTime(2017, 1, 1).AddHours(localtimeOffset), QuantityOnHand = 3, PurchasedPrice = 13.99, TotalReservedQuantity = 3},
                new ProductInventory() { ID = Guid.Parse("5091CB78-4DE9-4701-B5D4-3BC624F4B1FF"), SkuNumber = "12345684", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = new DateTime(2017, 5, 1).AddHours(localtimeOffset), QuantityOnHand = 4, PurchasedPrice = 11.99, TotalReservedQuantity = 3},
                new ProductInventory() { ID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), SkuNumber = "12345684", LocationID = Guid.Parse("165AFE5F-5ECC-444C-992A-8A37F4B3CA48"), ExpiredDate = new DateTime(2017, 5, 1).AddHours(localtimeOffset), QuantityOnHand = 10, PurchasedPrice = 12.99, TotalReservedQuantity =4 }
                );
        }

        

        public void SeedOrders(InventoryManagementDBContext context)
        {
            context.PaymentMethods.AddOrUpdate(
                pm=>pm.ID,
                new PaymentMethod() { ID = Guid.Parse("567A8990-F713-4A9D-9EAC-A80235EE1CCC"), Name = "微信" },
                new PaymentMethod() { ID = Guid.Parse("33B5BD17-7BAF-44C3-A295-6279BB121D4C"), Name = "支付宝" },
                new PaymentMethod() { ID = Guid.Parse("7C0D75EC-5BA4-447B-AF0B-2AED70182905"), Name = "淘宝" }
                );

            context.Orders.AddOrUpdate(
                order => order.ID,
                new Order() { ID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), CustomerID = Guid.Parse("E46E5038-3E2E-4BC5-ABB8-DB2B5EF1FD1F"), Status = OrderStatus.Preparing, TotalWeight = 1+2*4, ProductCost = 890+14.99+12.99*2+11.99, ShippingCost = 0, Billing = 0, Paid = 0, PaymentMethodID = Guid.Parse("567A8990-F713-4A9D-9EAC-A80235EE1CCC"), Note = "", PlacedDate = new DateTime(2017, 1, 3).AddHours(localtimeOffset), PaidDate = DateTime.MinValue.AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), IsPaid=false },
                new Order() { ID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), CustomerID = Guid.Parse("FB926DF3-9221-46E1-8E88-E94447705E90"), Status = OrderStatus.Preparing, TotalWeight = 1+2*4, ProductCost = 790+12.99+13.99*3, ShippingCost = 0, Billing = 0, Paid = 0, PaymentMethodID = Guid.Parse("567A8990-F713-4A9D-9EAC-A80235EE1CCC"), Note = "", PlacedDate = new DateTime(2017, 1, 4).AddHours(localtimeOffset), PaidDate = DateTime.MinValue.AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), IsPaid = false },
                //new Order() { ID = Guid.Parse("41700792-6059-4293-977B-72A9A9650F81"), CustomerID = Guid.Parse("FB926DF3-9221-46E1-8E88-E94447705E90"), Status = OrderStatus.Preparing, TotalWeight = 1, ProductCost = -1, ShippingCost = -1, Billing = -1, Paid = -1, PaymentMethodID = Guid.Parse("567A8990-F713-4A9D-9EAC-A80235EE1CCC"), Note = "", PlacedDate = new DateTime(2017, 1, 5).AddHours(localtimeOffset), PaidDate = DateTime.MinValue, ClosedDate = DateTime.MinValue },
                new Order() { ID = Guid.Parse("57C8B2EC-B017-45CB-9E7F-429B369995BF"), CustomerID = Guid.Parse("EC3A2256-5364-42F4-854E-876F04FD6206"), Status = OrderStatus.Preparing, TotalWeight = 1+2*2, ProductCost = 690+11.99*2, ShippingCost = 0, Billing = 0, Paid = 0, PaymentMethodID = Guid.Parse("7C0D75EC-5BA4-447B-AF0B-2AED70182905"), Note = "", PlacedDate = new DateTime(2017, 1, 6).AddHours(localtimeOffset), PaidDate = DateTime.MinValue.AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), IsPaid = false },
                //new Order() { ID = Guid.Parse("11C42724-D941-4A81-B4C4-B8BCBE9DC87A"), CustomerID = Guid.Parse("EC3A2256-5364-42F4-854E-876F04FD6206"), Status = OrderStatus.Preparing, TotalWeight = 1, ProductCost = -1, ShippingCost = -1, Billing = -1, Paid = -1, PaymentMethodID = Guid.Parse("7C0D75EC-5BA4-447B-AF0B-2AED70182905"), Note = "", PlacedDate = new DateTime(2017, 1, 7).AddHours(localtimeOffset), PaidDate = DateTime.MinValue, ClosedDate = DateTime.MinValue },
                //new Order() { ID = Guid.Parse("F853F8D5-5A83-4511-BE50-6D809DB40D9A"), CustomerID = Guid.Parse("EC3A2256-5364-42F4-854E-876F04FD6206"), Status = OrderStatus.Preparing, TotalWeight = 1, ProductCost = -1, ShippingCost = -1, Billing = -1, Paid = -1, PaymentMethodID = Guid.Parse("7C0D75EC-5BA4-447B-AF0B-2AED70182905"), Note = "", PlacedDate = new DateTime(2017, 1, 8).AddHours(localtimeOffset), PaidDate = DateTime.MinValue, ClosedDate = DateTime.MinValue },
                new Order() { ID = Guid.Parse("DFAD5F14-E10B-4783-BDB4-C3455E21E817"), CustomerID = Guid.Parse("20F3A626-A2A5-419C-BEAA-A4651B6675BA"), Status = OrderStatus.Preparing, TotalWeight = 5, ProductCost = 590*5, ShippingCost = 0, Billing = 0, Paid = 0, PaymentMethodID = Guid.Parse("7C0D75EC-5BA4-447B-AF0B-2AED70182905"), Note = "", PlacedDate = new DateTime(2017, 1, 9).AddHours(localtimeOffset), PaidDate = DateTime.MinValue.AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), IsPaid = false }
                );

            context.OrderProductInventories.AddOrUpdate(
                opi => opi.ID,
                new OrderProductInventory() { ID = Guid.Parse("6AE5EC61-EF46-43FE-A0ED-5C366CE80CBC"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), ProductInventoryID = Guid.Parse("241E3EDC-900A-4836-924A-AD471E263A7D"), OrderedQuantity = 1, ReservedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("85E177DA-251D-4847-A153-4CAF05B39F5A"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), ProductInventoryID = Guid.Parse("CD4E9B47-AFE7-40C6-9AC9-F659FDC41116"), OrderedQuantity = 1, ReservedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("8CF89971-5D90-4926-8545-49B56924629E"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), ProductInventoryID = Guid.Parse("C7D8A3E2-CAA5-42C6-B0A2-FBD2962253A5"), OrderedQuantity = 2, ReservedQuantity = 2 },
                new OrderProductInventory() { ID = Guid.Parse("51C8B1C4-C726-44EA-899C-06CABEEB1B09"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), ProductInventoryID = Guid.Parse("5091CB78-4DE9-4701-B5D4-3BC624F4B1FF"), OrderedQuantity = 1, ReservedQuantity = 1 },
                //new OrderProductInventory() { ID = Guid.Parse("60217DCE-354D-4F33-ABF3-BBBC783EB465"), OrderID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), ProductInventoryID = Guid.Parse("C7D8A3E2-CAA5-42C6-B0A2-FBD2962253A5"), OrderedQuantity = 1, ReservedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("C33ADC6D-3FEE-40F1-ADDC-03A33142A375"), OrderID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), ProductInventoryID = Guid.Parse("728AB5E2-FF2F-4DBA-B634-9B0F19C9F6D5"), OrderedQuantity = 1, ReservedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("85703495-61A5-4A77-8B3B-4D243A3E8A83"), OrderID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), ProductInventoryID = Guid.Parse("F681978E-F7B2-4C2A-914B-104763913094"), OrderedQuantity = 3, ReservedQuantity = 3 },
                //new OrderProductInventory() { ID = Guid.Parse("B12B477F-2C8F-4587-8E74-FCFEF874FA3C"), OrderID = Guid.Parse("41700792-6059-4293-977B-72A9A9650F81"), ProductInventoryID = Guid.Parse("B614E84F-561D-4C3B-9DC8-4AFAF0A97847"), OrderedQuantity = 1 },
                //new OrderProductInventory() { ID = Guid.Parse("9B577337-764F-45A5-8F1A-49F73D613AC9"), OrderID = Guid.Parse("41700792-6059-4293-977B-72A9A9650F81"), ProductInventoryID = Guid.Parse("B0DAD4AD-3A1A-4302-9101-88C956A69EEF"), OrderedQuantity = 1 },
                //new OrderProductInventory() { ID = Guid.Parse("DF90EEE8-1A78-4C0A-B208-5D5ACE011925"), OrderID = Guid.Parse("41700792-6059-4293-977B-72A9A9650F81"), ProductInventoryID = Guid.Parse("F681978E-F7B2-4C2A-914B-104763913094"), OrderedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("7C4A1AD0-CF04-48A8-B549-BD3A8BD24E6E"), OrderID = Guid.Parse("57C8B2EC-B017-45CB-9E7F-429B369995BF"), ProductInventoryID = Guid.Parse("2EA4B94E-E404-4B68-8783-5497B3099EDF"), OrderedQuantity = 1, ReservedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("922868B6-DFD5-495F-96CB-B011AEBE32FA"), OrderID = Guid.Parse("57C8B2EC-B017-45CB-9E7F-429B369995BF"), ProductInventoryID = Guid.Parse("5091CB78-4DE9-4701-B5D4-3BC624F4B1FF"), OrderedQuantity = 2, ReservedQuantity = 2 },
                //new OrderProductInventory() { ID = Guid.Parse("4145C376-76DF-4C08-872A-3E04836A38D0"), OrderID = Guid.Parse("11C42724-D941-4A81-B4C4-B8BCBE9DC87A"), ProductInventoryID = Guid.Parse("5091CB78-4DE9-4701-B5D4-3BC624F4B1FF"), OrderedQuantity = 1 },
                //new OrderProductInventory() { ID = Guid.Parse("EAFDECAA-9593-4040-80E3-A7090CD790C2"), OrderID = Guid.Parse("11C42724-D941-4A81-B4C4-B8BCBE9DC87A"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), OrderedQuantity = 1 },
                //new OrderProductInventory() { ID = Guid.Parse("A7F53A3A-CBD1-424A-B980-6DE36B206A78"), OrderID = Guid.Parse("F853F8D5-5A83-4511-BE50-6D809DB40D9A"), ProductInventoryID = Guid.Parse("F681978E-F7B2-4C2A-914B-104763913094"), OrderedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("D4EC85BC-DA87-4E9E-ACFE-4F2F36E90137"), OrderID = Guid.Parse("DFAD5F14-E10B-4783-BDB4-C3455E21E817"), ProductInventoryID = Guid.Parse("B614E84F-561D-4C3B-9DC8-4AFAF0A97847"), OrderedQuantity = 1, ReservedQuantity = 1 },
                new OrderProductInventory() { ID = Guid.Parse("30E7688E-AC5B-4723-BB4B-34019522E0C5"), OrderID = Guid.Parse("DFAD5F14-E10B-4783-BDB4-C3455E21E817"), ProductInventoryID = Guid.Parse("B0DAD4AD-3A1A-4302-9101-88C956A69EEF"), OrderedQuantity = 4, ReservedQuantity = 4 }
                );

            context.PurchaseRequiredProducts.AddOrUpdate(
                prp => prp.ID,
                new PurchaseRequiredProduct() { ID = Guid.Parse("1E32A238-D399-43DB-BD63-353F72B5DDC4"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), SkuNumber = "12345678", Name = "iPhone 7 - Apple", Note = "测试1", Quantity = 2 },
                new PurchaseRequiredProduct() { ID = Guid.Parse("DCFE0120-260E-498F-ABF9-832378F4BF3C"), OrderID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), SkuNumber = "12345679", Name = "iPhone 6s - Apple", Note = "测试2", Quantity = 3 },
                new PurchaseRequiredProduct() { ID = Guid.Parse("4D0F82CA-7E96-461F-8B02-0BA9407936A2"), OrderID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), SkuNumber = "", Name = "未知产品 - 未知品牌", Note = "测试3", Quantity = 3 }
                );

           

        }

        public void SeedShippings(InventoryManagementDBContext context)
        {
            context.ExpressCompanies.AddOrUpdate(
               ec => ec.ID,
               new ExpressCompany() { ID = Guid.Parse("DB0ED981-2B23-47F0-9E11-5D918CE53262"), Name = "百世汇通-卡尔加里", Address = "卡尔加里", WorkPhone = "" },
               new ExpressCompany() { ID = Guid.Parse("EDEB5D98-E425-49AA-A3A2-DAD358CDC2D7"), Name = "百世汇通-上海", Address = "上海", WorkPhone = "" },
               new ExpressCompany() { ID = Guid.Parse("BBECE8BC-B325-481E-BE83-ADD2E8E502D4"), Name = "Fast Go", Address = "卡尔加里", WorkPhone = "" }
               );

            //context.Shippings.AddOrUpdate(
            //    sh => sh.ID,
            //    new Shipping() { ID = Guid.Parse("A7C4F75C-E06A-486C-AE97-6B5C294E4FA5"), ExpressCompanyID = Guid.Parse("DB0ED981-2B23-47F0-9E11-5D918CE53262"), ShippedDate = DateTime.Today, ReceivedDate = DateTime.MinValue, ShippingNumber = "12121212", ShippingCost = 12, ShippingStatus = "", Note = "" },
            //    new Shipping() { ID = Guid.Parse("E04B0512-0F59-4A31-8319-506856561F67"), ExpressCompanyID = Guid.Parse("DB0ED981-2B23-47F0-9E11-5D918CE53262"), ShippedDate = DateTime.Today, ReceivedDate = DateTime.MinValue, ShippingNumber = "13131313", ShippingCost = 8, ShippingStatus = "", Note = "" },
            //    new Shipping() { ID = Guid.Parse("B9CF781E-88FB-4AEA-B616-3DC8BBFACE1A"), ExpressCompanyID = Guid.Parse("BBECE8BC-B325-481E-BE83-ADD2E8E502D4"), ShippedDate = DateTime.Today, ReceivedDate = DateTime.MinValue, ShippingNumber = "14141414", ShippingCost = 9, ShippingStatus = "", Note = "" }
            //    );

            //context.OrderShippings.AddOrUpdate(
            //    os => os.ID,
            //    new OrderShipping() { ID = Guid.Parse("9DE35605-8341-4487-ABC4-DFAADC41E308"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), ShippingID = Guid.Parse("A7C4F75C-E06A-486C-AE97-6B5C294E4FA5") },
            //    new OrderShipping() { ID = Guid.Parse("09A639FB-6C09-4738-88FE-E7F921140CCC"), OrderID = Guid.Parse("F6DE5FB0-9FEA-4AE2-A065-A1611DE9111A"), ShippingID = Guid.Parse("E04B0512-0F59-4A31-8319-506856561F67") },
            //    new OrderShipping() { ID = Guid.Parse("5B815937-9774-4EF2-990E-DC365BD91FC1"), OrderID = Guid.Parse("F2E95C84-359D-4CF1-A599-989940C11C57"), ShippingID = Guid.Parse("B9CF781E-88FB-4AEA-B616-3DC8BBFACE1A") }
            //    );
        }

        public void SeedTransfer(InventoryManagementDBContext context)
        {
            context.Transfers.AddOrUpdate(
                tr=>tr.ID,
                new Transfer() { ID = Guid.Parse("6764050B-4D60-4149-90BF-60D49AF2F05F"), Status = TransferStatus.Preparing, ToInventoryLocationID = Guid.Parse("D4AEB19B-44D5-4B38-B44E-8F8DAC9847F1"), ProductCost = 12.99, ShippingCost = 0, TotalWeight = 2, PlacedDate = new DateTime(2017, 1, 3).AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), Note = "" },
                new Transfer() { ID = Guid.Parse("FCEC245E-352E-4C5A-B2C8-92AEC8EC4C8E"), Status = TransferStatus.Preparing, ToInventoryLocationID = Guid.Parse("D4AEB19B-44D5-4B38-B44E-8F8DAC9847F1"), ProductCost = 12.99, ShippingCost = 0, TotalWeight = 2, PlacedDate = new DateTime(2017, 1, 4).AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), Note = "" },
                new Transfer() { ID = Guid.Parse("24DC8609-726D-4B21-A62D-677F67B11065"), Status = TransferStatus.Preparing, ToInventoryLocationID = Guid.Parse("D4AEB19B-44D5-4B38-B44E-8F8DAC9847F1"), ProductCost = 12.99, ShippingCost = 0, TotalWeight = 2, PlacedDate = new DateTime(2017, 1, 5).AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), Note = "" },
                new Transfer() { ID = Guid.Parse("A86C3303-555B-4A2D-BDB3-F73696FAE292"), Status = TransferStatus.Preparing, ToInventoryLocationID = Guid.Parse("7F614409-3C1E-4C62-AFC6-AFFD7D92ECE9"), ProductCost = 12.99, ShippingCost = 0, TotalWeight = 2, PlacedDate = new DateTime(2017, 1, 7).AddHours(localtimeOffset), ClosedDate = DateTime.MinValue.AddHours(localtimeOffset), Note = "" }
                );

            context.TransferProductInventories.AddOrUpdate(
                tpi => tpi.ID,
                new TransferProductInventory() { ID = Guid.Parse("0C897061-7EC8-42BC-8A2B-9F6B15E62E17"), TransferID = Guid.Parse("6764050B-4D60-4149-90BF-60D49AF2F05F"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), TransferedQuantity = 1 },
                new TransferProductInventory() { ID = Guid.Parse("D5578BC7-95A8-4009-876B-3873D42CC0C1"), TransferID = Guid.Parse("FCEC245E-352E-4C5A-B2C8-92AEC8EC4C8E"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), TransferedQuantity = 1 },
                new TransferProductInventory() { ID = Guid.Parse("1A945C3E-4795-47D4-B210-7F83086D8814"), TransferID = Guid.Parse("24DC8609-726D-4B21-A62D-677F67B11065"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), TransferedQuantity = 1 },
                new TransferProductInventory() { ID = Guid.Parse("5CD107F1-6B43-44C6-B130-DAB00EB66003"), TransferID = Guid.Parse("A86C3303-555B-4A2D-BDB3-F73696FAE292"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), TransferedQuantity = 1 }
                //new TransferProductInventory() { ID = Guid.Parse("168037DC-D553-4901-B026-329A260687E8"), TransferID = Guid.Parse("6764050B-4D60-4149-90BF-60D49AF2F05F"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), TransferedQuantity = 1 },
                //new TransferProductInventory() { ID = Guid.Parse("A6DFF414-3609-4006-8C88-2643A48C67B6"), TransferID = Guid.Parse("6764050B-4D60-4149-90BF-60D49AF2F05F"), ProductInventoryID = Guid.Parse("0D28D035-200D-47BA-B53D-8F7E86414F0E"), TransferedQuantity = 1 }

                );

        }


    }
}



