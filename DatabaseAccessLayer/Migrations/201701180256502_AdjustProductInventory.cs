namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustProductInventory : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("Instance.ProductSupply");
            CreateTable(
                "Instance.InventoryChangeRecord",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SkuNumber = c.String(nullable: false),
                        ProductInventoryID = c.Guid(nullable: false),
                        ChangeQuantity = c.String(nullable: false),
                        ChangeDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ChangeType = c.Int(nullable: false),
                        ChangeReason = c.Int(nullable: false),
                        ChangeNote = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Instance.ProductInventory", t => t.ProductInventoryID, cascadeDelete: true)
                .Index(t => t.ProductInventoryID);
            
            CreateTable(
                "Instance.ProductInventory",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        SkuNumber = c.String(nullable: false, maxLength: 128),
                        LocationID = c.Guid(nullable: false),
                        ExpiredDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        QuantityOnHand = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Instance.InventoryLocation", t => t.LocationID, cascadeDelete: true)
                .ForeignKey("Instance.Product", t => t.SkuNumber, cascadeDelete: true)
                .Index(t => t.SkuNumber)
                .Index(t => t.LocationID);
            
            CreateTable(
                "Instance.InventoryLocation",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        LocationName = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddPrimaryKey("Instance.ProductSupply", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("Instance.ProductInventory", "SkuNumber", "Instance.Product");
            DropForeignKey("Instance.ProductInventory", "LocationID", "Instance.InventoryLocation");
            DropForeignKey("Instance.InventoryChangeRecord", "ProductInventoryID", "Instance.ProductInventory");
            DropIndex("Instance.ProductInventory", new[] { "LocationID" });
            DropIndex("Instance.ProductInventory", new[] { "SkuNumber" });
            DropIndex("Instance.InventoryChangeRecord", new[] { "ProductInventoryID" });
            DropPrimaryKey("Instance.ProductSupply");
            DropTable("Instance.InventoryLocation");
            DropTable("Instance.ProductInventory");
            DropTable("Instance.InventoryChangeRecord");
            AddPrimaryKey("Instance.ProductSupply", new[] { "ID", "SkuNumber" });
        }
    }
}
