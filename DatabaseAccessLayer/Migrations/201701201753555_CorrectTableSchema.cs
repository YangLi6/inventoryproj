namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectTableSchema : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.NoRecordProducts", newName: "NoRecordProduct");
            RenameTable(name: "dbo.OrderProductInventories", newName: "OrderProductInventory");
            MoveTable(name: "dbo.NoRecordProduct", newSchema: "Instance");
            MoveTable(name: "dbo.OrderProductInventory", newSchema: "Instance");
        }
        
        public override void Down()
        {
            MoveTable(name: "Instance.OrderProductInventory", newSchema: "dbo");
            MoveTable(name: "Instance.NoRecordProduct", newSchema: "dbo");
            RenameTable(name: "dbo.OrderProductInventory", newName: "OrderProductInventories");
            RenameTable(name: "dbo.NoRecordProduct", newName: "NoRecordProducts");
        }
    }
}
