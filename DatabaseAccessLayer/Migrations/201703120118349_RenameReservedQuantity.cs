namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameReservedQuantity : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.OrderProductInventory", "ReservedQuantity", c => c.Int(nullable: false));
            DropColumn("Instance.OrderProductInventory", "PurchasedQuantity");
        }
        
        public override void Down()
        {
            AddColumn("Instance.OrderProductInventory", "PurchasedQuantity", c => c.Int(nullable: false));
            DropColumn("Instance.OrderProductInventory", "ReservedQuantity");
        }
    }
}
