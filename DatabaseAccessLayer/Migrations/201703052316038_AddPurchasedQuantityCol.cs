namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPurchasedQuantityCol : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.OrderProductInventory", "PurchasedQuantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Instance.OrderProductInventory", "PurchasedQuantity");
        }
    }
}
