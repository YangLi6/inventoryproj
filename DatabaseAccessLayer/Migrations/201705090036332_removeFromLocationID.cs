namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeFromLocationID : DbMigration
    {
        public override void Up()
        {
            DropColumn("Instance.Transfer", "FromInventoryLocationID");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Transfer", "FromInventoryLocationID", c => c.Guid(nullable: false));
        }
    }
}
