namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveForeignKeyFromOrderTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod");
            DropIndex("Instance.Order", new[] { "PaymentMethodID" });
            AlterColumn("Instance.Order", "PaymentMethodID", c => c.Guid());
            CreateIndex("Instance.Order", "PaymentMethodID");
            AddForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod");
            DropIndex("Instance.Order", new[] { "PaymentMethodID" });
            AlterColumn("Instance.Order", "PaymentMethodID", c => c.Guid(nullable: false));
            CreateIndex("Instance.Order", "PaymentMethodID");
            AddForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod", "ID", cascadeDelete: true);
        }
    }
}
