namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveForeignKeyFromOrderTable1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod");
            DropIndex("Instance.Order", new[] { "PaymentMethodID" });
        }
        
        public override void Down()
        {
            CreateIndex("Instance.Order", "PaymentMethodID");
            AddForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod", "ID");
        }
    }
}
