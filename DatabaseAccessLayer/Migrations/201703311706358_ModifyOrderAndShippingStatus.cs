namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyOrderAndShippingStatus : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Instance.Shipping", "ShippingStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("Instance.Shipping", "ShippingStatus", c => c.String());
        }
    }
}
