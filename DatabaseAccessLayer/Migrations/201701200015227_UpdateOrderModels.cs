namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOrderModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.Order", "TotalWeight", c => c.Double(nullable: false));
            AddColumn("Instance.Order", "ProductCost", c => c.Double(nullable: false));
            AddColumn("Instance.Order", "ShippingCost", c => c.Double(nullable: false));
            AddColumn("Instance.Order", "TotalBilling", c => c.Double(nullable: false));
            AddColumn("Instance.Order", "Note", c => c.String());
            AlterColumn("Instance.Order", "Status", c => c.Int(nullable: false));
            CreateIndex("Instance.Order", "CustomerID");
            CreateIndex("Instance.Order", "PaymentMethodID");
            AddForeignKey("Instance.Order", "CustomerID", "User.Customer", "ID", cascadeDelete: true);
            AddForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod", "ID", cascadeDelete: true);
            DropColumn("Instance.Order", "TotalPrice");
            DropColumn("Instance.Order", "PaidPrice");
            DropColumn("Instance.Order", "OtherDetail");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Order", "OtherDetail", c => c.String());
            AddColumn("Instance.Order", "PaidPrice", c => c.Single(nullable: false));
            AddColumn("Instance.Order", "TotalPrice", c => c.Single(nullable: false));
            DropForeignKey("Instance.Order", "PaymentMethodID", "Resource.PaymentMethod");
            DropForeignKey("Instance.Order", "CustomerID", "User.Customer");
            DropIndex("Instance.Order", new[] { "PaymentMethodID" });
            DropIndex("Instance.Order", new[] { "CustomerID" });
            AlterColumn("Instance.Order", "Status", c => c.String(nullable: false));
            DropColumn("Instance.Order", "Note");
            DropColumn("Instance.Order", "TotalBilling");
            DropColumn("Instance.Order", "ShippingCost");
            DropColumn("Instance.Order", "ProductCost");
            DropColumn("Instance.Order", "TotalWeight");
        }
    }
}
