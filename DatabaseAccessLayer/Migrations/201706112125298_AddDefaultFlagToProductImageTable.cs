namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultFlagToProductImageTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.ProductImage", "IsDefault", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Instance.ProductImage", "IsDefault");
        }
    }
}
