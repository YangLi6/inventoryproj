namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrderShippingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Instance.OrderShipping",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ShippingID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("Instance.OrderShipping");
        }
    }
}
