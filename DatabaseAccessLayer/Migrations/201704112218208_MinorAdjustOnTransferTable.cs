namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MinorAdjustOnTransferTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.Transfer", "Status", c => c.Int(nullable: false));
            DropColumn("Instance.TransferProductInventory", "TransferedQuantity");
            AddColumn("Instance.TransferProductInventory", "TransferedQuantity", c => c.Int(nullable: false));
            DropColumn("Instance.Transfer", "IsCompleted");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Transfer", "IsCompleted", c => c.Boolean(nullable: false));
            DropColumn("Instance.TransferProductInventory", "TransferedQuantity");
            AddColumn("Instance.TransferProductInventory", "TransferedQuantity", c => c.Guid(nullable: false));
            DropColumn("Instance.Transfer", "Status");
        }
    }
}
