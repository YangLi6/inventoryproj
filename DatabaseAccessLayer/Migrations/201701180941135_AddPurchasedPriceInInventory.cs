namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPurchasedPriceInInventory : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.InventoryChangeRecord", "FromLocationID", c => c.Guid(nullable: false));
            AddColumn("Instance.InventoryChangeRecord", "ToLocationID", c => c.Guid(nullable: false));
            AddColumn("Instance.ProductInventory", "PurchasedPrice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Instance.ProductInventory", "PurchasedPrice");
            DropColumn("Instance.InventoryChangeRecord", "ToLocationID");
            DropColumn("Instance.InventoryChangeRecord", "FromLocationID");
        }
    }
}
