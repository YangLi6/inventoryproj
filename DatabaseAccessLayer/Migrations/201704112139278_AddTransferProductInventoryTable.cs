namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransferProductInventoryTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Instance.TransferProductInventory",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TransferID = c.Guid(nullable: false),
                        ProductInventoryID = c.Guid(nullable: false),
                        TransferedQuantity = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("Instance.TransferProductInventory");
        }
    }
}
