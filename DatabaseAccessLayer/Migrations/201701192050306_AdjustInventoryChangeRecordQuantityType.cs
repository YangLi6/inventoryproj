namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustInventoryChangeRecordQuantityType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Instance.InventoryChangeRecord", "ChangeQuantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("Instance.InventoryChangeRecord", "ChangeQuantity", c => c.String(nullable: false));
        }
    }
}
