namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProductInventoryToTransferPI : DbMigration
    {
        public override void Up()
        {
            CreateIndex("Instance.TransferProductInventory", "ProductInventoryID");
            AddForeignKey("Instance.TransferProductInventory", "ProductInventoryID", "Instance.ProductInventory", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Instance.TransferProductInventory", "ProductInventoryID", "Instance.ProductInventory");
            DropIndex("Instance.TransferProductInventory", new[] { "ProductInventoryID" });
        }
    }
}
