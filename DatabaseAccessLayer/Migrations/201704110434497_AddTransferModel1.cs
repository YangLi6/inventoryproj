namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTransferModel1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Instance.Transfer",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        FromInventoryLocationID = c.Guid(nullable: false),
                        ToInventoryLocationID = c.Guid(nullable: false),
                        TotalWeight = c.Double(nullable: false),
                        ProductCost = c.Double(nullable: false),
                        ShippingCost = c.Double(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        Note = c.String(),
                        PlacedDate = c.DateTime(precision: 7, storeType: "datetime2"),
                        ClosedDate = c.DateTime(precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.TransferShipping",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        TransferID = c.Guid(nullable: false),
                        ShippingID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("Instance.TransferShipping");
            DropTable("Instance.Transfer");
        }
    }
}
