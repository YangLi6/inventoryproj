namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameStatusToNote : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.Shipping", "Note", c => c.String());
            DropColumn("Instance.Shipping", "Status");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Shipping", "Status", c => c.String());
            DropColumn("Instance.Shipping", "Note");
        }
    }
}
