namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustOrderModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.OrderProductInventory", "OrderedQuantity", c => c.Int(nullable: false));
            AddColumn("Instance.Order", "Billing", c => c.Double(nullable: false));
            AddColumn("Instance.Order", "Paid", c => c.Double(nullable: false));
            AddColumn("Instance.Order", "ClosedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            DropColumn("Instance.OrderProductInventory", "OrderQuantity");
            DropColumn("Instance.Order", "TotalBilling");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Order", "TotalBilling", c => c.Double(nullable: false));
            AddColumn("Instance.OrderProductInventory", "OrderQuantity", c => c.Int(nullable: false));
            DropColumn("Instance.Order", "ClosedDate");
            DropColumn("Instance.Order", "Paid");
            DropColumn("Instance.Order", "Billing");
            DropColumn("Instance.OrderProductInventory", "OrderedQuantity");
        }
    }
}
