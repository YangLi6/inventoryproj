namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCustomerIDPhoto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "User.CustomerIDPhoto",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        URL = c.String(),
                        CustomerID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("User.Customer", t => t.CustomerID, cascadeDelete: true)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "Instance.ProductSupply",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        PurchasedPrice = c.Single(nullable: false),
                        PurchasedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ExpiredDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ID, t.ProductID })
                .ForeignKey("Instance.Product", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            AddColumn("User.Customer", "Address", c => c.String());
            AddColumn("User.Customer", "AlterAddress", c => c.String());
            DropColumn("User.Customer", "IsAgent");
            DropColumn("User.Customer", "Address1");
            DropColumn("User.Customer", "Address2");
            DropColumn("User.Customer", "HomePhone");
        }
        
        public override void Down()
        {
            AddColumn("User.Customer", "HomePhone", c => c.String());
            AddColumn("User.Customer", "Address2", c => c.String());
            AddColumn("User.Customer", "Address1", c => c.String());
            AddColumn("User.Customer", "IsAgent", c => c.Boolean(nullable: false));
            DropForeignKey("Instance.ProductSupply", "ProductID", "Instance.Product");
            DropForeignKey("User.CustomerIDPhoto", "CustomerID", "User.Customer");
            DropIndex("Instance.ProductSupply", new[] { "ProductID" });
            DropIndex("User.CustomerIDPhoto", new[] { "CustomerID" });
            DropColumn("User.Customer", "AlterAddress");
            DropColumn("User.Customer", "Address");
            DropTable("Instance.ProductSupply");
            DropTable("User.CustomerIDPhoto");
        }
    }
}
