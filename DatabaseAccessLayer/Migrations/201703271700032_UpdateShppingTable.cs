namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateShppingTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.Shipping", "ExpressCompanyID", c => c.Guid(nullable: false));
            AddColumn("Instance.Shipping", "ShippingNumber", c => c.String(nullable: false));
            AddColumn("Instance.Shipping", "ShippingStatus", c => c.String());
            AddColumn("Instance.Shipping", "ShippingCost", c => c.Double(nullable: true));
            DropColumn("Instance.Shipping", "CanadaExpressCompanyID");
            DropColumn("Instance.Shipping", "CanadaExpressNumber");
            DropColumn("Instance.Shipping", "CanadaShippingStatus");
            DropColumn("Instance.Shipping", "CanadaShippingCost");
            DropColumn("Instance.Shipping", "ChinaExpressCompanyID");
            DropColumn("Instance.Shipping", "ChinaExpressNumber");
            DropColumn("Instance.Shipping", "ChinaShippingStatus");
            DropColumn("Instance.Shipping", "ChinaShippingCost");
            DropColumn("Instance.Shipping", "IsDirectShipping");
            DropColumn("Instance.Shipping", "ShippedToName");
            DropColumn("Instance.Shipping", "ShippedToAddress");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Shipping", "ShippedToAddress", c => c.String());
            AddColumn("Instance.Shipping", "ShippedToName", c => c.String());
            AddColumn("Instance.Shipping", "IsDirectShipping", c => c.Boolean(nullable: false));
            AddColumn("Instance.Shipping", "ChinaShippingCost", c => c.Single(nullable: false));
            AddColumn("Instance.Shipping", "ChinaShippingStatus", c => c.String());
            AddColumn("Instance.Shipping", "ChinaExpressNumber", c => c.String());
            AddColumn("Instance.Shipping", "ChinaExpressCompanyID", c => c.Guid(nullable: false));
            AddColumn("Instance.Shipping", "CanadaShippingCost", c => c.Single(nullable: false));
            AddColumn("Instance.Shipping", "CanadaShippingStatus", c => c.String());
            AddColumn("Instance.Shipping", "CanadaExpressNumber", c => c.String());
            AddColumn("Instance.Shipping", "CanadaExpressCompanyID", c => c.Guid(nullable: false));
            DropColumn("Instance.Shipping", "ShippingCost");
            DropColumn("Instance.Shipping", "ShippingStatus");
            DropColumn("Instance.Shipping", "ShippingNumber");
            DropColumn("Instance.Shipping", "ExpressCompanyID");
        }
    }
}
