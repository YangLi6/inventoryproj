namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeShippingDateNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Instance.Shipping", "ShippedDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("Instance.Shipping", "ReceivedDate", c => c.DateTime(precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("Instance.Shipping", "ReceivedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("Instance.Shipping", "ShippedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
    }
}
