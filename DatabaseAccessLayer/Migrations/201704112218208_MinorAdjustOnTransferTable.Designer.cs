// <auto-generated />
namespace DatabaseAccessLayer.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class MinorAdjustOnTransferTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MinorAdjustOnTransferTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201704112218208_MinorAdjustOnTransferTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
