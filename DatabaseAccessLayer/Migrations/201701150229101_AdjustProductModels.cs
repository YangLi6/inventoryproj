namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdjustProductModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Instance.ProductImage", "ProductID", "Instance.Product");
            DropForeignKey("Instance.ProductSupply", "ProductID", "Instance.Product");
            DropIndex("Instance.ProductImage", new[] { "ProductID" });
            DropIndex("Instance.ProductSupply", new[] { "ProductID" });
            DropColumn("Instance.ProductImage", "SkuNumber");
            RenameColumn(table: "Instance.ProductImage", name: "ProductID", newName: "SkuNumber");
            RenameColumn(table: "Instance.ProductSupply", name: "ProductID", newName: "SkuNumber");
            DropPrimaryKey("Instance.Product");
            DropPrimaryKey("Instance.ProductSupply");
            AddColumn("Instance.ProductSupply", "PurchasedStore", c => c.String());
            AddColumn("Instance.ProductSupply", "PurchasedQuantity", c => c.Int(nullable: false));
            AlterColumn("Instance.ProductImage", "SkuNumber", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("Instance.ProductImage", "SkuNumber", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("Instance.Product", "Brand", c => c.String());
            AlterColumn("Instance.Product", "SkuNumber", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("Instance.Product", "Weight", c => c.Double(nullable: false));
            AlterColumn("Instance.ProductSupply", "SkuNumber", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("Instance.ProductSupply", "PurchasedPrice", c => c.Double(nullable: false));
            AddPrimaryKey("Instance.Product", "SkuNumber");
            AddPrimaryKey("Instance.ProductSupply", new[] { "ID", "SkuNumber" });
            CreateIndex("Instance.ProductImage", "SkuNumber");
            CreateIndex("Instance.ProductSupply", "SkuNumber");
            AddForeignKey("Instance.ProductImage", "SkuNumber", "Instance.Product", "SkuNumber", cascadeDelete: true);
            AddForeignKey("Instance.ProductSupply", "SkuNumber", "Instance.Product", "SkuNumber", cascadeDelete: true);
            DropColumn("Instance.Product", "ID");
            DropColumn("Instance.Product", "CategoryID");
            DropColumn("Instance.ProductSupply", "Quantity");
        }
        
        public override void Down()
        {
            AddColumn("Instance.ProductSupply", "Quantity", c => c.Int(nullable: false));
            AddColumn("Instance.Product", "CategoryID", c => c.Guid(nullable: false));
            AddColumn("Instance.Product", "ID", c => c.Guid(nullable: false));
            DropForeignKey("Instance.ProductSupply", "SkuNumber", "Instance.Product");
            DropForeignKey("Instance.ProductImage", "SkuNumber", "Instance.Product");
            DropIndex("Instance.ProductSupply", new[] { "SkuNumber" });
            DropIndex("Instance.ProductImage", new[] { "SkuNumber" });
            DropPrimaryKey("Instance.ProductSupply");
            DropPrimaryKey("Instance.Product");
            AlterColumn("Instance.ProductSupply", "PurchasedPrice", c => c.Single(nullable: false));
            AlterColumn("Instance.ProductSupply", "SkuNumber", c => c.Guid(nullable: false));
            AlterColumn("Instance.Product", "Weight", c => c.Single(nullable: false));
            AlterColumn("Instance.Product", "SkuNumber", c => c.String(nullable: false));
            AlterColumn("Instance.Product", "Brand", c => c.String(nullable: false));
            AlterColumn("Instance.ProductImage", "SkuNumber", c => c.Guid(nullable: false));
            AlterColumn("Instance.ProductImage", "SkuNumber", c => c.String(nullable: false));
            DropColumn("Instance.ProductSupply", "PurchasedQuantity");
            DropColumn("Instance.ProductSupply", "PurchasedStore");
            AddPrimaryKey("Instance.ProductSupply", new[] { "ID", "ProductID" });
            AddPrimaryKey("Instance.Product", "ID");
            RenameColumn(table: "Instance.ProductSupply", name: "SkuNumber", newName: "ProductID");
            RenameColumn(table: "Instance.ProductImage", name: "SkuNumber", newName: "ProductID");
            AddColumn("Instance.ProductImage", "SkuNumber", c => c.String(nullable: false));
            CreateIndex("Instance.ProductSupply", "ProductID");
            CreateIndex("Instance.ProductImage", "ProductID");
            AddForeignKey("Instance.ProductSupply", "ProductID", "Instance.Product", "ID", cascadeDelete: true);
            AddForeignKey("Instance.ProductImage", "ProductID", "Instance.Product", "ID", cascadeDelete: true);
        }
    }
}
