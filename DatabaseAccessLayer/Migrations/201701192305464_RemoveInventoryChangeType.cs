namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveInventoryChangeType : DbMigration
    {
        public override void Up()
        {
            DropColumn("Instance.InventoryChangeRecord", "ChangeType");
        }
        
        public override void Down()
        {
            AddColumn("Instance.InventoryChangeRecord", "ChangeType", c => c.Int(nullable: false));
        }
    }
}
