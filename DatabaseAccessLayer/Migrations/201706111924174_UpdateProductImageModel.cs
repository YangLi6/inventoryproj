namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProductImageModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.ProductImage", "Extension", c => c.String(nullable: false));
            AddColumn("Instance.ProductImage", "ImageBaseUrl", c => c.String(nullable: false));
            DropColumn("Instance.ProductImage", "ImageUrl");
        }
        
        public override void Down()
        {
            AddColumn("Instance.ProductImage", "ImageUrl", c => c.String());
            DropColumn("Instance.ProductImage", "ImageBaseUrl");
            DropColumn("Instance.ProductImage", "Extension");
        }
    }
}
