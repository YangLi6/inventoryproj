namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNoRecordProductTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Instance.ReceiptImage", "ReceiptID", "Instance.Receipt");
            DropIndex("Instance.ReceiptImage", new[] { "ReceiptID" });
            CreateTable(
                "dbo.NoRecordProducts",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(),
                        Note = c.String(),
                        Quantity = c.Int(nullable: false),
                        OrderID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.OrderProductInventories",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ProductInventoryID = c.Guid(nullable: false),
                        OrderQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("Resource.Category");
            DropTable("Instance.CustomerOrderProduct");
            DropTable("Instance.ReceiptImage");
            DropTable("Instance.Receipt");
        }
        
        public override void Down()
        {
            CreateTable(
                "Instance.Receipt",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlacedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Total = c.Single(nullable: false),
                        CompanyName = c.String(),
                        NumberItem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.ReceiptImage",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ReceiptID = c.Guid(nullable: false),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.CustomerOrderProduct",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        NonExistedProductName = c.String(),
                        Quantity = c.Int(nullable: false),
                        EachSoldPrice = c.Single(nullable: false),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Resource.Category",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropTable("dbo.OrderProductInventories");
            DropTable("dbo.NoRecordProducts");
            CreateIndex("Instance.ReceiptImage", "ReceiptID");
            AddForeignKey("Instance.ReceiptImage", "ReceiptID", "Instance.Receipt", "ID", cascadeDelete: true);
        }
    }
}
