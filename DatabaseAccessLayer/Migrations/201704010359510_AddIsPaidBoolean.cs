namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsPaidBoolean : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.Order", "IsPaid", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Instance.Order", "IsPaid");
        }
    }
}
