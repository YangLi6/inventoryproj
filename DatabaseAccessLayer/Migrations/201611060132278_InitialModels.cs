namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Resource.Category",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.CustomerOrderProduct",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ProductID = c.Guid(nullable: false),
                        NonExistedProductName = c.String(),
                        Quantity = c.Int(nullable: false),
                        EachSoldPrice = c.Single(nullable: false),
                        Notes = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "User.Customer",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        IsAgent = c.Boolean(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        CellPhone = c.String(),
                        HomePhone = c.String(),
                        Note = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Resource.ExpressCompany",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        WorkPhone = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.OrderImage",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        OrderID = c.Guid(nullable: false),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.Order",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        CustomerID = c.Guid(nullable: false),
                        Status = c.String(nullable: false),
                        TotalPrice = c.Single(nullable: false),
                        PaidPrice = c.Single(nullable: false),
                        PaymentMethodID = c.Guid(nullable: false),
                        OtherDetail = c.String(),
                        PlacedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PaidDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Resource.PaymentMethod",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.ProductImage",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ImageUrl = c.String(),
                        SkuNumber = c.String(nullable: false),
                        ProductID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Instance.Product", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.ProductID);
            
            CreateTable(
                "Instance.Product",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Brand = c.String(nullable: false),
                        SkuNumber = c.String(nullable: false),
                        CategoryID = c.Guid(nullable: false),
                        HasExpiredDate = c.Boolean(nullable: false),
                        Weight = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.ReceiptImage",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ReceiptID = c.Guid(nullable: false),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Instance.Receipt", t => t.ReceiptID, cascadeDelete: true)
                .Index(t => t.ReceiptID);
            
            CreateTable(
                "Instance.Receipt",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        PlacedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Total = c.Single(nullable: false),
                        CompanyName = c.String(),
                        NumberItem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "Instance.ShippingImage",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        ShippingID = c.Guid(nullable: false),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("Instance.Shipping", t => t.ShippingID, cascadeDelete: true)
                .Index(t => t.ShippingID);
            
            CreateTable(
                "Instance.Shipping",
                c => new
                    {
                        OrderID = c.Guid(nullable: false),
                        ShippedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ReceivedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Status = c.String(),
                        CanadaExpressCompanyID = c.Guid(nullable: false),
                        CanadaExpressNumber = c.String(),
                        CanadaShippingStatus = c.String(),
                        CanadaShippingCost = c.Single(nullable: false),
                        ChinaExpressCompanyID = c.Guid(nullable: false),
                        ChinaExpressNumber = c.String(),
                        ChinaShippingStatus = c.String(),
                        ChinaShippingCost = c.Single(nullable: false),
                        IsDirectShipping = c.Boolean(nullable: false),
                        ShippedToName = c.String(),
                        ShippedToAddress = c.String(),
                    })
                .PrimaryKey(t => t.OrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Instance.ShippingImage", "ShippingID", "Instance.Shipping");
            DropForeignKey("Instance.ReceiptImage", "ReceiptID", "Instance.Receipt");
            DropForeignKey("Instance.ProductImage", "ProductID", "Instance.Product");
            DropIndex("Instance.ShippingImage", new[] { "ShippingID" });
            DropIndex("Instance.ReceiptImage", new[] { "ReceiptID" });
            DropIndex("Instance.ProductImage", new[] { "ProductID" });
            DropTable("Instance.Shipping");
            DropTable("Instance.ShippingImage");
            DropTable("Instance.Receipt");
            DropTable("Instance.ReceiptImage");
            DropTable("Instance.Product");
            DropTable("Instance.ProductImage");
            DropTable("Resource.PaymentMethod");
            DropTable("Instance.Order");
            DropTable("Instance.OrderImage");
            DropTable("Resource.ExpressCompany");
            DropTable("User.Customer");
            DropTable("Instance.CustomerOrderProduct");
            DropTable("Resource.Category");
        }
    }
}
