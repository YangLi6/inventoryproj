namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReplaceOrderIDToShippingID : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Instance.ShippingImage", "ShippingID", "Instance.Shipping");
            DropPrimaryKey("Instance.Shipping");
            AddColumn("Instance.Shipping", "ID", c => c.Guid(nullable: false));
            AddPrimaryKey("Instance.Shipping", "ID");
            AddForeignKey("Instance.ShippingImage", "ShippingID", "Instance.Shipping", "ID", cascadeDelete: true);
            DropColumn("Instance.Shipping", "OrderID");
        }
        
        public override void Down()
        {
            AddColumn("Instance.Shipping", "OrderID", c => c.Guid(nullable: false));
            DropForeignKey("Instance.ShippingImage", "ShippingID", "Instance.Shipping");
            DropPrimaryKey("Instance.Shipping");
            DropColumn("Instance.Shipping", "ID");
            AddPrimaryKey("Instance.Shipping", "OrderID");
            AddForeignKey("Instance.ShippingImage", "ShippingID", "Instance.Shipping", "OrderID", cascadeDelete: true);
        }
    }
}
