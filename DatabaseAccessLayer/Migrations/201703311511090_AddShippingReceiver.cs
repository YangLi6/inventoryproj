namespace DatabaseAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShippingReceiver : DbMigration
    {
        public override void Up()
        {
            AddColumn("Instance.Shipping", "Receiver", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Instance.Shipping", "Receiver");
        }
    }
}
